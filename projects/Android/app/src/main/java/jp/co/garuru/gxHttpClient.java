//----------------------------------------------------------
//
// http通信
//
//----------------------------------------------------------

package jp.co.garuru;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import java.io.File;
//
import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;
import java.io.InputStream;
//
import java.io.InputStreamReader;
import java.io.BufferedReader;


public class gxHttpClient extends AsyncTask<URL, Void, String> {

//	@Override
//	protected void onPreExecute() {
//		super.onPreExecute();
//		// doInBackground前処理
//	}

	private int m_ID = 0;

	public void SetID( int id )
	{
		m_ID = id;
	}
	@Override
	protected String doInBackground(URL... urls) {
		//非同期
		HttpURLConnection con = null;
		URL url = null;
		//String urlSt = ;//"http://www.garuru.co.jp/api/basic.php?ope=GetIP";
		final StringBuilder result = new StringBuilder();

		try {
			// URLの作成
			//url = new URL(urlSt);
			// 接続用HttpURLConnectionオブジェクト作成
			con = (HttpURLConnection)urls[0].openConnection();
			// リクエストメソッドの設定
			con.setRequestMethod("POST");
			// リダイレクトを自動で許可しない設定
			con.setInstanceFollowRedirects(false);
			// URL接続からデータを読み取る場合はtrue
			con.setDoInput(true);
			// URL接続にデータを書き込む場合はtrue
			con.setDoOutput(true);

			// 接続
			con.connect(); // ①

			final int status = con.getResponseCode();

			if (status == HttpURLConnection.HTTP_OK)
			{
				// 本文の取得
				//InputStream in = con.getInputStream();
				//byte bodyByte[] = new byte[1024];
				//in.read(bodyByte);
				//in.close();
				final InputStream in = con.getInputStream();

				long sz = in.available();

				byte bodyByte[] = new byte[1024*1024];//(int)sz];

				int sz2 = 0;

				while ( true )
				{
					long sz1 = 0;
					sz1 = in.read(bodyByte , sz2 , 1024 );

					if( sz1 <= 0 ) break;	//sz2は-1を返してくるので足しちゃダメ

					sz2 += sz1;
				}
				in.close();

				byte pixy[] = new byte[sz2];

				for( int ii=0; ii<sz2; ii++ ) {
					pixy[ii] = bodyByte[ii];
				}

				 GameGirlJNI.ReturnHttpResult( m_ID , pixy , true );

/*
				int iz = in.read();
				String encoding = con.getContentEncoding();

				if( encoding == null )
				{
					encoding = new String("sjis");
				}

				final InputStreamReader inReader = new InputStreamReader(in, encoding );
				final BufferedReader bufReader = new BufferedReader(inReader);
				String line = null;
				// 1行ずつテキストを読み込む
				while((line = bufReader.readLine()) != null) {
					result.append(line);
				}
				String strHttpText = result.toString();
				GameGirlJNI.ReturnHttpResult( m_ID , strHttpText );

				bufReader.close();
				inReader.close();
				in.close();
*/
			}
			else
			{
				byte pixy[] = new byte[8];
				pixy[0] = '4';
				pixy[1] = '0';
				pixy[2] = '4';
				pixy[3] = 0x00;
				GameGirlJNI.ReturnHttpResult( m_ID , pixy , false);
			}


		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if (con != null) {
				// コネクションを切断
				con.disconnect();
			}
		}
		return result.toString();
	}

//	@Override
 //   protected void onPostExecute(Integer result) {
 //	   super.onPostExecute(result);
 //	   // doInBackground後処理
 //   }

}