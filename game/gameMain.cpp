/*

  GameMain.cpp

  written by ragi.

  2017.12.02
*/
#include <gxLib.h>
#include <game/gunhound/gga2.h>
#include <time.h>


typedef struct StClock
{
	Float32 Alpha;
	Sint32  Count;

} StClock;

gxBool g_bEnableClockDisp = gxFalse;

StClock g_Clock;

gxBool GunHound( gxBool bReset );

gxSprite* GetClockFont( gxChar pMoji )
{
	Sint32 n = -1;
	
	switch( pMoji ){
	case '0':	n = 9;	break;
	case '1':	n = 0;	break;
	case '2':	n = 1;	break;
	case '3':	n = 2;	break;
	case '4':	n = 3;	break;
	case '5':	n = 4;	break;
	case '6':	n = 5;	break;
	case '7':	n = 6;	break;
	case '8':	n = 7;	break;
	case '9':	n = 8;	break;
	case ':':	n = 10;	break;
	default:
		break;
	}

	static gxSprite spr[]={
		{enDemoTexPage,0*206,0*386,206,386,100,193},
		{enDemoTexPage,1*206,0*386,206,386,100,193},
		{enDemoTexPage,2*206,0*386,206,386,100,193},
		{enDemoTexPage,3*206,0*386,206,386,100,193},
		{enDemoTexPage,4*206,0*386,206-6,386,100,193},

		{enDemoTexPage,0*206,1*386,206,386,100,193},
		{enDemoTexPage,1*206,1*386,206,386,100,193},
		{enDemoTexPage,2*206,1*386,206,386,100,193},
		{enDemoTexPage,3*206,1*386,206,386,100,193},
		{enDemoTexPage,4*206,1*386,206-6,386,100,193},

		{enDemoTexPage,0*206,2*386,206,1024-(386*2),100-80,193-32},

		{enDemoTexPage,0,0,1,1,0,0},	//dummy
	};

	if( n < 0 || n >= 12 )
	{
		return &spr[11];
	}

	return &spr[n];
}


void GHClock()
{
	static int m_sWatchSeq = 0;
	Sint32 prio = MAX_PRIORITY_NUM-1;

	if( m_sWatchSeq == 0 )
	{
		//初期化

		gxLib::LoadTexture( enDemoTexPage    , "asset/gh/ClockFont.tga" , 0xff00ff00 );
		gxLib::LoadTexture( enDemoTexPage+16 , "asset/gh/NewLogo.tga"   , 0xff00ff00 );
		gxLib::LoadTexture( enDemoTexPage+32 , "asset/gh/ghDemo.tga"    , 0xff00ff00 );
		gxLib::UploadTexture();

		g_Clock.Alpha = 0.0f;
		g_Clock.Count = 0;

		gxLib::SetBgColor( 0xff202020 );

		m_sWatchSeq ++;
	}

	//時計コア

	time_t timer;
	struct tm *t_st;

	time(&timer);
	t_st = localtime(&timer);

	Float32 w = 220.0f/2;

	gxChar buf[32];
	sprintf( buf , "%02d:%02d" , t_st->tm_hour , t_st->tm_min );

	Uint32 len = strlen(buf);
	Sint32 ax = WINDOW_W - (len*w) + 16;
	Sint32 ay = WINDOW_H/2;

	//表示制御

	if( !g_bEnableClockDisp )
	{
		g_Clock.Alpha = 0.0f;
		g_Clock.Count = 0;
		return;
	}
	else
	{
		if( g_Clock.Count < 120 )
		{
			g_Clock.Count ++;
		}
		else
		{
			g_Clock.Alpha += ( 1.0f - g_Clock.Alpha ) / 100.0f;
			g_Clock.Alpha += 0.001f;

			if( g_Clock.Alpha >= 1.0f ) g_Clock.Alpha = 1.0f;
		}
	}

	//デジタル表示

	gxSprite spr;

	for( Sint32 ii=0; ii<len; ii++ )
	{
		Float32 alp1 = 0.5f * g_Clock.Alpha;
		Float32 alp2 = 0.9f * g_Clock.Alpha;

		spr = *GetClockFont( buf[ii] );
		gxLib::PutSprite( &spr , ax+8 , ay+8 , prio , ATR_ALPHA_MINUS  , SET_ALPHA( alp1 , 0xF0A0A0A0 ) , 0.f , 1.1f/2 , 1.1f/2 );
		gxLib::PutSprite( &spr , ax   , ay   , prio , ATR_DFLT         , SET_ALPHA( alp2 , 0xF0FFFF00 ) , 0.f , 1.0f/2 , 1.0f/2 );

		if( ii == 1 )
		{
			ax += 2*w/3;
		}
		else
		{
			ax += w;
		}
	}
}

gxBool GameInit()
{
	gxLib::SetVirtualPad(gxTrue);
	return gxTrue;
}

gxBool GameMain()
{
/*
    if(gxLib::Joy(0)->psh&BTN_A) gxLib::Printf(100,100+16*0,100,ATR_DFLT , 0xffff0000 , "BTN_A");
    if(gxLib::Joy(0)->psh&BTN_B) gxLib::Printf(100,100+16*1,100,ATR_DFLT , 0xffff0000 , "BTN_B");
    if(gxLib::Joy(0)->psh&BTN_X) gxLib::Printf(100,100+16*2,100,ATR_DFLT , 0xffff0000 , "BTN_X");
    if(gxLib::Joy(0)->psh&BTN_Y) gxLib::Printf(100,100+16*3,100,ATR_DFLT , 0xffff0000 , "BTN_Y");

    
    return gxTrue;
*/
    //ガンハウンドのプログラムを動かす

	GunHound( gxFalse );

	//重ねて時計を表示する

	GHClock();

	return gxTrue;
}

gxBool GamePause()
{
	return gxTrue;
}

void ReleaseGH();

gxBool GameEnd()
{
	ReleaseGH();
	return gxTrue;
}

gxBool GameSleep()
{
	return gxTrue;
}

gxBool GameResume()
{
	return gxTrue;
}

gxBool GameReset()
{
	//Visorizer( gxTrue );

	return gxTrue;
}

gxBool DragAndDrop(char* szFileName)
{
	return gxTrue;
}

