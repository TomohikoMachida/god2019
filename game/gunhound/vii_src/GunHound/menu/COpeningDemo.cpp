//--------------------------------------------
//
//  ・ｽI・ｽ[・ｽv・ｽj・ｽ・ｽ・ｽO・ｽf・ｽ・ｽ・ｽ・ｽ・ｽ・ｽp
//
//--------------------------------------------

#include <game/gunvalken.h>
#include "COpeningDemo.h"

enum {
	enSeqOpeningNone,
	enSeqLoadingFinish,
	enSeqWaitGame,
	enSeqFadeFinish,

	enAdvertiseWaitMax = (60*120)/2,
};

COpeningDemo::COpeningDemo()
{
	m_sTimer = 0;
	m_bEnd = gxFalse;

	m_sSequence       = 0;
	m_sOperationIndex = enOperationNone;

	m_sAlpha = 0x00;

	m_fAlpha = 0.0f;

	m_bScreenDisp1 = gxFalse;
	m_bScreenDisp2 = gxFalse;
	m_sDemoCount = 0;
}



COpeningDemo::~COpeningDemo()
{
	
}


void COpeningDemo::Action()
{

	switch(m_sSequence){
	case 0:
		m_sSequence = 100;
		m_sOperationIndex = enOperationInit;
		break;

	case 100:
		m_sOperationIndex = enOperationMain;
		m_sTimer ++;
//		if( m_sTimer >= enAdvertiseWaitMax/10 )
		if( m_sTimer >= enAdvertiseWaitMax )
		{
			m_sSequence = 200;
		}
		else if( Joy[0].psh )
		{
			m_sSequence = 900;
		}
		break;

	case 200:
		m_sAlpha += 3;
		if( m_sAlpha > 255 )
		{
			m_fAlpha = 0.0f;
			m_sDemoCount = 0;
			m_sAlpha    = 255;
			m_sSequence = 300;

			static int demoLoop = 0;
			if( demoLoop%2 == 0 )
			{
				m_bScreenDisp1 = gxTrue;
			}
			else
			{
				m_bScreenDisp2 = gxTrue;
			}
			demoLoop ++;
		}
		break;

	case 300:
		//操作説明Demo1
		m_fAlpha += ( 1.0f - m_fAlpha )/20.0f;
		m_fAlpha += 0.01f;
		if( m_fAlpha >= 1.0f )
		{
			m_fAlpha = 1.0f;
			if( m_sDemoCount >= 60*10 )
			{
				m_sSequence = 800;
			}
		}
		else if( Joy[0].psh )
		{
			m_sSequence = 800;
		}
		break;

	case 800:
		//操作説明Demo1 End
		m_fAlpha += ( 0.0f - m_fAlpha )/20.0f;
		m_fAlpha += -0.01f;
		if( m_fAlpha <= 0.0f )
		{
			m_fAlpha = 0.0f;
			m_sSequence = 900;
		}
		break;

	case 900:
		m_sOperationIndex = enOperationEnd;
		m_bEnd = gxTrue;
		break;
	}

	m_sDemoCount ++;

}

void COpeningDemo::SetStatusGameOver()
{
	if( m_sSequence == 100 )
	{
		m_sSequence = 200;
	}

}

void COpeningDemo::Draw()
{
	viiSub::MenuSprite( WINDOW_W - 100,WINDOW_H-48/*224*/,PRIO_FADEOVER, enTexPageSysLogo,272,176,208,80,104,40,ATR_DFLT,ARGB(0xf0,0xff,0xff,0xff) );

	if( m_sAlpha )
	{
		viiDraw::Box( -32, -32, WINDOW_W+64 , WINDOW_H+64 , PRIO_FADEOVER+2  , gxTrue , ARGB(m_sAlpha,0x01,0x01,0x01) );
	}

	if( gxLib::GetGameCounter()%20 < 10 )
	{
		viiDraw::sml_printf( WINDOW_W/2+(-8*11)/2 , WINDOW_H/2+64 , SET_ALPHA( 1.0f , 0xffffffff ) ,"INSERT COIN" );
	}

	int alp = 0;

	static const Sint32 texPage = (MAX_MASTERTEX_NUM-1)*16;

	if( m_bScreenDisp1 )
	{
		alp = 255*m_fAlpha;
		Float32 fAlp = m_fAlpha;
		gxLib::PutSprite( 0,0 , PRIO_FADEOVER+3 ,
			enDemoTexPage+32,0,272*0,1024,272,0,0,
			ATR_DFLT   , SET_ALPHA( fAlp , ARGB_DFLT ) , 0.f , 1.0f , 1.0f );
	}

	if( m_bScreenDisp2 )
	{
		alp = 255*m_fAlpha;
		Float32 fAlp = m_fAlpha;
		gxLib::PutSprite( 0,0 , PRIO_FADEOVER+3 ,
			enDemoTexPage+32,0,272*1,1024,272,0,0,
			ATR_DFLT   , SET_ALPHA( fAlp , ARGB_DFLT ) , 0.f , 1.0f , 1.0f );
	}

}



