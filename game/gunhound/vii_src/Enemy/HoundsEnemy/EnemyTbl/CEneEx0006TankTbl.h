enum {
 enTexPageBase = enTexCEneEx0006Tank,
};
gxSprite SprCEneEx0006Tank[]={
    {enTexPageBase+0,0,0,1,1,0,0},//ダミー
    {enTexPageBase+0,0,0,64,32,24,16},//砲台
    {enTexPageBase+0,64,0,48,32,8,18},//砲塔
    {enTexPageBase+1,0,0,0,0,0,0},//0
};
Sint32 sPosCEneEx0006Tank[][16]={
    {0,0,0,0,0,0,100,100,100,0},//シーン
    {0,0,0,0,0,0,100,100,100,1},//砲台
    {-5,-13,-1,0,0,0,100,100,100,2},//砲塔
    {0,0,0,0,0,0,0,0,0,3},//0
};
enum {
    enPARENT,
    enHOUDAI,
    enHOUTOU,
    enMax,
    
};
Sint32 m_sParentCEneEx0006Tank[]={
    -1,
    enPARENT,
    enHOUDAI,
    
};
