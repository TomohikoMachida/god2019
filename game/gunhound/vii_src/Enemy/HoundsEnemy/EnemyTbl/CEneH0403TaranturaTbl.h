enum {
 enTexPageBase = enTexCEneH0403Tarantura,
};
gxSprite SprCEneH0403Tarantura[]={
    {enTexPageBase+0,0,0,1,1,0,0},//ダミー
    {enTexPageBase+0,40,112,48,40,16,16},//胴体
    {enTexPageBase+0,0,112,40,32,32,16},//背中
    {enTexPageBase+0,88,112,16,48,8,4},//右脚
    {enTexPageBase+0,112,112,16,48,8,44},//左脚
    {enTexPageBase+0,136,112,16,24,8,4},//右手
    {enTexPageBase+0,152,112,16,24,8,20},//左手
    {enTexPageBase+0,0,152,24,8,4,4},//砲台１
    {enTexPageBase+0,0,168,24,8,4,4},//砲台２
    {enTexPageBase+1,0,0,0,0,0,0},//0
};
Sint32 sPosCEneH0403Tarantura[][16]={
    {160,200,0,0,0,0,100,100,100,0},//シーン
    {0,0,0,0,0,0,100,100,100,1},//胴体
    {0,3,-1,0,0,0,100,100,100,2},//背中
    {0,7,0,0,0,-20,100,100,100,3},//右脚１
    {0,-7,0,0,0,20,100,100,100,4},//左脚１
    {19,18,1,0,0,45,100,100,100,5},//右手１
    {19,-18,1,0,0,-45,100,100,100,6},//左手１
    {0,-12,1,0,0,45,100,100,100,7},//砲台１１
    {0,12,1,0,0,-45,100,100,100,8},//砲台２１
    {0,0,0,0,0,0,0,0,0,9},//0
};
enum {
    enPARENT,
    enDOUTAI,
    enSENAKA,
    enMIGIASI,
    enHIDARIASI,
    enMIGITE,
    enHIDARITE,
    enHOUDAI1,
    enHOUDAI2,
    enMax,
    
};
Sint32 m_sParentCEneH0403Tarantura[]={
    -1,
    enPARENT,
    enDOUTAI,
    enDOUTAI,
    enDOUTAI,
    enDOUTAI,
    enDOUTAI,
    enDOUTAI,
    enDOUTAI,
    
};
