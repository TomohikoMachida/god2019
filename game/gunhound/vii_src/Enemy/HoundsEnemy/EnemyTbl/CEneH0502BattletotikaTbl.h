enum {
 enTexPageBase = enTexCEneH0502Battletotika,
};
gxSprite SprCEneH0502Battletotika[]={
    {enTexPageBase+0,0,0,1,1,0,0},//ダミー
    {enTexPageBase+0,0,88,80,24,8,12},//右砲台根
    {enTexPageBase+0,88,96,32,8,0,4},//右砲台先
    {enTexPageBase+0,0,112,80,24,80,12},//左砲台根
    {enTexPageBase+0,88,120,32,8,120,4},//左砲台先
    {enTexPageBase+0,0,40,96,48,52,48},//砲台
    {enTexPageBase+1,0,0,0,0,0,0},//0
};
Sint32 sPosCEneH0502Battletotika[][16]={
    {160,200,0,0,0,0,100,100,100,0},//シーン
    {0,30,5,0,0,180,100,100,100,5},//砲台1
    {14,10,-2,0,0,0,100,100,100,1},//右砲台根1
    {-40,0,-1,0,0,0,100,100,100,2},//右砲台先1
    {-15,10,-2,0,0,0,100,100,100,3},//左砲台根1
    {40,0,-1,0,0,0,100,100,100,4},//左砲台先1
    {0,0,0,0,0,0,0,0,0,6},//0
};
enum {
    enPARENT,
    enHOUDAI,
    enMIGIHOUDAI1,
    enMIGIHOUDAI2,
    enHIDARIHOUDAI1,
    enHIDARIHOUDAI2,
    enMax,
    
};
Sint32 m_sParentCEneH0502Battletotika[]={
    -1,
    enPARENT,
    enHOUDAI,
    enMIGIHOUDAI1,
    enHOUDAI,
    enHIDARIHOUDAI1,
    
};
