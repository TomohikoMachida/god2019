//------------------------------------------------------------
//
// 新型ライブラリとのつじつま合わせコード
//
//------------------------------------------------------------

#include <game/gunvalken.h>

//---------------------------------------------
//つじつま合わせ用
JOYSTICK_T Joy[enPlayerMax];
ButtonConfig_t undoConfig;

typedef struct StLocaValue {
	char LocalFilePath[255];
	Sint32 val[32];
}StLocaValue;

StLocaValue g_LocalValue;
//グローバル変数

ghLib::SAVEDATA_T g_StSaveData;

Float32 gxAbs( Float32 n )
{
	return ABS( n );
}
Sint32  gxAbs( Sint32 n )
{
	return ABS( n );
}

//テクスチャファイル読み込み（BMP or TGA）
gxBool LoadTexture(int pg , gxChar *pBmpfilename,int col)
{
	return gxLib::LoadTexture( pg , pBmpfilename , col );
}

gxBool ReadTexture(int pg, Uint8 *pMemory,Uint32 sz,int col)
{
	return gxLib::ReadTexture( pg , pMemory, sz , col );
}

gxBool UploadTexture(bool force_reading )
{
	return gxLib::UploadTexture( force_reading );
}

//スプライト表示
int PutSpriteDX(
	int x,		int y,	int prio,
	int page,	int u, 	int v,	int w,	int h,
	 //オプション
	 int cx,	int cy,
	 int atr,	 unsigned long col,
	 float rot, float scl_x,float scl_y
	)
{
	return gxLib::PutSprite(
		x , y , prio ,
		page , u , v , w ,h ,cx , cy,
		atr , col , rot , scl_x , scl_y );
}

//ポリゴングラフィック表示
int PutGraphics(
	 int type,
	 int x,	int y,	int prio,
	 GRA_POS_T *pos,
	 //オプション
	  int atr,
	  float rot,
	  float scl_x,float scl_y
	 )
{
	//GRA_BOXF
	gxLib::DrawColorBox(
		pos[0].x +x, pos[0].y+y ,pos[0].col,
		pos[1].x +x, pos[1].y+y ,pos[1].col,
		pos[2].x +x, pos[2].y+y ,pos[2].col,
		pos[3].x +x, pos[3].y+y ,pos[3].col,
		prio,
		atr
		);

	//CBellowsSpriteで使用、しかしCBellowsSpriteが使われていないので割愛
	//GRA_SPRITE

	return 0;
}

void GGX_Putsprite(gxSprite *p,int x,int y,int prio,int atr,unsigned long col,float scl,float rot)
{
	//-------------------------
	//スプライトを描画する
	//-------------------------

	PutSpriteDX(
		x,y,prio,
		p->page,p->u,p->v,p->w,p->h,
		//オプション
		p->cx,p->cy,
		atr,
		col,
		rot,scl,scl
		);

}

void GGX_DrawPixel(int x,int y,int z,int col,int atr)
{
	//------------------------------------
	//点を描画する
	//------------------------------------

/*
	GRA_POS_T pos[4];

	pos[0].x=(float)x;
	pos[0].y=(float)y;
	pos[0].z=(float)z;
	pos[0].col=col;
*/
	//PutGraphics( GRA_PIXEL,0,0,z,pos,atr);
	gxLib::DrawPoint( x , y , z , atr , col );

}


void GGX_DrawBox(int x1,int y1,int x2,int y2,int z,gxBool flag,int col,int atr)
{
	//------------------------------------
	//四角い箱を描画
	//flag (true=BOXFILL / false=BOX)
	//------------------------------------
/*
	GRA_POS_T pos[4];

	pos[0].x=(float)x1;
	pos[0].y=(float)y1;
	pos[0].z=(float)z;
	pos[0].col=col;

	pos[1].x=(float)x2;
	pos[1].y=(float)y1;
	pos[1].z=(float)z;
	pos[1].col=col;

	pos[2].x=(float)x2;
	pos[2].y=(float)y2;
	pos[2].z=(float)z;
	pos[2].col=col;

	pos[3].x=(float)x1;
	pos[3].y=(float)y2;
	pos[3].z=(float)z;
	pos[3].col=col;

	if(flag){
		PutGraphics( GRA_BOXF,0,0,z,pos,atr);
	}else{
		PutGraphics( GRA_BOX,0,0,z,pos,atr);
	}
*/

	gxLib::DrawBox( x1 , y1 , x2 , y2 , z , flag , atr , col );
}


void GGX_DrawTriangle(int x1,int y1,int x2,int y2,int x3,int y3,int z,int col,int atr)
{
	//------------------
	//三角を描画する
	//------------------
/*
	GRA_POS_T pos[4];

	pos[0].x=(float)x1;
	pos[0].y=(float)y1;
	pos[0].z=(float)z;
	pos[0].col=col;

	pos[1].x=(float)x2;
	pos[1].y=(float)y2;
	pos[1].z=(float)z;
	pos[1].col=col;

	pos[2].x=(float)x3;
	pos[2].y=(float)y3;
	pos[2].z=(float)z;
	pos[2].col=col;

	PutGraphics( GRA_TRIANGLE,0,0,z,pos,atr);
*/

	gxLib::DrawTriangle( x1 , y1 , x2 , y2 , x3 , y3 ,z , gxFalse , atr , col );

}


void GGX_DrawLine(int x1,int y1,int x2,int y2,int z,int col,int atr)
{
	//------------------
	//線を描画する
	//------------------
/*
	GRA_POS_T pos[4];

	pos[0].x=(float)x1;
	pos[0].y=(float)y1;
	pos[0].z=(float)z;
	pos[0].col=col;

	pos[1].x=(float)x2;
	pos[1].y=(float)y2;
	pos[1].z=(float)z;
	pos[1].col=col;

	PutGraphics( GRA_LINE,0,0,z,pos,atr);
*/
	gxLib::DrawLine( x1 , y1 , x2 , y2 , z , atr , col );
}

/*
void GGX_DrawBoxGradation(int x1,int y1,int x2,int y2,int z,int atr,int col1,int col2,int col3,int col4)
{
	//--------------------------------------------
	//特殊グラデBOXF
	//--------------------------------------------
	GRA_POS_T pos[4];
	pos[0].x=x1*1.f;
	pos[0].y=y1*1.f;
	pos[0].z=z*1.f;
	pos[0].col=col1;

	pos[1].x=x2*1.f;
	pos[1].y=y1*1.f;
	pos[1].z=z*1.f;
	pos[1].col=col2;

	pos[2].x=x2*1.f;
	pos[2].y=y2*1.f;
	pos[2].z=z*1.f;
	pos[2].col=col3;

	pos[3].x=x1*1.f;
	pos[3].y=y2*1.f;
	pos[3].z=z*1.f;
	pos[3].col=col4;

	PutGraphics( GRA_BOXF,0,0,z,pos,atr);
}
*/

void msg_printf(int x,int y,int pr,char *msg,...)
{
	gxLib::Printf( x , y , pr , ATR_DFLT , ARGB_DFLT , msg );
}
void msg_printfwithCol(int x,int y,int pr,unsigned int col,char *msg,...)
{
	gxLib::Printf( x , y , pr , ATR_DFLT , col , msg );
}
int PutStringDX(int x,int y,int prio,char *msg,unsigned long col)
{
	return 0;
}
void dbg_printf(char *msg,...)
{
	gxLib::DebugLog( msg );
}

gxBool ghLib::IsFullScreen()
{
	return gxFalse;
	//return !GGA.ModeWindow;
}

gxBool ghLib::ChangeWindowMode( gxBool bFlag )
{
	//ChangeScreenMode( GGA.windowSize );

	return gxTrue;
}


Sint32& ghLib::Value(Sint32 n)
{
	return	g_LocalValue.val[n];
}
/*

char* ghLib::GetLocalFilePath()
{

	return g_LocalValue.LocalFilePath;

}


void ghLib::SetLocalFilePath(char *p)
{

	//CWin32tool::GetFilePath( p, &g_LocalValue.LocalFilePath[0] );

}

*/

gxBool ghLib::SaveConfig()
{
	//セーブデータを保存する

	//WriteFile( _FILENAME_ENVIRONMENT_ ,(char *)&g_StSaveData,sizeof(gxLib::SAVEDATA_T));

	return gxTrue;

}


gxBool ghLib::LoadConfig()
{
	//セーブデータを読み込む

	Uint8 *pData;
	Uint32 uSize;

	pData = gxLib::LoadFile( _FILENAME_ENVIRONMENT_ , &uSize );

	if( pData == NULL )
	{
		SetConfigDefault();
		return gxFalse;
	}

	memcpy( &g_StSaveData , pData , uSize );

	delete[] pData;

/*
	if(ReadFile( _FILENAME_ENVIRONMENT_ ,(char*)&g_StSaveData,sizeof(gxLib::SAVEDATA_T)) < 0)
	{
		gxLib::SetConfigDefault();
		return gxFalse;
	}
*/

	return gxTrue;
}

gxBool ghLib::SetConfigDefault()
{
	//メモリ中のセーブデータをデフォルトにする

	g_StSaveData.windowSize = 0;
	g_StSaveData.version_no = 0;
	g_StSaveData.wideScreen = 0;
	//gxLib::SetDefaultPadCongig();

	memset( &g_StSaveData.free[0] , 0x00, sizeof(Uint32)*ghLib::enSaveDataFreeArea );

	return gxTrue;
}


void ghLib::SetDefaultPadCongig()
{
	//--------------------------------------------
	//デフォルトのパッドデータを設定する
	//--------------------------------------------
	
	for(int i=0;i<16;i++)
	{
		for(int j=0;j<16;j++)
		{
			g_StSaveData.BtnConfig.enable[i][j] = 1;	//all usable
		}
	}

	g_StSaveData.BtnConfig.bUseController = 0;	//使用しない

	for(int ii=0;ii<enPlayerMax;ii++)
	{
		g_StSaveData.BtnConfig.useDevice[ii] = 0;
		for(int iii=0;iii<32;iii++)
		{
			g_StSaveData.BtnConfig.assign[ii][iii] = -1;
		}
	}

	undoConfig = g_StSaveData.BtnConfig;

}


void ghLib::SetRumble(int player,int time,int x,int y)
{
	//--------------------------------------------
	//振動を設定する
	//--------------------------------------------

	gxLib::SetRumble( player , 0 , time );//, x , y );

}

Sint32 ghLib::GetJoypadDir(int pl,int mode)
{
	//--------------------------
	//入力方向を返す。
	//--------------------------

	int j,dir=0;

	if(mode == JOY_PUSH){
		j=Joy[pl].psh&(JOY_U|JOY_L|JOY_R|JOY_D);
	}
	if(mode == JOY_TRIG){
		j=Joy[pl].psh&(JOY_U|JOY_L|JOY_R|JOY_D);
	}

	switch(j){
	case JOY_R:
		dir = 3;
		break;
	case JOY_D:
		dir = 5;
		break;
	case JOY_L:
		dir = 7;
		break;
	case JOY_U:
		dir = 1;
		break;

	case JOY_R|JOY_D:
		dir = 4;
		break;
	case JOY_D|JOY_L:
		dir = 6;
		break;
	case JOY_L|JOY_U:
		dir = 8;
		break;
	case JOY_U|JOY_R:
		dir = 2;
		break;
	}

	return dir;
}

//----------------------------------------------------
//
//サウンド関連のラッパー
//
//----------------------------------------------------
int ghLib::LoadWaveFile(long no, char *file)	//SoundLoad
{
	return gxLib::LoadAudio(no,file);
}

int ghLib::ReadWaveData(long no, unsigned char *pWaveBuf , Uint32 uSize )	// SoundLoadFromMemory
{
	return 	gxLib::ReadAudio(no, pWaveBuf, uSize);
}

void ghLib::PlayWave(Sint32 no,gxBool bLoop,Sint32 vol,Uint64 current )	//SoundPlay
{
	gxLib::PlayAudio( no , bLoop );//,vol,current);
}

void ghLib::StopWave( Sint32 no , Sint32 frm )	//SoundStop
{
	gxLib::StopAudio( no , frm );
}

/*
//void ghLib::SetMasterVolume(int vol,int pan)	//SoundMasterControl
//{
//	gxLib::SetAudioMasterVolume( vol );//, pan );
//}
*/
/*
//Uint32 ghLib::GetPlayPosition(Sint32 no )	//SoundGetPosion
//{
//	return 0;
////	return SoundGetPosion( no );
//}
//
//Uint32 ghLib::GetSoundLength(Sint32 no )	//SoundGetLength
//{
//	return 0;
////	return SoundGetLength( no );
//}
*/
/*
//StSoundInfo* ghLib::GetWaveInfo(Sint32 no)	//GetSoundInfo
//{
//	return GetSoundInfo( no );
//}
*/


void ghLib::SetSoundVolume(int no , int vol )	//SoundVolumeControl
{
	gxLib::SetAudioVolume( no , vol );
}

gxBool ghLib::IsSoundPlay(int n)	//SoundIsPlay
{
	return gxLib::IsPlayAudio( n );
}

Sint32 ghLib::GetMouseWheelNum(Sint32 sMax)
{
	//マウスのホイール現在値をーn～＋nの間で返す
	//Sint32 sCnt = CPadControl::GetInstance()->Mouse._WheelCnt.y;

	//if( sCnt >=  sMax ) sCnt =  sMax;
	//if( sCnt <= -sMax ) sCnt = -sMax;

	//CPadControl::GetInstance()->Mouse._WheelCnt.y = sCnt;

	//	return sCnt;

	return 0;
}

void ghLib::AdjustControl()
{
	
	Joy[0].psh       = gxLib::Joy(0)->psh;
	Joy[0].trg       = gxLib::Joy(0)->trg;
	Joy[0].rep       = gxLib::Joy(0)->rep;
	Joy[0].analog_x  = gxLib::Joy(0)->lx;
	Joy[0].analog_y  = gxLib::Joy(0)->ly;
	Joy[0].analog_z  = 0;
	Joy[0].analog_rx = gxLib::Joy(0)->rx;
	Joy[0].analog_ry = gxLib::Joy(0)->ry;
	Joy[0].analog_rz = 0;
	Joy[0].mx        = gxLib::Joy(0)->mx;
	Joy[0].my        = gxLib::Joy(0)->my;

#if 0
	Joy[0].dragnow;
	Joy[0].drag1;	//ドラッグ開始位置
	Joy[0].drag2;	//ドラッグ終了位置
	Joy[0].scr;	//スクロールポジション

	Sint16	x, y;		/* 方向スティックの変量. x,y は -0x8000～0～0x7fff になる ※mouseのときは座標*/
	Sint16	cx, cy;		/* mm系でx,yを求めるための変数. ※mouseのときは現在処理中の座標 */
	long	dw,dh;		/* 〃						*/
	char name[0xff];
	Sint32 stick_num;
	Sint32 repeat[20];
#endif
}


const Uint32 crctable[256] = {
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
	0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
	0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
	0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
	0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,
	0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
	0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
	0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
	0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
	0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
	0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,
	0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683,
	0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
	0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
	0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
	0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236, 0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
	0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
	0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,
	0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
	0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
	0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
	0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
	0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
	0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94, 0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d,
};

Uint32 ghLib::CalcCrc32(void *dat, int siz)
{
	Uint8  *s = (Uint8*)dat;
	Uint32 r;

	r = 0xFFFFFFFFUL;
	while (--siz >= 0)
		r = (r >> 8) ^ crctable[( (Uint8) r) ^ *s++];

	Uint32 crc=(int) (r ^ 0xFFFFFFFFUL);

	return crc;
}


