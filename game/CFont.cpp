#include <gxLib.h>
#include <gxLib/util/Font/CFontManager.h>
#include <gxLib/util/CCsv.h>
#include <gxLib/gxTexManager.h>

class CFont
{


public:
	enum {
		enTempLength = 1024,
	};

	//UTF16で扱う新型フォントルーチン
	CFont()
	{

	}

	gxBool MakeKNJ2( Sint32 sFontPixel , gxChar* textFile );

	gxBool LoadFNT( Sint32 tpg , gxChar *pFileName );

	void Draw();
	void CFont::Printf(Sint32 x, Sint32 y, Sint32 prio, Uint32 atr, Uint32 argb, gxChar* str, ...);

	SINGLETON_DECLARE(CFont);

private:

	Sint32 getWidth(wchar_t *str, size_t len);

		struct info {
		Sint8   half;
		Sint8   pg,u,v;
		Sint8   t,l,r,b;
		Sint8   ox,oy;
	};

	std::map<wchar_t, info> listUV;
	Uint8 m_PixelNum = 32;
	Sint32 m_TexStartPage = 0;

	gxChar m_Temp[enTempLength];

	Float32 m_fScale = 1.0f;
	Float32 m_fRotation = 0.0f;
	Float m_fWidth = 16.0f;
	Uint32 m_ShadowARGB  = 0xFF808080;
	Uint32 m_OutlineARGB = 0xFF00FF00;
};


gxBool CFont::LoadFNT( Sint32 tpg , gxChar *pFileName )
{
	gxChar filePath[1024] = { 0 };
	gxChar fileName[1024] = { 0 };
	gxChar fileOnly[1024] = { 0 };

	gxUtil::GetFileNameWithoutPath(pFileName, fileName);
	gxUtil::GetFileNameWithoutExt(fileName, fileOnly);
	gxUtil::GetPath(pFileName, filePath);

	CCsv csv;
	
	csv.LoadFile(pFileName);
	std::string str[32];
	listUV.clear();
	for (Sint32 ii = 0; ii < csv.GetHeight(); ii++)
	{
		for (Sint32 jj = 0; jj < 32; jj++)
		{
			csv.GetCell(jj, ii, str[jj]);
		}

		if (str[0] == "tex")
		{
			std::string path = filePath;
			if (path != "") path += '/';
			path += str[2];

			gxLib::LoadTexture( tpg + atoi(str[1].c_str()) , (gxChar*)path.c_str() );
		}
		else if (str[0] == "pixel")
		{
			m_PixelNum = atoi(str[1].c_str());
		}
		else
		{
			wchar_t word = atoi(str[0].c_str());
			info* p = &listUV[word];

			p->half = atoi(str[1].c_str());
			p->pg   = atoi(str[2].c_str());
			p->u    = atoi(str[3].c_str());
			p->v    = atoi(str[4].c_str());
			p->t    = atoi(str[5].c_str());
			p->l    = atoi(str[6].c_str());
			p->r    = atoi(str[7].c_str());
			p->b    = atoi(str[8].c_str());
			p->ox   = atoi(str[9].c_str());
			p->oy   = atoi(str[10].c_str());
		}
	}

	gxLib::UploadTexture();

	return gxTrue;
}

gxBool CFont::MakeKNJ2( Sint32 pixel , gxChar* textFile )
{
	//ファイルパス解析
	Sint32 pageHeight = 2048;

	m_PixelNum = pixel;

	gxChar filePath[1024] = { 0 };
	gxChar fileName[1024] = { 0 };
	gxChar fileOnly[1024] = { 0 };

	gxUtil::GetFileNameWithoutPath(textFile, fileName);
	gxUtil::GetFileNameWithoutExt(fileName, fileOnly);
	gxUtil::GetPath(textFile,filePath);

	//テキストデータ解析

	uint32_t uSize;
	Uint8* pData = gxLib::LoadFile( textFile , &uSize );
	wchar_t *p16 = (wchar_t*)&pData[2];
	uSize -= 2;
	uSize = uSize / 2;

	Sint32 x=0,y=0;
	Sint32 maxx = 0;
	Sint32 maxy = 0;

	for(Sint32 ii=0; ii<uSize; ii++ )
	{
		switch(p16[ii]){
		case 0x000D:
			y++;
			maxy ++;
		case 0x000A:
			if( x >= maxx ) maxx = x;
			x = 0;
			break;
		default:
			listUV[ p16[ii] ].pg = 0;
			listUV[ p16[ii] ].u  = x;
			listUV[ p16[ii] ].v  = y%(2046/pixel);
			x ++;
			break;
		}
	}

	//登録文字数
	size_t num = listUV.size();

	//テクスチャの読み込み
	Sint32 w = maxx*pixel;
	Sint32 h = maxy*pixel;

	Sint32 texMax = h/pageHeight;
	texMax += (h % pageHeight) ? 1 : 0;

	for( Sint32 ii=0; ii<texMax; ii++ )
	{
		char buf[32];
		sprintf(buf,"%02d" , ii+1 );
		std::string tgaName = filePath;
		if( tgaName != "" ) tgaName += '/';
		tgaName += fileOnly;
		tgaName += +buf;
		tgaName += +".tga";
		gxLib::LoadTexture(ii * 64, (gxChar*)tgaName.c_str());
		gxLib::UploadTexture();
	}

	//自詰めの位置を検出する

	for( auto itr=listUV.begin(); itr != listUV.end(); ++itr )
	{
		info* p = &itr->second;

		if (itr->first == 0x30fb)
		{
			int nnn = 0;
			nnn++;
		}
		CFileTarga *pTga;
		pTga = gxTexManager::GetInstance()->GetAtlasTexture(0);

		Sint32 ax,ay;
		Uint32 argb;
		Uint32 l=-1,r=-1,t=-1,b=-1;
		for( Sint32 y=0; y<pixel; y++ )
		{
			for( Sint32 x=0; x<pixel; x++ )
			{
				ax = p->u*pixel;
				ay = p->v*pixel;
				argb = pTga->GetARGB(ax+x,ay+y);
				if( argb )
				{
					if( x < l || l == -1 ) l = x;
					if( x > r || r == -1 ) r = x;
					if( y < t || t == -1 ) t = y;
					if( y > b || b == -1 ) b = y;
				}
			}
		}

		//p->t = 1.0f * t / pixel;
		//p->l = 1.0f * l / pixel;
		//p->r = 1.0f * r / pixel;
		//p->b = 1.0f * b / pixel;

		p->t = t;
		p->l = l;
		p->r = r;
		p->b = b;
		p->ox = 0;//.0f;
		p->oy = 0;//.0f;

		if (t == b || r == l)
		{
			p->t = pixel/2-8;//0.5f - 0.1f;
			p->b = pixel/2-8;//0.5f + 0.1f;
			p->l = pixel/2+8;//0.5f - 0.1f;
			p->r = pixel/2+8;//0.5f + 0.1;
		}

	}

	//KNJファイルを生成する
	CCsv csv;
	y = 0;
	char* pStr;

	for (Sint32 ii = 0; ii < texMax; ii++)
	{
		char buf[32];
		sprintf(buf, "%02d", ii + 1);
		std::string tgaName = "";
		tgaName += fileOnly;
		tgaName += +buf;
		tgaName += +".tga";
		csv.SetCell(0, y, "tex");
		csv.SetCell(1, y, "%d",ii);
		csv.SetCell(2, y, "%s",tgaName.c_str());
		y++;
	}

	csv.SetCell(0, y, "pixel");
	csv.SetCell(1, y, "%d",m_PixelNum);
	y++;

	for( auto itr=listUV.begin(); itr != listUV.end(); ++itr )
	{
		info* p = &itr->second;
		wchar_t str[2] = { itr->first,0x00 };
		pStr = CDeviceManager::UTF16toUTF8(str);
		csv.SetCell(0 ,y,"%d",itr->first);
		csv.SetCell(1 ,y,"%d",p->half );
		csv.SetCell(2 ,y,"%d",p->pg );
		csv.SetCell(3 ,y,"%d",p->u );
		csv.SetCell(4 ,y,"%d",p->v );
		csv.SetCell(5 ,y,"%d",p->t );
		csv.SetCell(6 ,y,"%d",p->l );
		csv.SetCell(7 ,y,"%d",p->r );
		csv.SetCell(8 ,y,"%d",p->b );
		csv.SetCell(9 ,y,"%d",p->ox );
		csv.SetCell(10,y,"%d",p->oy );
		csv.SetCell(11,y,"%s",pStr);
		SAFE_DELETES(pStr);
		y ++;
	}

	std::string tgaName = filePath;
	if (tgaName != "") tgaName += '/';
	tgaName += fileOnly;
	tgaName += +".fnt";
	csv.SaveFile( tgaName.c_str());

	SAFE_DELETES(pData);

	return gxTrue;
}


void CFont::Draw()
{
	#if 0
	va_list app;
	va_start(app, str);
	vsprintf(m_Temp, str, app);
	va_end(app);

	wchar_t* p16 = CDeviceManager::UTF8toUTF16(m_Temp);
	wchar_t* text = p16;
	size_t len = wcslen(text);

	Sint32 xx = _x;
	Sint32 yy = _y;


	Float32 fScale = 0.5f;

	Sint32 width = 0;
	width = getWidth(&text[0], len) * fScale;
	xx = _x - width;		//みぎつめ
	//xx = _x - width/2;	//センタリング

	if (atr & STR_OUTLINE)
		if (atr & STR_OUTLINE)
			for (Sint32 ii = 0; ii < len; ii++)
			{
				if (*text == 0x00)
				{
					continue;
				}

				auto itr = listUV.find(*text);

				if (itr != listUV.end())
				{
					xx -= itr->second.l * fScale;

					Sint32 u, v;
					u = itr->second.u * m_PixelNum;
					v = itr->second.v * m_PixelNum;

					Sint32 tpg = m_TexStartPage + itr->second.pg;
					Sint32 ax, ay;

					ax = xx;
					ay = yy;

					//ay -= itr->second.t*fScale;	//ウエツメ
					//ay -= itr->second.b*fScale;	//シタヅメ

					gxLib::PutSprite(ax, ay, prio, tpg, u, v, m_PixelNum, m_PixelNum, 0, 0, ATR_DFLT, 0xffffffff, 0.0f, fScale, fScale);
					/*
								gxLib::DrawBox(ax, ay, ax + m_PixelNum, ay + m_PixelNum, prio, gxFalse, ATR_DFLT, 0xffffff00);
								gxLib::DrawPoint(ax + itr->second.l, ay + itr->second.t, prio, ATR_DFLT, 0xffffff00, 3.0f);
								gxLib::DrawPoint(ax + itr->second.r, ay + itr->second.t, prio, ATR_DFLT, 0xffffff00, 3.0f);
								gxLib::DrawPoint(ax + itr->second.l, ay + itr->second.b, prio, ATR_DFLT, 0xffffff00, 3.0f);
								gxLib::DrawPoint(ax + itr->second.r, ay + itr->second.b, prio, ATR_DFLT, 0xffffff00, 3.0f);
					*/
					xx += (itr->second.r) * fScale;
				}
				else
				{
					xx += m_PixelNum;
				}

				text++;
			}

	SAFE_DELETES(p16);
#endif
}


Sint32 CFont::getWidth(wchar_t *str ,size_t len )
{
	Sint32 xx = 0;

	for (Sint32 ii = 0; ii < len; ii++)
	{
		auto itr = listUV.find(str[ii]);

		if (itr == listUV.end()) continue;

		xx += (itr->second.r- itr->second.l);
		xx += m_fWidth;
	}
	return xx;
}

void CFont::Printf( Sint32 _x , Sint32 _y , Sint32 prio , Uint32 atr , Uint32 argb , gxChar *str , ... )
{
	va_list app;
	va_start(app, str);
	vsprintf(m_Temp, str, app);
	va_end(app);

	wchar_t* p16 = CDeviceManager::UTF8toUTF16(m_Temp);
	wchar_t* text = p16;
	size_t len = wcslen(text);

	Sint32 xx = _x;
	Sint32 yy = _y;


	Float32 fScale = m_fScale;

	Sint32 width = 0;
	width = getWidth(&text[0] , len)*fScale;
	//xx = _x - width/2;	//センタリング

	//if (atr & STR_OUTLINE)
	//if (atr & STR_OUTLINE)
	xx = _x - width;		//みぎつめ
	for (Sint32 ii = 0; ii < len; ii++)
	{
		if (*text == 0x00)
		{
			continue;
		}

		auto itr = listUV.find(*text);

		if (itr != listUV.end())
		{
			xx -= itr->second.l * fScale;

			Sint32 u, v;
			u = itr->second.u * m_PixelNum;
			v = itr->second.v * m_PixelNum;

			Sint32 tpg = m_TexStartPage + itr->second.pg;
			Sint32 ax, ay;

			ax = xx;
			ay = yy;

			//ay -= itr->second.t*fScale;	//ウエツメ
			//ay -= itr->second.b*fScale;	//シタヅメ

			gxLib::PutSprite(ax + 4*m_PixelNum/64 , ay+ 4*m_PixelNum/64, prio, tpg, u, v, m_PixelNum, m_PixelNum, 0, 0, ATR_DFLT, m_ShadowARGB, 0.0f, fScale, fScale);

			for (Float32 r = 0.0f; r < 360.0f; r += 45.0f)
			{
				Float32 bx, by;
				bx = gxUtil::Cos(r) * 2.0f;
				by = gxUtil::Sin(r) * 2.0f;
				gxLib::PutSprite(ax + bx, ay + by, prio, tpg, u, v, m_PixelNum, m_PixelNum, 0, 0, ATR_DFLT, m_OutlineARGB, 0.0f, fScale, fScale);
			}


			xx += (itr->second.r) * fScale;
			xx += m_fWidth;
		}
		else
		{
			xx += m_PixelNum;
		}

		text++;
	}

	text = p16;
	xx = _x - width;		//みぎつめ
	for (Sint32 ii = 0; ii < len; ii++)
	{
		if (*text == 0x00)
		{
			continue;
		}

		auto itr = listUV.find(*text);

		if (itr != listUV.end())
		{
			xx -= itr->second.l*fScale;

			Sint32 u, v;
			u = itr->second.u * m_PixelNum;
			v = itr->second.v * m_PixelNum;

			Sint32 tpg = m_TexStartPage + itr->second.pg;
			Sint32 ax, ay;

			ax = xx;
			ay = yy;

			//ay -= itr->second.t*fScale;	//ウエツメ
			//ay -= itr->second.b*fScale;	//シタヅメ

			gxLib::PutSprite(ax, ay, prio, tpg, u, v, m_PixelNum, m_PixelNum, 0, 0, ATR_DFLT, 0xffffffff,0.0f , fScale, fScale);
/*
			gxLib::DrawBox(ax, ay, ax + m_PixelNum, ay + m_PixelNum, prio, gxFalse, ATR_DFLT, 0xffffff00);
			gxLib::DrawPoint(ax + itr->second.l, ay + itr->second.t, prio, ATR_DFLT, 0xffffff00, 3.0f);
			gxLib::DrawPoint(ax + itr->second.r, ay + itr->second.t, prio, ATR_DFLT, 0xffffff00, 3.0f);
			gxLib::DrawPoint(ax + itr->second.l, ay + itr->second.b, prio, ATR_DFLT, 0xffffff00, 3.0f);
			gxLib::DrawPoint(ax + itr->second.r, ay + itr->second.b, prio, ATR_DFLT, 0xffffff00, 3.0f);
*/
			xx += (itr->second.r) * fScale;
			xx += m_fWidth;
		}
		else
		{
			xx += m_PixelNum;
		}

		text++;
	}

	SAFE_DELETES(p16);
}


SINGLETON_DECLARE_INSTANCE(CFont);

void FontTest()
{
	CFont::GetInstance()->LoadFNT(0,"hetamoji.fnt");

}
