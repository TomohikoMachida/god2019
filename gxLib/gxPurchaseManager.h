﻿//--------------------------------------------------------
//
// 課金コンテンツ制御用
//
//--------------------------------------------------------

#ifndef _GXPURCHASEMANAGER_H_
#define _GXPURCHASEMANAGER_H_

enum PurchaseErrorCode{
	enNoError,
	enNoInternetConnection,
	enAlreadyBuy,
	enNoSell,
	enIllegalItemCode,
	enErrorAnything,
};

typedef struct StPurchaseError {
	PurchaseErrorCode  errorCode;
	gxChar* errorReason;
} StPurchaseError;

class gxPurchaseManager
{
public:

	enum {
		enDLCMax = 3,
		enDLCIncludeMax = 3,
	};

	gxPurchaseManager();
	~gxPurchaseManager();

	void Action();
	void Draw();

	//購入するコンテンツIDの登録
	gxBool SetContentsID( Uint32 uDLCItemID );

	//アイテムの購入状況を復元
	gxBool Recovery( Uint32 uDLCItemID );

	//販売しているかチェック
	gxBool IsSell( Uint32 uDLCItemID );

	//セットコンテンツの登録
	gxBool SetSubContentsID( Uint32 uParentDLCItemID , Uint32 uChildDLCItemID );

	//購入を決定する
	gxBool Buy( Uint32 uDLCItemID );

	//すでに購入しているか？
	gxBool IsBuy( Uint32 uDLCItemID );

	//値段を取得する
	gxChar* GetPrice();

	//エラーを返す
	StPurchaseError *GetError();

	SINGLETON_DECLARE( gxPurchaseManager );

private:

	

};

#endif

