﻿//------------------------------------------------
//
//
// 
//
//
//------------------------------------------------

class CSikakuEffect : public CAction
{
public:

	CSikakuEffect( Sint32 x , Sint32 y , Sint32 dir , Sint32 sPlayer , gxBool bFinal = gxFalse );
	~CSikakuEffect();
	void SeqMain();
	void SeqCrash();
	void Draw();
	void SetDir( Sint32 sDir)
	{
		m_sDir = sDir;
	}
private:

	void setAtari( Sint32 u , Sint32 v , Sint32 w, Sint32 h );

	gxPos m_Pos;
	gxPos m_Add;

	CCollision m_Atari;
	CCollision m_Kurai;

	Sint32 m_sPlayer;
	gxBool m_bFinish;
	Sint32 m_sDir;
	Sint32 m_sWait;

};



class CEffHitmark : public CAction
{
public:

	CEffHitmark( Sint32 x , Sint32 y );
	~CEffHitmark();
	void SeqMain();
	void SeqCrash();
	void Draw();

private:

	gxPos m_Pos;
	gxPos m_Add;

	Sint32 m_sAlpha;

	Float32 m_fScale;

};



class CEffBeam : public CAction
{
public:

	CEffBeam( Sint32 x , Sint32 y );
	~CEffBeam();
	void SeqMain();
	void SeqCrash();
	void Draw();

private:

	gxPos m_Pos;
	gxPos m_Add;

	Sint32 m_sAlpha;

	Float32 m_fScale;

};


class CEffPlasma : public CAction
{
public:

	CEffPlasma( Sint32 x , Sint32 y );
	~CEffPlasma();
	void SeqMain();
	void SeqCrash();
	void Draw();

private:

	gxPos m_Pos;
	gxPos m_Add;

	Sint32 m_sAlpha;

	Float32 m_fScale;

};



class CEffComboDisp : public CAction
{
public:

	CEffComboDisp( Sint32 x , Sint32 y );
	~CEffComboDisp();
	void SeqMain();
	void SeqCrash();
	void Draw();

private:

	gxPos m_Pos;
	gxPos m_Add;

	Sint32 m_sType;
	Sint32 m_sAlpha;

	Float32 m_fScale;

};



class CEffAtack : public CAction
{
public:

	CEffAtack( Sint32 x , Sint32 y , Sint32 sType=0 );
	~CEffAtack();
	void SeqMain();
	void SeqCrash();
	void Draw();

private:

	gxPos m_Pos;
	gxPos m_Add;

	Sint32 m_sAlpha;
	Sint32 m_sType;

	Float32 m_fScale;

};
