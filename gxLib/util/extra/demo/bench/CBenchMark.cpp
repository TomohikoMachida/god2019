﻿//----------------------------------------------
//
//----------------------------------------------
#include <gxLib.h>
#include "CBenchmark.h"

class CQuadManager
{
public:

	CQuadManager()
	{
		m_XNum = 20.0f;
		m_YNum = 20.0f;

		m_TotalTriangleNum = m_XNum * m_YNum * 3 ;

		m_pTriangles = new CQuad[m_TotalTriangleNum];

		m_Camera.pos.z = 3.0f;
	}

	~CQuadManager()
	{
		SAFE_DELETES( m_pTriangles );
	}

	void Init()
	{
		gxVertex vtx[4];
		Sint32 n = 0;

		Float32 w = WINDOW_W/m_XNum;
		Float32 h = WINDOW_H/m_YNum;

		Float32 ww = w/WINDOW_W;
		Float32 hh = h/WINDOW_H;

		for( Float32 y=0; y<WINDOW_H; y += h )
		{
			for( Float32 x=0; x<WINDOW_W; x += w )
			{
				Float32 x1,y1,x2,y2;
				Float32 u1,v1,u2,v2;
				Float32 px,py;
				
				u1 = x / WINDOW_W;
				v1 = y / WINDOW_H;
				u2 = (x+w) / WINDOW_W;
				v2 = (y+h) / WINDOW_H;

				x1 = -ww/2.0f;//(w/WINDOW_W)/2.0f;
				y1 = -hh/2.0f;//(h/WINDOW_H)/2.0f;
				x2 = +ww/2.0f;//(w/WINDOW_W)/2.0f;
				y2 = +hh/2.0f;//(h/WINDOW_H)/2.0f;

				px = x/WINDOW_W+ww/2;//(u1+u2)/2;
				py = y/WINDOW_H+hh/2;//(v1+v2)/2;

				vtx[0].pos.x = x1;//-px;
				vtx[0].pos.y = y1;//-py;
				vtx[0].pos.z = 0;
				vtx[0].tex.x = u1;
				vtx[0].tex.y = v1;

				vtx[1].pos.x = x2;//-px;
				vtx[1].pos.y = y1;//-py;
				vtx[1].pos.z = 0;
				vtx[1].tex.x = u2;
				vtx[1].tex.y = v1;

				vtx[2].pos.x = x1;//-px;
				vtx[2].pos.y = y2;//-py;
				vtx[2].pos.z = 0;
				vtx[2].tex.x = u1;
				vtx[2].tex.y = v2;

				vtx[3].pos.x = x2;//-px;
				vtx[3].pos.y = y2;//-py;
				vtx[3].pos.z = 0;
				vtx[3].tex.x = u2;
				vtx[3].tex.y = v2;

				m_pTriangles[n].SetVtx( n , px-0.5f , py-0.5f , vtx[0] , vtx[1] , vtx[2] , vtx[3] );
				n ++;
			}
		}
		m_TotalTriangleNum = n;
	}

	void Action()
	{

		//--------------------------------------------------
		//カメラ制御
		mtxSetUnit();


	if( gxLib::Joy(0)->lx < -0.5f )
	{
		m_Camera.pos.z -= 0.1f;
	}
	else if( gxLib::Joy(0)->ly > 0.5f )
	{
		m_Camera.pos.z += 0.1f;
	}
	if( gxLib::Joy(0)->lx < -0.5f )
	{
		m_Camera.pos.x -= 0.1f;
	}
	else if( gxLib::Joy(0)->lx > 0.5f )
	{
		m_Camera.pos.x += 0.1f;
	}

	if( gxLib::Joy(0)->ry < -0.5f )
	{
		m_Camera.rot.x += 1.0f;
	}
	else if( gxLib::Joy(0)->ry > 0.5f )
	{
		m_Camera.rot.x -= 1.0f;
	}

	if( gxLib::Joy(0)->rx < -0.5f )
	{
		m_Camera.rot.y += 1.f;
	}
	else if( gxLib::Joy(0)->rx > 0.5f )
	{
		m_Camera.rot.y -= 1.f;
	}



		VECTOR3 pos={0,0,0};
		pos -= m_Camera.pos;
		mtxTrans( &pos );
		mtxRotY( DEG2RAD(m_Camera.rot.y) );
		mtxRotX( DEG2RAD(m_Camera.rot.x) );

		m_CameraMatrix = GetCurrentMatrix();

		//--------------------------------------------------

		for( Sint32  ii=0; ii<m_TotalTriangleNum; ii ++ )
		{
			m_pTriangles[ii].Action();
		}
	}

	void Draw()
	{
		drawGround();

		for( Sint32  ii=0; ii<m_TotalTriangleNum; ii ++ )
		{
			m_pTriangles[ii].Draw();
		}
	}

	MATRIX GetCameraMatrix()
	{
		return m_CameraMatrix;
	}

private:

	void drawGround()
	{
		//地面
		Sint32 ax = WINDOW_W / 2, ay = WINDOW_H / 2;
		Sint32 z = 100;

		for (Float32 xx = -10.0f; xx < 10.0f; xx += 0.5f)
		{
			VECTOR3 p1 = { xx,1.f,-10.0f };
			VECTOR3 p2 = { xx,1.f,+10.0f };
			VECTOR3 dst1, dst2;
			mtxAffin(&dst1, &p1);
			mtxAffin(&dst2, &p2);
			dst1.x *= 1000.0f;
			dst1.y *= 1000.0f;
			dst2.x *= 1000.0f;
			dst2.y *= 1000.0f;
			gxLib::DrawLine(dst1.x + ax, dst1.y + ay, dst2.x + ax, dst2.y + ay, z, ATR_DFLT, 0xff004000);

		}

		for(Float32 yy=-10.0f; yy<=10.0f;yy+=1.0f)
		{
			VECTOR3 p1={-10.0f,1.f,-yy};
			VECTOR3 p2={+10.0f,1.f,-yy};
			VECTOR3 dst1,dst2;
			mtxAffin( &dst1 , &p1 );
			mtxAffin( &dst2 , &p2 );
			dst1.x *= 1000.0f;
			dst1.y *= 1000.0f;
			dst2.x *= 1000.0f;
			dst2.y *= 1000.0f;
			gxLib::DrawLine( dst1.x + ax , dst1.y+ay ,dst2.x + ax , dst2.y+ay , z , ATR_DFLT , 0xff004000 );

		}

	}

	Float32 m_XNum,m_YNum;
	Sint32  m_TotalTriangleNum;

	CQuad *m_pTriangles = NULL;

	gxTransform m_Camera;
	MATRIX m_CameraMatrix;

};

CQuadManager *g_pTriangleManager = NULL;

void mtxTest()
{
	Sint32 ax = WINDOW_W/2 , ay = WINDOW_H/2;
	Sint32 z = 100;

	VECTOR3 dst[3];
	VECTOR3 vtx[3]={
		{0.0,-1.f,-5.f},	{1.0f,1.f,-5.0f},	{-1.0f,1.f,-5.0f}
	};

	//カメラ

	mtxSetUnit();

	static VECTOR3 camPos={0,0,10.0f};
	static VECTOR3 camRot={0, 0, 0 };

	if( gxLib::Joy(0)->ly < -0.5f )
	{
		camPos.z -= 0.1f;
	}
	else if( gxLib::Joy(0)->ly > 0.5f )
	{
		camPos.z += 0.1f;
	}
	if( gxLib::Joy(0)->lx < -0.5f )
	{
		camPos.x -= 0.1f;
	}
	else if( gxLib::Joy(0)->lx > 0.5f )
	{
		camPos.x += 0.1f;
	}

	if( gxLib::Joy(0)->ry < -0.5f )
	{
		camRot.x += 1.0f;
	}
	else if( gxLib::Joy(0)->ry > 0.5f )
	{
		camRot.x -= 1.0f;
	}

	if( gxLib::Joy(0)->rx < -0.5f )
	{
		camRot.y += 1.f;
	}
	else if( gxLib::Joy(0)->rx > 0.5f )
	{
		camRot.y -= 1.f;
	}

	gxLib::Printf( 32,32+32*0,255 , ATR_DFLT , 0xffffffff , "CamPos( %.2f , %.2f )", camPos.x , camPos.y );
	gxLib::Printf( 32,32+32*1,255 , ATR_DFLT , 0xffffffff , "CamRot( %.2f , %.2f )", camRot.x , camRot.z );

	VECTOR3 pos={0,0,0};

	pos -= camPos;
	mtxTrans( &pos );

	mtxRotY( DEG2RAD(camRot.y) );
	mtxRotX( DEG2RAD(camRot.x) );

	//地面

	for (Float32 xx = -10.0f; xx < 10.0f; xx += 0.5f)
	{
		VECTOR3 p1 = { xx,1.f,-10.0f };
		VECTOR3 p2 = { xx,1.f,+10.0f };
		VECTOR3 dst1, dst2;
		mtxAffin(&dst1, &p1);
		mtxAffin(&dst2, &p2);
		dst1.x *= 1000.0f;
		dst1.y *= 1000.0f;
		dst2.x *= 1000.0f;
		dst2.y *= 1000.0f;
		gxLib::DrawLine(dst1.x + ax, dst1.y + ay, dst2.x + ax, dst2.y + ay, z, ATR_DFLT, 0xff004000);

	}

	for(Float32 yy=-10.0f; yy<=10.0f;yy+=1.0f)
	{
		VECTOR3 p1={-10.0f,1.f,-yy};
		VECTOR3 p2={+10.0f,1.f,-yy};
		VECTOR3 dst1,dst2;
		mtxAffin( &dst1 , &p1 );
		mtxAffin( &dst2 , &p2 );
		dst1.x *= 1000.0f;
		dst1.y *= 1000.0f;
		dst2.x *= 1000.0f;
		dst2.y *= 1000.0f;
		gxLib::DrawLine( dst1.x + ax , dst1.y+ay ,dst2.x + ax , dst2.y+ay , z , ATR_DFLT , 0xff004000 );

	}

	//三角

	mtxAffin( &dst[0] , &vtx[0] );
	mtxAffin( &dst[1] , &vtx[1] );
	mtxAffin( &dst[2] , &vtx[2] );

	Float32 fLength = 1000.0f;
	Float32 x1 = ax + dst[0].x*fLength;
	Float32 y1 = ay + dst[0].y*fLength;
	Float32 x2 = ax + dst[1].x*fLength;
	Float32 y2 = ay + dst[1].y*fLength;
	Float32 x3 = ax + dst[2].x*fLength;
	Float32 y3 = ay + dst[2].y*fLength;

	gxLib::DrawTriangle( x1 , y1 ,x2, y2, x3 , y3 , z , gxTrue , ATR_DFLT , 0xff401010 );
	gxLib::DrawLine( x1 , y1 ,x2, y2, z , ATR_DFLT , 0xff00ff00 );
	gxLib::DrawLine( x2 , y2 ,x3, y3, z , ATR_DFLT , 0xff00ff00 );
	gxLib::DrawLine( x3 , y3 ,x1, y1, z , ATR_DFLT , 0xff00ff00 );

	//原点

	mtxSetUnit();
	VECTOR3 pos2 ={0,0,0};
	pos2 += pos;

	VECTOR3 dst2  ={0,0,0};

	mtxAffin( &dst2 , &pos2 );
	dst2.x *= 1000.0f;
	dst2.y *= 1000.0f;

	dst2.x += ax;
	dst2.y += ay;

	gxLib::DrawBox( dst2.x-8 , dst2.y-8 ,dst2.x+8, dst2.y+8, z , gxTrue , ATR_DFLT , 0xff00ff00 );
}


MATRIX GetCameraMatrix()
{
	return g_pTriangleManager->GetCameraMatrix();
}


void BenchMark()
{

	static CTriangle *pTriangle = NULL;

	if(pTriangle == NULL )
	{
		pTriangle = new CTriangle();
		VECTOR3 vtx[3] = {
			{ 0.0f   , -100.0f , 0.0f},
			{+100.0f , +100.0f , 0.0f},
			{-100.0f , +100.0f , 0.0f},
		};

		pTriangle->SetVertex( &vtx[0] , &vtx[1] , &vtx[2] );
	}

	pTriangle->Action();
	pTriangle->Draw();

	return;

	if( g_pTriangleManager == NULL )
	{
		gxLib::LoadTexture( 0 , "saturn.png" , 0xff00ff00 );
		gxLib::UploadTexture();
		g_pTriangleManager = new CQuadManager();
		g_pTriangleManager->Init();
	}

	g_pTriangleManager->Action();
	g_pTriangleManager->Draw();
}

