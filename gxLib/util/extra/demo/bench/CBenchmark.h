﻿#ifndef _CBENCHMARK_H_
#define _CBENCHMARK_H_

#include <gxLib/util/gxMatrix.h>

MATRIX GetCameraMatrix();

typedef struct gxVertex
{
public:

	VECTOR3 pos;
	VECTOR3 tex;

private:

} gxVertex;

typedef struct gxTransform {
	gxTransform()
	{
		VECTOR3 vec = {0.f,0.f,0.f};
		pos = vec;
		rot = vec;
		scl   = { 1.f, 1.f, 1.f };
	}
	
	VECTOR3 pos;
	VECTOR3 rot;
	VECTOR3 scl;

} gxTransform;

class CTriangle
{
public:

	void SetVertex( VECTOR3 *p1 , VECTOR3 *p2 , VECTOR3 *p3 )
	{
		vtx[0] = *p1;
		vtx[1] = *p2;
		vtx[2] = *p3;

		SetVertexColor( 0xFFFFFFFF , 0xFFFFFFFF , 0xFFFFFFFF );
	}

	void SetVertexColor( Uint32 argb1 , Uint32 argb2 , Uint32 argb3 )
	{
		argb[0] = argb1;
		argb[1] = argb2;
		argb[2] = argb3;
	}

	void Action()
	{
		if( gxLib::Joy(0)->psh&JOY_L ) transform.rot.y -= 1.0f;
		if( gxLib::Joy(0)->psh&JOY_R ) transform.rot.y += 1.0f;
		if( gxLib::Joy(0)->psh&JOY_U ) transform.rot.x -= 1.0f;
		if( gxLib::Joy(0)->psh&JOY_D ) transform.rot.x += 1.0f;

		//transform.rot.z += 0.1f;
		//transform.scl.x += 0.1f;

		mtxSetUnit();
		mtxRotX( DEG2RAD( transform.rot.x ) );
		mtxRotY( DEG2RAD( transform.rot.y ) );
		mtxRotZ( DEG2RAD( transform.rot.z ) );

		mtxScale( transform.scl.x , transform.scl.y , transform.scl.z );
		mtxTrans( &transform.pos );

		//頂点情報を変形させる

		VECTOR3 _vtx[3];

		mtxAffinLocal( &_vtx[0], &vtx[0] );
		mtxAffinLocal( &_vtx[1], &vtx[1] );
		mtxAffinLocal( &_vtx[2], &vtx[2] );

		//変形済み頂点から法線を得る

		VECTOR3 ab = _vtx[1] - _vtx[0];
		VECTOR3 bc = _vtx[2] - _vtx[1];

		ab = ab.crossProduct(bc);

		normal = ab.normalize();
	}


	void Draw()
	{

		Sint32 prio = 100;
		Sint32 ax = WINDOW_W/2;
		Sint32 ay = WINDOW_H/2;
		VECTOR3 _vtx[3];

		//表示座標を計算する

		mtxAffinLocal( &_vtx[0], &vtx[0] );
		mtxAffinLocal( &_vtx[1], &vtx[1] );
		mtxAffinLocal( &_vtx[2], &vtx[2] );

		//ライトによる明るさを計算する

		VECTOR3 dirLight = { 0.0f , 0.0f ,-1.0f };
		//VECTOR3 diff;
		//diff = dirLight.crossProduct( normal );

		//ライト角度との内積をとって平行具合をしる
		//帰ってくる値はradianによる角度なので、-1 ～ +1の範囲。１なら平行

		dirLight = dirLight.normalize();

		Float32 fBrightness = ABS(dirLight.dotProduct( normal ));	//1.0f - diff.length();

		//描画する

		Sint32 lum = 32 + (127+96) * fBrightness;
		Uint32 rgb = ARGB(0xff , lum, lum , lum );

		gxLib::DrawTriangle(
			_vtx[0].x+ax,	_vtx[0].y+ay, 
			_vtx[1].x+ax,	_vtx[1].y+ay, 
			_vtx[2].x+ax,	_vtx[2].y+ay, 
			prio,
			gxTrue,
			ATR_DFLT , SET_ALPHA( 1.0f , rgb ) );
	}


	gxTransform transform;
	VECTOR3 vtx[3];
	VECTOR3 normal;
	Uint32  argb[3];



};


class CQuad
{
public:

	CQuad()
	{
		m_TexturePage = 0;

		trans.pos.x = 0.0f;
		trans.pos.y = 0.0f;
		trans.pos.z = 0.0f;

		trans.scl.x = 1.0f;
		trans.scl.y = 1.0f;
		trans.scl.z = 1.0f;

		trans.rot.x = gxLib::Rand()%360;
		trans.rot.y = gxLib::Rand()%360;
		trans.rot.z = gxLib::Rand()%360;
	}

	~CQuad()
	{
	}

	void SetVtx( Sint32 index , Float32 px , Float32 py , gxVertex v1 ,  gxVertex v2 , gxVertex v3 , gxVertex v4)
	{
		m_Index = index;
		
		m_WaitFrm = m_Index/100 + gxLib::Rand()%100;

		trans.pos.x = 2.0f;
		trans.pos.y = 0.0f;
		trans.pos.z = 0.0f;

		m_Vtx[0] = v1;
		m_Vtx[1] = v2;
		m_Vtx[2] = v3;
		m_Vtx[3] = v4;

		target.x = px;
		target.y = py;
		target.z = 5;

		speed.x = gxUtil::Cos( gxLib::Rand()%360 )*0.01f;
		speed.y = gxUtil::Cos( gxLib::Rand()%360 )*0.01f;
		speed.z = gxUtil::Cos( gxLib::Rand()%360 )*0.01f;
	}

	void Action()
	{
		//m_fscl = gxUtil::Cos( (gxLib::GetGameCounter()*8)%360 );

		if( m_WaitFrm > 0 )
		{
			m_WaitFrm --;
			return;
		}

		if( gxLib::Joy(0)->psh&MOUSE_L )
		{
			m_WaitFrm = m_Index/10;
			
			trans.pos.x = 2.0f;
			trans.pos.y = 0.0f;
			trans.pos.z = 0.0f;

			trans.rot.x = gxLib::Rand()%360;
			trans.rot.y = gxLib::Rand()%360;
			trans.rot.z = gxLib::Rand()%360;

			speed.x = gxUtil::Cos( gxLib::Rand()%360 )*0.01f;
			speed.y = gxUtil::Cos( gxLib::Rand()%360 )*0.01f;
			speed.z = gxUtil::Cos( gxLib::Rand()%360 )*0.01f;

			m_fRatio = 1000.0f;
		}
		if( gxLib::Joy(0)->psh&MOUSE_R )
		{
			trans.rot.x += -trans.rot.x/10.0f;
			trans.rot.y += -trans.rot.y/10.0f;
			trans.rot.z += -trans.rot.z/10.0f;

			trans.pos.x += (target.x-trans.pos.x)/10.0f;
			trans.pos.y += (target.y-trans.pos.y)/10.0f;
			trans.pos.z += (target.z-trans.pos.z)/10.0f;
		}
		else
		{
			//デフォ動作

			//--------------------
			//誘導
			//--------------------

			VECTOR3 tmp = target - trans.pos;
			Float32 fLength = tmp.length();

			fLength = 0.001f;//fLength/100.0f;

			if( target.x < trans.pos.x ) speed.x -= fLength;	else speed.x += fLength;
			if( target.y < trans.pos.y ) speed.y -= fLength;	else speed.y += fLength;
			if( target.z < trans.pos.z ) speed.z -= fLength;	else speed.z += fLength;

			m_fRatio -= 1.0f;
			m_fRatio = CLAMP( m_fRatio , 2.0f , 10000.0f );

			speed.x += -speed.x /m_fRatio;
			speed.y += -speed.y /m_fRatio;
			speed.z += -speed.z /m_fRatio;

			trans.pos += speed;

			if( tmp.length() <= 0.2f )
			{
				trans.rot.x += -trans.rot.x/10.0f;
				trans.rot.y += -trans.rot.y/10.0f;
				trans.rot.z += -trans.rot.z/10.0f;

				trans.pos.x += (target.x-trans.pos.x)/10.0f;
				trans.pos.y += (target.y-trans.pos.y)/10.0f;
				trans.pos.z += (target.z-trans.pos.z)/10.0f;

			}
			else
			{
				trans.rot.x += 4.0f;
				trans.rot.y += 8.0f;
			}

		}

	}

	void Draw()
	{
		//トライアングルの描画

		Sint32 ax = WINDOW_W/2 , ay = WINDOW_H/2;
		Sint32 prio = 100;

		VECTOR3 dst[4];
		VECTOR3 vtx[4];
		VECTOR3 vtx_temp[4];

//		trans.pos = target;

		vtx_temp[0] = m_Vtx[0].pos;
		vtx_temp[1] = m_Vtx[1].pos;
		vtx_temp[2] = m_Vtx[2].pos;
		vtx_temp[3] = m_Vtx[3].pos;

		//-----------------------------------------------
		//ローカル座標系のモデルデータを頂点変形する
		//-----------------------------------------------

		mtxSetUnit();
		mtxRotY( DEG2RAD( trans.rot.y ) );
		mtxRotX( DEG2RAD( trans.rot.x ) );

		mtxAffinLocal( &vtx[0], &vtx_temp[0] );
		mtxAffinLocal( &vtx[1], &vtx_temp[1] );
		mtxAffinLocal( &vtx[2], &vtx_temp[2] );
		mtxAffinLocal( &vtx[3], &vtx_temp[3] );

		vtx[0] = vtx[0] + (trans.pos*1.0f);
		vtx[1] = vtx[1] + (trans.pos*1.0f);
		vtx[2] = vtx[2] + (trans.pos*1.0f);
		vtx[3] = vtx[3] + (trans.pos*1.0f);

		//-----------------------------------------------
		//カメラ座表に応じた変形をする
		//-----------------------------------------------

		MATRIX cm = GetCameraMatrix();
		SetCurrentMatrix( &cm );

		mtxAffin( &dst[0], &vtx[0] );
		mtxAffin( &dst[1], &vtx[1] );
		mtxAffin( &dst[2], &vtx[2] );
		mtxAffin( &dst[3], &vtx[3] );

		dst[0] *= 1000.0f;
		dst[1] *= 1000.0f;
		dst[2] *= 1000.0f;
		dst[3] *= 1000.0f;

		gxLib::PutTriangle(
			dst[0].x+ax,	dst[0].y+ay, m_Vtx[0].tex.x*WINDOW_W, m_Vtx[0].tex.y*WINDOW_H,
			dst[1].x+ax,	dst[1].y+ay, m_Vtx[1].tex.x*WINDOW_W, m_Vtx[1].tex.y*WINDOW_H,
			dst[2].x+ax,	dst[2].y+ay, m_Vtx[2].tex.x*WINDOW_W, m_Vtx[2].tex.y*WINDOW_H,
			prio,
			m_TexturePage,
			ATR_DFLT , ARGB_DFLT	);

		gxLib::PutTriangle(
			dst[1].x+ax,	dst[1].y+ay, m_Vtx[1].tex.x*WINDOW_W, m_Vtx[1].tex.y*WINDOW_H,
			dst[3].x+ax,	dst[3].y+ay, m_Vtx[3].tex.x*WINDOW_W, m_Vtx[3].tex.y*WINDOW_H,
			dst[2].x+ax,	dst[2].y+ay, m_Vtx[2].tex.x*WINDOW_W, m_Vtx[2].tex.y*WINDOW_H,
			prio,
			m_TexturePage,
			ATR_DFLT , ARGB_DFLT	);


		//gxLib::DrawLine( dst[0].x + ax, dst[0].y + ay, dst[1].x + ax, dst[1].y + ay, prio, ATR_DFLT, 0xff00ff00);
		//gxLib::DrawLine( dst[1].x + ax, dst[1].y + ay, dst[3].x + ax, dst[3].y + ay, prio, ATR_DFLT, 0xff00ff00);
		//gxLib::DrawLine( dst[3].x + ax, dst[3].y + ay, dst[2].x + ax, dst[2].y + ay, prio, ATR_DFLT, 0xff00ff00);
		//gxLib::DrawLine( dst[2].x + ax, dst[2].y + ay, dst[0].x + ax, dst[0].y + ay, prio, ATR_DFLT, 0xff00ff00);
	}


private:

	gxVertex m_Vtx[4];
	Sint32 m_TexturePage;

	gxTransform trans;
	VECTOR3     speed;
	VECTOR3     target;
	Sint32      m_Index = 0;
	Sint32      m_WaitFrm = 0;

	Float32 m_fRatio = 1000.0f;
};


#endif
