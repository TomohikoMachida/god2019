﻿#include <gxLib.h>
#include "CPhyzics.h"

SINGLETON_DECLARE_INSTANCE( CPhyzics );

void CPhyzics::Draw()
{
	if( !m_bDebugOn ) return;

	Sint32 num = 0;

	for(Sint32 ii=0;ii<enObjMax;ii++)
	{

		if( m_pObj[ii].bUse )
		{
			if( m_pObj[ii].type == 1 )
			{
				drawBox( GetBody( ii ) );
			}
			else if( m_pObj[ii].type == 2 )
			{
				drawJoint( GetLink( ii ) );
			}

			num ++;
			if( num >= m_sObjCnt ) break;
		}	
	}
}

void CPhyzics::drawBox(b2Body* p)
{
//	b2Fixture *pFix;
//	Float32 x,y,r;
//	Uint32 argb = 0xff00ffff;
//
//	Float32 cameraScale = CCamera::GetInstance()->GetScale();
//
//	pFix = p->GetFixtureList();
//
//	if( !pFix->GetBody()->IsAwake() ) argb = 0xffff00ff;
//
//	x = p->GetPosition().x*100.f;
//	y = p->GetPosition().y*100.f;
//	r = p->GetAngle();
//	r = RAD2DEG(r);
//
//	gxLib::DrawBox((Sint32)(x - 2)*cameraScale, (Sint32)(y - 2)*cameraScale, (Sint32)(x + 2)*cameraScale, (Sint32)(y + 2)*cameraScale, enPhyzPrioDebug0, gxFalse, ATR_DFLT, argb);
//
//	switch( (int) pFix->GetShape()->GetType() ){
//	case b2Shape::e_polygon:
//		{
//			b2PolygonShape* pPolygon = (b2PolygonShape*)pFix->GetShape();
//
//			Sint32 cnt = pPolygon->GetVertexCount();
//
//			for(Sint32 ii=0;ii<cnt;ii++)
//			{
//				Sint32 nn = (ii+1)%cnt;
//				b2Vec2 vec1 = pPolygon->GetVertex( ii );
//				b2Vec2 vec2 = pPolygon->GetVertex( nn );
//				Sint32 x1,y1,x2,y2;
//				x1 = (Sint32) (vec1.x*100);
//				y1 = (Sint32) (vec1.y*100);
//				x2 = (Sint32) (vec2.x*100);
//				y2 = (Sint32) (vec2.y*100);
//
//				Float32 r1,r2,d1,d2;
//				r1 = gxUtil::Atan( (Float32) x1, (Float32) y1 );
//				r2 = gxUtil::Atan( (Float32) x2, (Float32) y2 );
//				d1 = gxUtil::Distance( (Float32) x1, (Float32) y1 );
//				d2 = gxUtil::Distance( (Float32) x2, (Float32) y2 );
//
//
//				x1 = (Sint32) (gxUtil::Cos(r1+r)*d1);
//				y1 = (Sint32) (gxUtil::Sin(r1+r)*d1);
//				x2 = (Sint32) (gxUtil::Cos(r2+r)*d2);
//				y2 = (Sint32) (gxUtil::Sin(r2+r)*d2);
//
//				gxLib::DrawLine((Sint32)(x + x1)*cameraScale, (Sint32)(y + y1)*cameraScale, (Sint32)(x + x2)*cameraScale, (Sint32)(y + y2)*cameraScale, 255, ATR_DFLT, argb);
//			}
//			//方向
//			Sint32 x3 = (Sint32) (x + gxUtil::Cos(r)*32);
//			Sint32 y3 = (Sint32) (y + gxUtil::Sin(r)*32);
//			gxLib::DrawLine((Sint32)x*cameraScale, (Sint32)y*cameraScale, x3*cameraScale, y3*cameraScale, 0, ATR_DFLT, argb);
//		}
//		break;
//
//	case b2Shape::e_circle:
//		{
//			Uint32 argb = 0xff00ffff;
//			Float32 radius = pFix->GetShape()->m_radius*100;
//
//			if( !pFix->GetBody()->IsAwake() ) argb = 0xffff00ff;
//
//			for( Sint32 ii=0;ii<360;ii+=40 )
//			{
//				Sint32 x1 = (Sint32) (x + gxUtil::Cos((Float32) ii)*radius);
//				Sint32 y1 = (Sint32) (y + gxUtil::Sin((Float32) ii)*radius);
//				Sint32 x2 = (Sint32) (x + gxUtil::Cos((Float32) (ii+40))*radius);
//				Sint32 y2 = (Sint32) (y + gxUtil::Sin((Float32) (ii+40))*radius);
//				gxLib::DrawLine(x1*cameraScale, y1*cameraScale, x2*cameraScale, y2*cameraScale, 0, ATR_DFLT, argb);
//			}
//			//方向
//			Sint32 x3 = (Sint32) (x + gxUtil::Cos(r)*radius);
//			Sint32 y3 = (Sint32) (y + gxUtil::Sin(r)*radius);
//			gxLib::DrawLine((Sint32)x*cameraScale, (Sint32)y*cameraScale, x3*cameraScale, y3*cameraScale, 0, ATR_DFLT, argb);
//
//			//自分を基準にした時の中心
//			Sint32 x1,y1;
//			//x1 = p->GetLocalCenter().x*100;
//			//y1 = p->GetLocalCenter().y*100;
//			//gxLib::DrawBox( x1-2,y1-2,x1+2,y1+2,0,gxFalse,ATR_DFLT,0xffff0000 );
//
//			//p->GetMass();移動の力
//			//p->GetAngularVelocity角速度？
//			x1 = (Sint32) (p->GetWorldCenter().x*100);
//			y1 = (Sint32) (p->GetWorldCenter().y*100);
//			//gxLib::DrawBox( x1-2,y1-2,x1+2,y1+2,0,gxFalse,ATR_DFLT,0xffff0000 );
//		}
//		break;
//	case b2Shape::e_edge:
//	case b2Shape::e_chain:
//		break;
//	}
//
}

void CPhyzics::drawJoint(b2Joint* p)
{
//	Float32 cameraScale = CCamera::GetInstance()->GetScale();
//
//	Uint32 argb = 0xffff0000;
//
//	switch( p->GetType() ){
//	case e_unknownJoint:
//	case e_revoluteJoint:
//		{
//			Sint32 x = (Sint32) (p->GetAnchorA().x*100);
//			Sint32 y = (Sint32) (p->GetAnchorA().y*100);
//			gxLib::DrawBox((x - 4) * cameraScale, (y - 4) * cameraScale, (x + 4) * cameraScale, (y + 4) * cameraScale, enPhyzPrioDebug1, gxFalse, ATR_DFLT, argb);
//
//			b2Body *q1,*q2;
//			q1 = p->GetBodyA();
//			q2 = p->GetBodyB();
//			Sint32 x1,y1,x2,y2;
//			x1 = (Sint32) (q1->GetPosition().x*100.f);
//			y1 = (Sint32) (q1->GetPosition().y*100.f);
//			x2 = (Sint32) (q2->GetPosition().x*100.f);
//			y2 = (Sint32) (q2->GetPosition().y*100.f);
//			gxLib::DrawLine(x * cameraScale, y * cameraScale, x1 * cameraScale, y1 * cameraScale, enPhyzPrioDebug1, ATR_DFLT, 0x40ffff00);
//			gxLib::DrawLine(x * cameraScale, y * cameraScale, x2 * cameraScale, y2 * cameraScale, enPhyzPrioDebug1, ATR_DFLT, 0xffffff00);
//
//		}
//		break;
//	case e_prismaticJoint:
//	case e_distanceJoint:
//	case e_pulleyJoint:
//	case e_mouseJoint:
//	case e_gearJoint:
//	case e_wheelJoint:
//	case e_weldJoint:
//	case e_frictionJoint:
//	case e_ropeJoint:
//		break;
//	}
}


void CPhyzics::AddPower( Sint32 objID , Sint32 x , Sint32 y )
{
	b2Body* p;
	p = GetBody(objID);

	if( p )
	{
		b2Vec2 vec;
		vec.x = PIX( x );
		vec.y = PIX( y );

		b2Vec2 force;
		force.x = PIX(x);
		force.y = PIX(y);

		p->ApplyForceToCenter ( force , gxFalse );
		//m_pBodyMaster->ApplyTorque( DEG2RAD(-3) );
	}
}

void CPhyzics::AddTorque( Sint32 objID , Sint32 torque )
{
	b2Body* p;

	if( torque == 0 ) return;

	p = GetBody(objID);

	if( p )
	{
		p->ApplyTorque( DEG2RAD( torque) , gxFalse );
	}
}

Sint32 CPhyzics::Polygon( Sint32 x, Sint32 y , gxPoint *pt , Sint32 num , Uint32 maskBits )
{
	b2Vec2* vertex = new b2Vec2[num];
	b2PolygonShape poly;
	//Uint16 maskBits = 0xd9d9;

	for( Sint32 ii=0;ii<num; ii++ )
	{
		vertex[ii].Set( PIX( pt[ii].x ) , PIX( pt[ii].y ) );
	}
	poly.Set( vertex, num );

	delete[] vertex;	// メモリリークしていた	2015/10/03 Iso

	b2FixtureDef   fixtureDef;
	fixtureDef.shape       = &poly;
	fixtureDef.density     = 1.0f;
	fixtureDef.friction    = 1.0f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.filter.categoryBits = 0x0002;
	fixtureDef.filter.maskBits     = maskBits;

	///*TEST*/		b2CircleShape  circ;
	///*TEST*/		fixtureDef.shape       = &circ;
	///*TEST*/		circ.m_radius = PIX(24);

	/*TEST*/		fixtureDef.filter.groupIndex = -1;

	if (maskBits == 0xd9d9)
	{
		fixtureDef.filter.groupIndex = 1;
	}

	b2BodyDef      bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(PIX(x), PIX(y));

	b2Body *p;
	p = m_pWorld->CreateBody(&bodyDef);
	p->CreateFixture(&fixtureDef);
	b2Vec2 trans;

	trans.x = PIX(x);
	trans.y = PIX(y);
	p->SetTransform(trans, 0);
	Sint32 id = addObj(p);

	return id;

}

Sint32 CPhyzics::Box( Sint32 x , Sint32 y  , Sint32 x1 , Sint32 y1 , Sint32 x2 , Sint32 y2 , Float32 rot , Uint16 maskBits )
{
	//-----------------------

	if( x1 > x2 ) SWAP(x1,x2);
	if( y1 > y2 ) SWAP(y1,y2);

	b2Vec2 vertex[4];
	b2PolygonShape poly;

/*
	vertex[0].Set( PIX(x1)  , PIX(y1) );
	vertex[1].Set( PIX(x2)  , PIX(y1) );
	vertex[2].Set( PIX(x2)  , PIX(y2) );
	vertex[3].Set( PIX(x1)  , PIX(y2) );
	poly.Set( vertex, 4 );
*/


	Float32 r[4],dst[4];

	r[0] = gxUtil::Atan( (Float32) x1 , (Float32) y1 );
	r[1] = gxUtil::Atan( (Float32) x2 , (Float32) y1 );
	r[2] = gxUtil::Atan( (Float32) x2 , (Float32) y2 );
	r[3] = gxUtil::Atan( (Float32) x1 , (Float32) y2 );

	dst[0] = gxUtil::Distance( (Float32) x1 , (Float32) y1 );
	dst[1] = gxUtil::Distance( (Float32) x2 , (Float32) y1 );
	dst[2] = gxUtil::Distance( (Float32) x2 , (Float32) y2 );
	dst[3] = gxUtil::Distance( (Float32) x1 , (Float32) y2 );


	Float32 ax[4];
	Float32 ay[4];

	ax[0] = gxUtil::Cos( r[0]+ rot )*dst[0];
	ay[0] = gxUtil::Sin( r[0]+ rot )*dst[0];

	ax[1] = gxUtil::Cos( r[1]+ rot )*dst[1];
	ay[1] = gxUtil::Sin( r[1]+ rot )*dst[1];

	ax[2] = gxUtil::Cos( r[2]+ rot )*dst[2];
	ay[2] = gxUtil::Sin( r[2]+ rot )*dst[2];

	ax[3] = gxUtil::Cos( r[3]+ rot )*dst[3];
	ay[3] = gxUtil::Sin( r[3]+ rot )*dst[3];

	vertex[0].Set( PIX( ax[0] ) , PIX( ay[0] ) );
	vertex[1].Set( PIX( ax[1] ) , PIX( ay[1] ) );
	vertex[2].Set( PIX( ax[2] ) , PIX( ay[2] ) );
	vertex[3].Set( PIX( ax[3] ) , PIX( ay[3] ) );
	poly.Set( vertex, 4 );


	b2FixtureDef   fixtureDef;
	fixtureDef.shape       = &poly;
	fixtureDef.density     = 10.0f;
	fixtureDef.friction    = 0.6f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.filter.categoryBits = 0x0001;
	fixtureDef.filter.maskBits     = maskBits;

///*TEST*/		b2CircleShape  circ;
///*TEST*/		fixtureDef.shape       = &circ;
///*TEST*/		circ.m_radius = PIX(24);

/*TEST*/		fixtureDef.filter.groupIndex = -1;

if( maskBits == 0xd9d9 )
{
	fixtureDef.filter.groupIndex = 1;
}

	b2BodyDef      bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set( PIX(x) , PIX(y) );

	b2Body *p;
	p = m_pWorld->CreateBody( &bodyDef );
	p->CreateFixture( &fixtureDef );
	b2Vec2 trans;

	trans.x = PIX(x);
	trans.y = PIX(y);
	p->SetTransform( trans , 0 );//DEG2RAD(rot) );
	Sint32 id = addObj( p );

	return id;
}

Sint32 CPhyzics::SetLink( Sint32 src , Sint32 dst , Sint32 x , Sint32 y , Sint32 limit1, Sint32 limit2 )
{
	//srcは根本
	//dstは当該オブジェ

	b2RevoluteJointDef RevJointDef;
	b2Vec2 vec;

	if( limit1 == 0 && limit2 == 0 )
	{
		RevJointDef.enableLimit = gxFalse;
		RevJointDef.lowerAngle = DEG2RAD( 0 );
		RevJointDef.upperAngle = DEG2RAD( 0 );
	}
	else
	{
		RevJointDef.enableLimit = gxTrue;
		LIMIT_MAX(limit1, limit2);
		RevJointDef.upperAngle = DEG2RAD(limit1 );

		LIMIT_MIN(limit1, limit2);
		RevJointDef.lowerAngle = DEG2RAD( limit1 );
	}

	// つないだもの同士は衝突しない
	RevJointDef.collideConnected = false;


//		b2Fixture *pFix;
//		pFix = GetBody( src )->GetFixtureList();
//		b2PolygonShape* pPolygon = (b2PolygonShape*)pFix->GetS hape();
//		if( !pFix->GetBody()->IsAwake() ) argb = 0xffff00ff;

	b2Transform trans;
	trans = GetBody(src)->GetTransform();

//		vec.x = GetBody( src )->GetWorld Center().x+PIX( x );
//		vec.y = GetBody( src )->GetWorldCenter().y+PIX( y );
	vec.x = trans.p.x+PIX( x );
	vec.y = trans.p.y+PIX( y );

	RevJointDef.Initialize( GetBody( src ),GetBody( dst ), vec );	//基準はアンカー元

	b2Joint *pJoint;

	pJoint = m_pWorld->CreateJoint( &RevJointDef );

	Sint32 id = addJoint( pJoint );

	return id;
}
