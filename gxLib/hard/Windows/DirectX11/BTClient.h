﻿#ifndef _SERVER_H_
#define _SERVER_H_

//#include <WinSock2.h>
//#include <ws2bth.h>


// BlueTooth状態(サーバー時)
#define		BT_SOC_WAIT		0x01 // 接続待ち
#define		BT_SOC_ACCE		0x02 // 接続中


#define		REC_MAX_BYTE	128	 // 受信データbyte数
#define		MAX_BT_DATA_CNT	10	 // 受信データ数(仮値)


// BlueTooth受信データ構造体
struct bt_rec_data
{
	BYTE id;		// デバイスID(未使用)

	char free[256];
	int  size;

/*
	int touchOn;
	float mouse_x;
	float mouse_y;

	float azimuth;	// デバイスの回転
	float pitch;		// デバイスのピッチ
	float roll;		// デバイスのロール

	float gx;			// デバイスのジャイロポイントX
	float gy;			// デバイスのジャイロポイントY
	float gz;			// デバイスのジャイロポイントZ

	float ax;			// デバイスのジャイロポイントX
	float ay;			// デバイスのジャイロポイントY
	float az;			// デバイスのジャイロポイントZ

	float mx;			// デバイスのジャイロポイントX
	float my;			// デバイスのジャイロポイントY
	float mz;			// デバイスのジャイロポイントZ

	int Speed;		// デバイスのタッチ速度
	int px;			// デバイスのタッチポイントX
	int py;			// デバイスのタッチポイントY
*/
};

extern int que_num;
extern bt_rec_data rec_BT[MAX_BT_DATA_CNT];

extern boolean hassya;
extern boolean hassya_draw;

#endif	// _SERVER_H_