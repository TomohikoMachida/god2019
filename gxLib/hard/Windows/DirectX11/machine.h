﻿// --------------------------------------------------------------------
//
// つじつまあわせ用共通ヘッダ
//
// --------------------------------------------------------------------
#define PLATFORM_WINDESKTOP
#define TARGET_OS_WINDOWS

#define _USE_OPENGL
#define _USE_OPENAL

//#define FILE_FROM_ZIP

//---------------------------------------
//以下自動設定
//---------------------------------------

#ifdef WIN32
	#define GX_BUILD_OPTIONx86
#else
	#define GX_BUILD_OPTIONx64
#endif


#ifdef _USE_OPENAL
	#define CAudio COpenAL
#else
	#define CAudio CXAudio
#endif

//#define GX_STRING_ENCODE_SJIS

#define WIN32_LEAN_AND_MEAN             // Windows ヘッダーから使用されていない部分を除外します。

// Windows ヘッダー ファイル:
#include <windows.h>
#include <windowsx.h>
#include <assert.h>
#include <string.h>
#include <math.h>

// C ランタイム ヘッダー ファイル
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <locale.h>

#ifdef _USE_OPENGL
	#define CGraphics COpenGL3
#else
	#define CGraphics CDirectX11
	#include <dxgi1_4.h>
	#include <d3d11_3.h>
	#include <DirectXMath.h>
#endif

// C++ ランタイム ヘッダー ファイル
#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <functional>
#include <cstdlib>
#include <chrono>
#include <algorithm>

#pragma warning(disable : 4996)
#pragma warning(disable : 4819)
#pragma warning(disable : 4244)

//----------------------------------------------------
//プラットフォーム専用
//----------------------------------------------------

class CWindows
{
	//Windowsデバイス間での制御用クラス

public:

	enum eSamplingFilter {
		enSamplingNearest,
		enSamplingBiLenear,
	};

	CWindows()
	{
		m_bFullScreen  = gxFalse;
		m_bSoundEnable = true;

		m_SamplingFilter = enSamplingNearest;

		m_bVirtualPad = gxFalse;

		m_bBatchMode = gxFalse;
	}

	~CWindows()
	{

	}

	void SetFullScreen( gxBool bFullScreen )
	{
		m_bFullScreen = bFullScreen;
	}

	gxBool IsFullScreen()
	{
		return m_bFullScreen;
	}

	void SetSoundEnable( gxBool bSoundEnable )
	{
		m_bSoundEnable = bSoundEnable;
	}

	gxBool IsSoundEnable()
	{
		return m_bSoundEnable;
	}


	eSamplingFilter GetRenderingFilter()
	{
		return m_SamplingFilter;
	}

	void  SetRenderingFilter( eSamplingFilter Filter )
	{
		m_SamplingFilter = Filter;
	}

	HWND GetWindowHandle()
	{
		return m_hWindow;
	}

	gxBool IsBatchMode()
	{
		return m_bBatchMode;
	}

	void ExecuteApp(char *Appname);

	void SetBatchMode( gxBool bBbatchMode )
	{
		m_bBatchMode = bBbatchMode;
	}

	//クリップボード関連
	gxChar* GetClipBoard( Uint32 type );
	gxBool  SetClipBoard( gxChar *pText );

	HINSTANCE m_hInstance;
	HWND      m_hWindow;
	WPARAM    m_wParam;
	HACCEL    m_hAccel;
	HDC		  m_WinDC;
	Uint32	  m_AppStyle;
	RECT	  m_WinRect;

    //HACCEL hAccelTable;
	LONGLONG Vsyncrate;

	gxBool m_bFullScreen;
	gxBool m_bSoundEnable;
	gxBool m_bVirtualPad;


private:

	SINGLETON_DECLARE( CWindows );

	//サンプリングモード
	eSamplingFilter m_SamplingFilter;

	gxBool m_bBatchMode;

};


