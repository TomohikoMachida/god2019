﻿//------------------------------------------------------------------

//------------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gxSoundManager.h>
#include <gxLib/util/CFileWave.h>

#include "COpenSLES.h"


SINGLETON_DECLARE_INSTANCE( COpenSLES );


COpenSLES::COpenSLES() {

	m_running = false;
	m_engineObject	= NULL;
	m_outputMixObject = NULL;
	m_engineEngine	= NULL;
}


COpenSLES::~COpenSLES()
{
	if( m_running )
	{
		finish();
	}
}

void COpenSLES::Init()
{
	init( MAX_SOUND_NUM );
}


void COpenSLES::Action()
{
	for(Sint32 ii=0; ii<MAX_SOUND_NUM; ii++)
	{
		StPlayInfo *pInfo;

		pInfo = gxSoundManager::GetInstance()->GetPlayInfo( ii );

		if( pInfo->bReq )
		{
			if( pInfo->uStatus & enSoundReqStop )
			{
				StopSound( ii );
			}
			if( pInfo->uStatus & enSoundReqLoad )
			{
				//read( ii );
                LoadSound( ii , gxSoundManager::GetInstance()->GetPlayInfo(ii)->fileName );
			}
			if( pInfo->uStatus & enSoundReqPlay )
			{
				PlaySound( ii , pInfo->bLoop , pInfo->bOverWrap , pInfo->fVolume );
			}
			if( pInfo->uStatus & enSoundReqVolume )
			{
				SetVolume( ii );
			}
			if (pInfo->uStatus & enSoundReqChangeFreq)
			{
				SetFrequency(ii);
			}

			pInfo->bReq = gxFalse;
			pInfo->uStatus = enSoundReqNone;
		}

	}

}


gxBool COpenSLES::init( Sint32 channelCount )
{
	if( m_running )
	{
		GX_DEBUGLOG("andio_engine: audio engine is already created.");
		return false;
	}

	m_channelCount = channelCount;

	SLresult result;
	m_channels = (Channel *)malloc(sizeof(Channel) * channelCount);

	for( Sint32 i = 0; i < channelCount ; i++ ) {
		m_channels[i].loaded = SL_BOOLEAN_FALSE;
	}
	//create engine

    const SLInterfaceID engine_ids[] = {SL_IID_ENGINE};
    const SLboolean engine_req[] = {SL_BOOLEAN_TRUE};
	//result = slCreateEngine(&m_engineObject, 0, NULL, 0 , NULL , NULL );

    result = slCreateEngine(&m_engineObject, 0, NULL, 1, engine_ids, engine_req);

	if (SL_RESULT_SUCCESS != result)
	{
		GX_DEBUGLOG("andio_engine: failed to create audio engine");
		return false;
	}

	// realize the engine
	result = (*m_engineObject)->Realize(m_engineObject, SL_BOOLEAN_FALSE);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to realize audio engine");
		return false;
	}

	// get the engine interface, which is needed in order to create other objects
	result = (*m_engineObject)->GetInterface(m_engineObject, SL_IID_ENGINE, &m_engineEngine);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to get audio engine interface");
		return false;
	}

	// create output mix
	//ここがダメ

	//const SLInterfaceID mix_ids[4] = {SL_IID_PLAYBACKRATE,SL_IID_ENVIRONMENTALREVERB, SL_IID_VOLUME , SL_IID_SEEK };
	const SLInterfaceID mix_ids[4] = {SL_IID_PLAY, SL_IID_SEEK};
	const SLboolean mix_req[3] = { SL_BOOLEAN_FALSE , SL_BOOLEAN_FALSE,SL_BOOLEAN_TRUE };

	result = (*m_engineEngine)->CreateOutputMix( m_engineEngine, &m_outputMixObject, 2, mix_ids, mix_req);
	//result = (*m_engineEngine)->CreateOutputMix( m_engineEngine, &m_outputMixObject, 0, NULL , NULL );

	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to create output mix object");
		return false;
	}
	
	// realize the output mix
	result = (*m_outputMixObject)->Realize(m_outputMixObject, SL_BOOLEAN_FALSE);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to realize output mix object");
		return false;
	}

	m_running = true;
	return true;
}


gxBool COpenSLES::LoadSound( Sint32 index , gxChar *pFileName )
{
	//loadFromAsset( pFileName , index );
	__ReadSound(index);
	return gxTrue;
}

gxBool COpenSLES::ReadSound( Sint32 index )
{
    //メモリからPCMデータを読み込む

    if( index >= 16 ) return gxFalse;

    SLresult result;

    Channel* channel = &m_channels[index];

    if (channel->loaded == SL_BOOLEAN_TRUE)
    {
        closeChannel(index);
        if (channel->playerObject != NULL) {
            (*channel->playerObject)->Destroy(channel->playerObject);
            channel->playerObject = NULL;
            m_channels[index].loaded = false;
        }
    }

//    Uint8 *pData  = (Uint8*)gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetPCMImage();
    Uint8 *pData  = (Uint8*)gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetWaveImage();
    Uint32 uSize  = gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetFileSize();
    gxChar *pName = gxSoundManager::GetInstance()->GetPlayInfo( index )->fileName;


    // configure audio source
//    SLDataLocator_AndroidFD loc_fd = {SL_DATALOCATOR_ANDROIDFD, 0, 0, uSize };
    SLDataLocator_Address loc_fd = {SL_DATALOCATOR_ADDRESS, pData, uSize };
    SLDataFormat_MIME format_mime = {
            SL_DATAFORMAT_MIME,
            NULL,
            SL_CONTAINERTYPE_UNSPECIFIED
    };

    SLDataSource audioSrc = {
            &loc_fd,
            &format_mime
    };

    SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {	SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};

    SLDataFormat_PCM format_pcm = {
            SL_DATAFORMAT_PCM,
            2,//channels,
            SL_SAMPLINGRATE_22_05,//sr,
            SL_PCMSAMPLEFORMAT_FIXED_16,
            SL_PCMSAMPLEFORMAT_FIXED_16,
            SL_SPEAKER_FRONT_CENTER,//speakers,
            SL_BYTEORDER_LITTLEENDIAN
    };

    SLDataSource audioSrc2 = {
            &loc_bufq,
            &format_pcm
    };


    // configure audio sink
    SLDataLocator_OutputMix loc_outmix = {
            SL_DATALOCATOR_OUTPUTMIX,
            m_outputMixObject
    };

    SLDataSink audioSnk = {
            &loc_outmix,
            NULL
    };

    // create audio player
    const SLInterfaceID player_ids[3] = {SL_IID_PLAY, SL_IID_VOLUME, SL_IID_SEEK};
    const SLboolean player_req[3] = {
            SL_BOOLEAN_TRUE,
            SL_BOOLEAN_TRUE,
            SL_BOOLEAN_TRUE
    };

    result = (*m_engineEngine)->CreateAudioPlayer(
            m_engineEngine,
            &channel->playerObject,
            &audioSrc,	//入力元
            &audioSnk,	//出力先
            3,
            player_ids,
            player_req );

    if (SL_RESULT_SUCCESS != result)
    {
        GX_DEBUGLOG("andio_engine: failed to create an audio player");
        return false;
    }

    // realize the player
    result = (*channel->playerObject)->Realize(channel->playerObject, SL_BOOLEAN_FALSE);
    if (SL_RESULT_SUCCESS != result)
    {
        GX_DEBUGLOG("andio_engine: failed to realize an audio player");
        return false;
    }

    channel->loaded = SL_BOOLEAN_TRUE;

    // get the play interface
    result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_PLAY, &channel->playerPlay);
    if (SL_RESULT_SUCCESS != result) {
        GX_DEBUGLOG("andio_engine: failed to get an audio player interface");
        return false;
    }

    // get the seek interface
    result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_SEEK, &channel->playerSeek);
    if (SL_RESULT_SUCCESS != result) {
        GX_DEBUGLOG("andio_engine: failed to get an audio seek interface");
        return false;
    }

    // the volume interface
    result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_VOLUME, &channel->playerVolume);
    if (SL_RESULT_SUCCESS != result) {
        GX_DEBUGLOG("andio_engine: failed to create an audio volume interface");
        return false;
    }

    result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_PITCH, &channel->playerPitch);
    if (SL_RESULT_SUCCESS != result) {
        GX_DEBUGLOG("andio_engine: failed to create an audio pitch interface");
        return false;
    }

    return true;
}

gxBool COpenSLES::__ReadSound( Sint32 index )
{
	if( gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetWaveImage() )
	{
		//SAFE_DELETE( gxSoundManager::GetInstance()->GetPlayInfo( index )->m_pWave );
		//gxSoundManager::GetInstance()->GetPlayInfo( index )->m_pWave = NULL;
	}

	Uint8 *pData  = (Uint8*)gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetWaveImage();	//gxSoundManager::GetInstance()->GetPlayInfo( index )->m_pTempBuffer;
	Uint32 uSize  = gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetFileSize();			//gxSoundManager::GetInstance()->GetPlayInfo( index )->m_uTempSize;
	gxChar *pName = gxSoundManager::GetInstance()->GetPlayInfo( index )->fileName;

//	SAFE_DELETE( gxSoundManager::GetInstance()->GetPlayInfo( index )->m_pTempBuffer );

    if( index >= 16 ) return gxFalse;

	SLresult result;

	Channel* channel = &m_channels[index];

	if (channel->loaded == SL_BOOLEAN_TRUE)
	{
		closeChannel(index);
        if (channel->playerObject != NULL) {
            (*channel->playerObject)->Destroy(channel->playerObject);
            channel->playerObject = NULL;
            m_channels[index].loaded = false;
        }
    }

	//-------------------------------------------------------------------
	SLDataLocator_AndroidSimpleBufferQueue  loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 4};
//	SLDataLocator_AndroidBufferQueue		loc_bufq = {SL_DATALOCATOR_ANDROIDBUFFERQUEUE, 4};
    SLDataFormat_PCM                        format_pcm;
    SLDataSource                            audioSrc = {&loc_bufq, &format_pcm};
 	//-------------------------------------------------------------------
	struct WaveFormat
	{
		Sint8  riff[4];
		Uint32 total_size;
		Sint8  fmt[8];
		Uint32 fmt_size;
		Uint16 format;
		Uint16 channel;
		Uint32 rate;
		Uint32 avgbyte;
		Uint16 block;
		Uint16 bit;
		Sint8  data[4];
		Uint32 data_size;
	};

	WaveFormat* _info = (WaveFormat*)pData;
 
	format_pcm.formatType	= SL_DATAFORMAT_PCM;
	format_pcm.numChannels	= (SLuint32)_info->channel;
	format_pcm.samplesPerSec= (SLuint32)_info->rate*1000;
	format_pcm.bitsPerSample= (SLuint32)_info->bit;
	format_pcm.containerSize= (SLuint32)_info->bit;
	format_pcm.channelMask	= (_info->channel == 1) ? SL_SPEAKER_FRONT_CENTER : (SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT);
	format_pcm.endianness	= SL_BYTEORDER_LITTLEENDIAN;

/*
 * StWaveInfo *pWaveInfo;
    pWaveInfo = gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetFormat();
    format_pcm.formatType	= SL_DATAFORMAT_PCM;
    format_pcm.numChannels	= (SLuint32)pWaveInfo->Format.nChannels;
    format_pcm.samplesPerSec= (SLuint32)pWaveInfo->Format.nSamplesPerSec;//*1000;
    format_pcm.bitsPerSample= (SLuint32)pWaveInfo->Format.nSamplesPerSec;
    format_pcm.containerSize= (SLuint32)pWaveInfo->Format.cbSize;
    format_pcm.channelMask	= (_info->channel == 1) ? SL_SPEAKER_FRONT_CENTER : (SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT);
    format_pcm.endianness	= SL_BYTEORDER_LITTLEENDIAN;
*/

  //          sound_data = (char*)((u32)_data + sizeof(WaveFormat));
  //          sound_size = _info->data_size;
  //          sound_loop = _loop;

    // configure audio sink
	SLDataLocator_OutputMix loc_outmix = {
		SL_DATALOCATOR_OUTPUTMIX,
		m_outputMixObject
	};

	SLDataSink audioSnk = {
		&loc_outmix,
		NULL
	};

	// create audio player
	const SLInterfaceID player_ids[5] = {SL_IID_PLAY, SL_IID_BUFFERQUEUE , SL_IID_VOLUME, SL_IID_PITCH , SL_IID_SEEK };
//	const SLInterfaceID player_ids[5] = {SL_IID_PLAY, SL_IID_BUFFERQUEUE ,SL_IID_SEEK, };
	const SLboolean player_req[5] = {
		SL_BOOLEAN_TRUE,
		SL_BOOLEAN_TRUE,
		SL_BOOLEAN_TRUE,
		SL_BOOLEAN_FALSE,
		SL_BOOLEAN_FALSE
	};

	result = (*m_engineEngine)->CreateAudioPlayer(
		m_engineEngine,
		&channel->playerObject,
		&audioSrc,	//入力元
		&audioSnk,	//出力先
		5,
		player_ids,
		player_req );

	if (SL_RESULT_SUCCESS != result)
	{
		GX_DEBUGLOG("andio_engine: failed to create an audio player");
		return false;
	}



    result = (*channel->playerObject)->Realize( channel->playerObject, SL_BOOLEAN_FALSE);   // リアライズ
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to get an audio realize");
		//return false;
	}

    result = (*channel->playerObject)->GetInterface( channel->playerObject, SL_IID_PLAY, &channel->playerPlay );
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to get an audio play interface");
		//return false;
	}

    result = (*channel->playerObject)->GetInterface( channel->playerObject, SL_IID_BUFFERQUEUE, &channel->PlayerBufferQueue);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to get an audio bufferQue interface");
		//return false;
	}

    result = (*channel->playerObject)->GetInterface( channel->playerObject, SL_IID_VOLUME, &channel->playerVolume );
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to get an audio volume interface");
		//return false;
	}

	// get the seek interface
	result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_SEEK, &channel->playerSeek);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to get an audio seek interface");
		//return false;
	}

	result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_PITCH, &channel->playerPitch);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to create an audio pitch interface");
		//return false;
	}

//    result = (*channel->PlayerBufferQueue)->Enqueue( channel->PlayerBufferQueue, pData, (SLuint32)uSize );
    result = (*channel->PlayerBufferQueue)->Enqueue( channel->PlayerBufferQueue, gxSoundManager::GetInstance()->GetPlayInfo( index )->m_WaveData.GetPCMImage(), (SLuint32)uSize );

	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to create an audio pitch interface");
		return false;
	}

	channel->loaded = SL_BOOLEAN_TRUE;


	return gxTrue;
}


gxBool COpenSLES::loadFromAsset(char* fname, Sint32 index)
{
    if( index >= 16 ) return gxFalse;

	SLresult result;

	Channel* channel = &m_channels[index];

	if (channel->loaded == SL_BOOLEAN_TRUE)
	{
		closeChannel(index);
        if (channel->playerObject != NULL) {
            (*channel->playerObject)->Destroy(channel->playerObject);
            channel->playerObject = NULL;
            m_channels[index].loaded = false;
        }
    }


	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	AAssetManager* GetAssetManager();
	AAsset* asset;
	AAssetManager* mgr = GetAssetManager();

	asset = AAssetManager_open( mgr, fname, AASSET_MODE_UNKNOWN );

	if (asset == NULL)
	{
		GX_DEBUGLOG("andio_engine: failed to open an audio file");
		GX_DEBUGLOG(fname);
		return false;
	}

	// open asset as file descriptor
	off_t start, length;
	int fd = AAsset_openFileDescriptor(asset, &start, &length);
	if (fd < 0) {
		GX_DEBUGLOG("andio_engine: failed to open an audio file");
		GX_DEBUGLOG(fname);
		return false;
	}

	AAsset_close(asset);

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------

	// configure audio source
	SLDataLocator_AndroidFD loc_fd = {SL_DATALOCATOR_ANDROIDFD, fd, start, length};
	SLDataFormat_MIME format_mime = {
		SL_DATAFORMAT_MIME,
		NULL,
		SL_CONTAINERTYPE_UNSPECIFIED
	};

	SLDataSource audioSrc = {
		&loc_fd,
		&format_mime
	};

	SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {	SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};

	SLDataFormat_PCM format_pcm = {
			SL_DATAFORMAT_PCM,
            2,//channels,
            SL_SAMPLINGRATE_22_05,//sr,
            SL_PCMSAMPLEFORMAT_FIXED_16,
            SL_PCMSAMPLEFORMAT_FIXED_16,
            SL_SPEAKER_FRONT_CENTER,//speakers,
            SL_BYTEORDER_LITTLEENDIAN
	};

    SLDataSource audioSrc2 = {
            &loc_bufq,
            &format_pcm
    };


    // configure audio sink
	SLDataLocator_OutputMix loc_outmix = {
		SL_DATALOCATOR_OUTPUTMIX,
		m_outputMixObject
	};

	SLDataSink audioSnk = {
		&loc_outmix,
		NULL
	};

	// create audio player
	const SLInterfaceID player_ids[3] = {SL_IID_PLAY, SL_IID_VOLUME, SL_IID_SEEK};
	const SLboolean player_req[3] = {
		SL_BOOLEAN_TRUE,
		SL_BOOLEAN_TRUE,
		SL_BOOLEAN_TRUE
	};

	result = (*m_engineEngine)->CreateAudioPlayer(
		m_engineEngine,
		&channel->playerObject,
		&audioSrc,	//入力元
		&audioSnk,	//出力先
		3,
		player_ids,
		player_req );

	if (SL_RESULT_SUCCESS != result)
	{
		GX_DEBUGLOG("andio_engine: failed to create an audio player");
		return false;
	}

	// realize the player
	result = (*channel->playerObject)->Realize(channel->playerObject, SL_BOOLEAN_FALSE);
	if (SL_RESULT_SUCCESS != result)
	{
		GX_DEBUGLOG("andio_engine: failed to realize an audio player");
		return false;
	}

	channel->loaded = SL_BOOLEAN_TRUE;

	// get the play interface
	result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_PLAY, &channel->playerPlay);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to get an audio player interface");
		return false;
	}

	// get the seek interface
	result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_SEEK, &channel->playerSeek);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to get an audio seek interface");
		return false;
	}

	// the volume interface
	result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_VOLUME, &channel->playerVolume);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to create an audio volume interface");
		return false;
	}

	result = (*channel->playerObject)->GetInterface(channel->playerObject, SL_IID_PITCH, &channel->playerPitch);
	if (SL_RESULT_SUCCESS != result) {
		GX_DEBUGLOG("andio_engine: failed to create an audio pitch interface");
		return false;
	}

	return true;
}

gxBool COpenSLES::setChannelState(Sint32 index, SLuint32 state)
{
	GX_DEBUGLOG("andio_engine::setChannelState");

	Channel* channel = &m_channels[index];

	if (!channel->loaded)
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return false;
	}

	SLresult result = (*channel->playerPlay)->SetPlayState( channel->playerPlay , state );

	checkError("andio_engine: failed to set play state[%d]", result);

	return (SL_RESULT_SUCCESS == result);
}


SLuint32 COpenSLES::getChannelState(Sint32 index)
{

	GX_DEBUGLOG("andio_engine::getChannelState");
	Channel* channel = &m_channels[index];

	if (!channel->loaded)
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return SL_PLAYSTATE_STOPPED;
	}

	SLuint32 state;
	SLresult result = (*channel->playerPlay)->GetPlayState(channel->playerPlay, &state);
	checkError("andio_engine: failed to get play state", result);

	return state;
}


void COpenSLES::closeChannel(Sint32 index)
{
//	GX_DEBUGLOG("andio_engine::closeChannel");

	Channel* channel = &m_channels[index];

	if ( channel->loaded )
	{
		if (getChannelState(index) != SL_PLAYSTATE_PAUSED)
		{
			StopSound( index );
		}

		(*channel->playerObject)->Destroy(channel->playerObject);

		channel->playerObject = NULL;
		channel->playerPlay   = NULL;
		channel->playerSeek   = NULL;
		channel->playerVolume = NULL;
		channel->loaded	   = SL_BOOLEAN_FALSE;
	}
}

int COpenSLES::getChannelCount()
{
//	GX_DEBUGLOG("andio_engine::getChannelCount");

	return m_channelCount;
}


/* this callback handler is called every time a buffer finishes playing */
static void opensl_callback( SLAndroidSimpleBufferQueueItf bq ,void*  nnn )
{
	Uint32 uIndex = 0;
    Uint8 *pData  = (Uint8*)gxSoundManager::GetInstance()->GetPlayInfo( uIndex )->m_WaveData.GetWaveImage();	//gxSoundManager::GetInstance()->GetPlayInfo( index )->m_pTempBuffer;
    Uint32 uSize  = gxSoundManager::GetInstance()->GetPlayInfo( uIndex )->m_WaveData.GetFileSize();			//gxSoundManager::GetInstance()->GetPlayInfo( index )->m_uTempSize;

	COpenSLES::Channel* channel = &COpenSLES::GetInstance()->m_channels[0];
	SLresult result = (*channel->PlayerBufferQueue)->Enqueue( channel->PlayerBufferQueue, pData, (SLuint32)uSize );

    //result = (*bq)->Enqueue(bq, data->buffer, data->bufferSize);
    //PRINTERR(result, "bq->Enqueue");
}

gxBool COpenSLES::PlaySound( Uint32 uIndex , gxBool bLoop , gxBool bOverWrap , Float32 fVolume )
{

    //overwrapできない

    if( !bOverWrap )
    {
        StopSound( uIndex );
    }

    //closeChannel(uIndex);

	Channel* channel = &m_channels[uIndex];

	SLresult result = (*channel->PlayerBufferQueue)->RegisterCallback( channel->PlayerBufferQueue, opensl_callback ,NULL );

    playChannel( uIndex );

    //setChannelLooping( uIndex, bLoop );

    gxSoundManager::GetInstance()->PlayNow( uIndex , gxTrue );

    return gxTrue;
}


gxBool COpenSLES::SetVolume( Uint32 uIndex )
{
    Sint32 i = uIndex;
    Float32 fMasterVolume = gxSoundManager::GetInstance()->GetMasterVolume();
    Float32 fForceVolume  = 1.0f;//gxSoundManager::GetInstance()->Get->GetForceVolume();

    Float32 fVol = gxSoundManager::GetInstance()->GetPlayInfo( i )->fVolume;

    if ( fVol*100.f > 100.f )
    {
        fVol = 0.f;
    }
    else if ( fVol*100.f < 0.f )
    {
        fVol = -10000.f;
    }
    else
    {
        fVol *= fMasterVolume;
        fVol = -(Float32)pow( 100.0f, (Float32)(log10(100-fVol)) );
    }

    fVol = 1.0f - fVol;


    setChannelVolume( uIndex, -10000.0f * fVol );

    return gxTrue;
}


SLmillibel COpenSLES::getChannelVolume( Sint32 index )
{
	GX_DEBUGLOG("andio_engine::getChannelVolume");

	Channel* channel = &m_channels[index];

	if (!channel->loaded)
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return 0;
	}

	SLmillibel volumeLevel;

	SLresult result = (*channel->playerVolume)->GetVolumeLevel(channel->playerVolume, &volumeLevel);

	checkError("andio_engine: failed to get audio channel volume", result);

	return volumeLevel;
}


SLmillibel COpenSLES::setChannelVolume( Sint32 index, SLmillibel volumeLevel )
{
	Channel* channel = &m_channels[index];

	if (!channel->loaded)
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return 0;
	}

	SLresult result = (*channel->playerVolume)->SetVolumeLevel(channel->playerVolume, volumeLevel);

	checkError("andio_engine: failed to set audio channel volume", result);
    GX_DEBUGLOG("[gxLib::SLES] SetVolume()");

	return getChannelVolume(index);
}

SLmillibel COpenSLES::getChannelMaxVolume( Sint32 index )
{
	Channel* channel = &m_channels[index];

	if (!channel->loaded)
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return 0;
	}

	SLmillibel volumeLevel;

	SLresult result = (*channel->playerVolume)->GetMaxVolumeLevel(channel->playerVolume, &volumeLevel);

	checkError("andio_engine: failed to get audio channel max volume", result);
    GX_DEBUGLOG("[gxLib::SLES] GetVolume()");

	return volumeLevel;
}


gxBool COpenSLES::seekChannel(Sint32 index, Sint32 pos, SLuint32 seekMode)
{
	Channel* channel = &m_channels[index];

	if (!channel->loaded)
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return false;
	}

	SLresult result = (*channel->playerSeek)->SetPosition(channel->playerSeek, pos, seekMode);

	checkError("andio_engine: failed to seek audio channel", result);
    GX_DEBUGLOG("[gxLib::SLES] Seek()");

	return (SL_RESULT_SUCCESS == result);
}

gxBool COpenSLES::playChannel( Sint32 index )
{
	Channel* channel = &m_channels[index];

	if ( !channel->loaded )
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return false;
	}

	//if ( getChannelState( index ) != SL_PLAYSTATE_STOPPED )
	//{
	//	setChannelState(index, SL_PLAYSTATE_STOPPED );
	//	seekChannel(index, 0, SL_SEEKMODE_FAST );
	//}
	//seekChannel(index, 0, SL_SEEKMODE_ACCURATE );

    GX_DEBUGLOG("[gxLib::SLES]Play()");
	return setChannelState( index, SL_PLAYSTATE_PLAYING );
}


gxBool COpenSLES::resumeChannel(Sint32 index)
{
	Channel* channel = &m_channels[index];

	if (!channel->loaded)
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return false;
	}

    GX_DEBUGLOG("[gxLib::SLES]Resume()");
	return setChannelState(index, SL_PLAYSTATE_PLAYING);
}


gxBool COpenSLES::PauseSound(Sint32 index)
{
	Channel* channel = &m_channels[index];

	if ( !channel->loaded )
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return false;
	}

    GX_DEBUGLOG("[gxLib::SLES]Pause()");
	return setChannelState(index, SL_PLAYSTATE_PAUSED);
}


gxBool COpenSLES::StopSound( Uint32 index)
{

	Channel* channel = &m_channels[index];

	if (!channel->loaded )
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return false;
	}

    GX_DEBUGLOG("[gxLib::SLES]Stop()");
	return setChannelState(index, SL_PLAYSTATE_STOPPED);
}


void COpenSLES::finish()
{
	//終了処理

	GX_DEBUGLOG("andio_engine::close");

	if (m_running) {
		for (int i = 0; i < getChannelCount(); i++)
		{
			closeChannel(i);
		}

		free(m_channels);

		(*m_outputMixObject)->Destroy(m_outputMixObject);
		(*m_engineObject)->Destroy(m_engineObject);

		m_engineObject	= NULL;
		m_outputMixObject = NULL;
		m_engineEngine	= NULL;
	}

	m_running = false;
}


gxBool COpenSLES::isRunning()
{
	return m_running;
}


gxBool COpenSLES::isLoaded(Sint32 index)
{
   return m_channels[index].loaded;
}


gxBool COpenSLES::getChannelLooping(Sint32 index)
{
	Channel* channel = &m_channels[index];

	if (!channel->loaded)
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return false;
	}

	SLboolean enabled;

	SLmillisecond start = 0;
	SLmillisecond end = SL_TIME_UNKNOWN;
	SLresult result = (*channel->playerSeek)->GetLoop(channel->playerSeek, &enabled, &start, &end);

	if (SL_RESULT_SUCCESS == result && enabled)
	{
		return true;
	}

	if (SL_RESULT_SUCCESS != result)
	{
		GX_DEBUGLOG("andio_engine: failed to get the channel loop status");
	}

	return false;
}

gxBool COpenSLES::setChannelLooping(Sint32 index, gxBool enable)
{
	Channel* channel = &m_channels[index];

	if (!channel->loaded)
	{
		GX_DEBUGLOG("andio_engine: audio channel is closed");
		return false;
	}
    return false;

	SLresult result;

	if (enable)
	{
		result = (*channel->playerSeek)->SetLoop(channel->playerSeek, SL_BOOLEAN_TRUE, 0, SL_TIME_UNKNOWN);
	}
	else
	{
		result = (*channel->playerSeek)->SetLoop(channel->playerSeek, SL_BOOLEAN_FALSE, 0, SL_TIME_UNKNOWN);
	}

	return (SL_RESULT_SUCCESS == result);
}


gxBool COpenSLES::SetFrequency(Sint32 index)
{
	return gxFalse;

/*
	Channel* channel = &m_channels[index];

    SLpermille mill;

	(*channel->playerPitch)->GetPitch( channel->playerPitch, &mill );
	(*channel->playerPitch)->SetPitch( channel->playerPitch, mill+1000 );
*/

}


SLresult COpenSLES::checkError( char* message, SLresult result )
{
	if( SL_RESULT_SUCCESS != result )
	{
		GX_DEBUGLOG( message);
	}

	return result;
}


void soundTestSLES()
{
#if 0
 
    static int seq = 0;

    int channel = 0;

    if (seq == 0) {
        gxLib::LoadAudio(0, "asset/visor/wav/Loop_148.wav");
//        COpenSLES::GetInstance()->LoadSound( 0 , "asset/visor/wav/Loop_148.wav" );
//        COpenSLES::GetInstance()->ReadSound( 0 );

        Uint32 uSize = 0;
        //Uint8 *pData = gxLib::LoadFile("asset/visor/wav/Loop_148.mp3" , &uSize );
        //gxLib::ReadAudio( 0 , pData , uSize );
        //COpenSLES::GetInstance()->ReadSound( 0 );
        COpenSLES::GetInstance()->PlaySound(0);
        seq++;
    }

    if (gxLib::Joy(0)->trg & MOUSE_L) {
//		//COpenSLES::GetInstance()->PlaySound( 0 );
        gxLib::PlayAudio(0);
    }

    COpenSLES::GetInstance()->Action();

#endif

	static int seq = 0;
	
	if( seq == 0 )
	{
		gxLib::LoadAudio( 0 , "visor/wav/bgm.wav" );
	}

	if( gxLib::Joy(0)->trg&MOUSE_L )
	{
		if( gxLib::Joy(0)->my < 256 )
		{
			gxLib::StopAudio( 0 );
		}
		else
		{
			gxLib::PlayAudio( 0 );
		}
	}

	seq ++;
}


