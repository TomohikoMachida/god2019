﻿//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>

SINGLETON_DECLARE_INSTANCE( CDeviceManager );

void AppInit()
{
	CDeviceManager::GetInstance()->AppInit();
	CGameGirl::GetInstance()->ToggleDeviceMode();

}


void AppUpdate()
{

	CGameGirl::GetInstance()->Action();

	if (CGameGirl::GetInstance()->IsAppFinish())
	{

	}

}


void AppFlip()
{


}


void AppFinish()
{
	CGameGirl::DeleteInstance();

	CDeviceManager::DeleteInstance();

	gxLib::Destroy();
}


int testmain()
{
	//AppInit();

	//CGameGirl::GetInstance()->Init();
	//CGameGirl::GetInstance()->AdjustScreenResolution();
	//CGraphics::GetInstance()->Init();
	//CAudio::GetInstance()->Init();
 	//CGamePad::GetInstance()->Init();
	//gxLib::SetVirtualPad( CWindows::GetInstance()->m_bVirtualPad );

	//void BlueToothThreadMake();
	//BlueToothThreadMake();

/*
	while( gxTrue )
	{
		AppUpdate()
		CGameGirl::GetInstance()->Action();

		//if (CGameGirl::GetInstance()->IsAppFinish())
		{
			break;
		}
	}
*/

	//CBlueToothManager::GetInstance()->DestroyBlueToothThread();
	//CGameGirl::DeleteInstance();

	//CGraphics::DeleteInstance();
	//CAudio::DeleteInstance();
	//CGamePad::DeleteInstance();

	//CDeviceManager::DeleteInstance();
	//CBlueToothManager::DeleteInstance();
	//gxLib::Destroy();

	return 0;
}

void* REALLOC( void* p , Uint32 sz )
{
	return NULL;
}

CDeviceManager::CDeviceManager()
{
}


CDeviceManager::~CDeviceManager()
{
}


void CDeviceManager::AppInit()
{
	static int nnn = 0;
	if( nnn == 0 )
	{
		CGameGirl::GetInstance()->Init();
		//CAudio::GetInstance()->Init();
		//COpenGLES2::GetInstance()->Init();
	} else{
        CGameGirl::GetInstance()->SetResume();
    }

	CGameGirl::GetInstance()->AdjustScreenResolution();

	nnn ++;
}


void   CDeviceManager::GameInit()
{

}


gxBool CDeviceManager::GameUpdate()
{
	static gxBool m_bGameInit = gxFalse;
	gxBool bExist = gxTrue;

	if( !m_bGameInit )
	{
		bExist = ::GameInit();
		m_bGameInit = gxTrue;
	}

	if( bExist )
	{
		//ゲームメインへ
		bExist = ::GameMain();
	}

	if( !bExist ) return gxFalse;

	return gxTrue;
}


gxBool CDeviceManager::GamePause()
{
	return ::GamePause();
}


void   CDeviceManager::Render()
{
	if( CGameGirl::GetInstance()->IsResume() ) return;

//	COpenGLES2::GetInstance()->Update();
//	COpenGLES2::GetInstance()->Render();

}


Float32 getFrameTime()
{
	static Uint64 oldCount = 0;
	static Uint64 freq = 0;

	static Sint64 current = 3;

	timespec ts;
	clock_gettime( CLOCK_MONOTONIC, &ts );

    if( current > 0 )
    {
        current  --;
        return 0.0f;
    }

	if (oldCount == 0 )
	{
		oldCount = ts.tv_sec*1000 + ts.tv_nsec/1000000;
	}

	Uint64 now = ts.tv_sec*1000 + ts.tv_nsec/1000000;
	Uint64 sa = ( now - oldCount );

	Float32 fElapsedTime = (Float32)(sa/1000.0f);

	return fElapsedTime;
}


void   CDeviceManager::vSync()
{
	static Float32 s_fOld = getFrameTime();
	Float32 fps = FRAME_PER_SECOND;

	while ( gxTrue )
	{
		Float32 fNow = getFrameTime();;

		if( fNow >= s_fOld + ( 1.0f / fps  ) )
		{
			s_fOld = fNow;
			break;
		}
	}

}


void   CDeviceManager::Flip()
{
//	COpenGLES2::GetInstance()->Present();
}


void   CDeviceManager::Resume()
{
}


void   CDeviceManager::Movie()
{
}


void   CDeviceManager::Play()
{
//	CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	return gxTrue;
}



void   CDeviceManager::UploadTexture(Sint32 sBank)
{
//    COpenGLES2::GetInstance()->ReadTexture( sBank );
}

void   CDeviceManager::LogDisp(char* pString)
{

}


Float32 CDeviceManager::Clock()
{
	timespec ts;
	tm st;
	// 時刻の取得
	clock_gettime(CLOCK_REALTIME, &ts);  //時刻の取得
	localtime_r( &ts.tv_sec, &st);       //取得時刻をローカル時間に変換
	
	
	Sint32 Year  = st.tm_year+1900;
	Sint32 Month = st.tm_mon+1;
	Sint32 Day   = st.tm_mday;
	//Sint32 time_st->tm_wday;

	Sint32 Hour = st.tm_hour;
	Sint32 Min  = st.tm_min;
	Sint32 Sec  = st.tm_sec;
	Sint32 MSec = ts.tv_nsec;
	Sint32 USec = ts.tv_nsec;

	CGameGirl::GetInstance()->SetTime( Year , Month , Day , Hour , Min , Sec , MSec , USec );

	return getFrameTime();
}



Uint8* CDeviceManager::LoadFile( const gxChar* pFileName , Uint32* pLength , Uint32 uLocation )
{
	return nullptr;
}


gxBool CDeviceManager::SaveFile( const gxChar* pFileName , Uint8* pReadBuf , Uint32 uSize , Uint32 uLocation )
{
	return gxTrue;
}



gxBool CDeviceManager::LoadConfig()
{
	return gxTrue;
}


gxBool CDeviceManager::SaveConfig()
{
	return gxTrue;
}



void CDeviceManager::ToastDisp( gxChar* pMessage )
{
}


void CDeviceManager::OpenWebClient( gxChar* pURL )
{
}



gxBool CDeviceManager::SetAchievement( Uint32 index )
{
	return gxTrue;
}


gxBool CDeviceManager::GetAchievement( Uint32 index )
{
	return gxTrue;
}



//特殊
static pthread_t test_thread;

void   CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	pthread_create( &test_thread, NULL, pFunc, (void *)pArg);
}


void   CDeviceManager::Sleep( Uint32 msec )
{
	struct timespec	 req, res;
	req.tv_sec  = 0;
	req.tv_nsec = msec * 1000000;

	nanosleep(&req, &res);
}


gxBool CDeviceManager::PadConfig()
{
	return gxTrue;
}



void CDeviceManager::UpdateMemoryStatus(Uint32* uNow, Uint32* uTotal, Uint32* uMax)
{

}



wchar_t* CDeviceManager::UTF8toUTF16( gxChar*  pString  , size_t* pSize )
{
	setlocale(LC_ALL, "JPN");
	static wchar_t destBuf[FILENAMEBUF_LENGTH];

	mbstowcs(destBuf, pString, FILENAMEBUF_LENGTH );

	return destBuf;
}


wchar_t* CDeviceManager::SJIStoUTF16( gxChar*  pString  , size_t* pSize )
{
	return NULL;
}


gxChar*  CDeviceManager::UTF16toUTF8( wchar_t* pUTF16buf, size_t* pSize )
{
	setlocale(LC_ALL, "JPN");
	static char destBuf[FILENAMEBUF_LENGTH];

	wcstombs( destBuf, pUTF16buf , FILENAMEBUF_LENGTH );

	return destBuf;
}


gxChar*  CDeviceManager::UTF16toSJIS( wchar_t* pUTF16buf, size_t* pSize )
{
	return NULL;
}


gxChar*  CDeviceManager::UTF8toSJIS ( gxChar*  pUTF8buf , size_t* pSize )
{
	return NULL;
}


gxChar*  CDeviceManager::SJIStoUTF8 ( gxChar*  pSJISbuf , size_t* pSize )
{
	return NULL;
}


