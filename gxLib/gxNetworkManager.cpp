﻿//ネットワーク管理
#include <gxLib.h>
#include "gxNetworkManager.h"
#include "util/gxUIManager.h"

SINGLETON_DECLARE_INSTANCE( gxNetworkManager );

Sint32 gxNetworkManager::OpenURL( const gxChar *pOpenURL )
{
	Sint32 index = m_sHttpRequestMax;// %enHttpOpenMax;

	sprintf( m_pHttp[ index ].m_URL ,"%s" , pOpenURL );

	m_pHttp[index].m_User[0] = 0x00;
	m_pHttp[index].m_Pass[0] = 0x00;

	m_pHttp[ index ].uFileSize    = 0;
	m_pHttp[ index ].uReadFileSize = 0;
	m_pHttp[ index ].bClose = gxFalse;

	//SAFE_DELETES( m_pHttp[ index ].pData );

	m_pHttp[ index ].pData    = NULL;
	m_pHttp[ index ].bRequest = gxTrue;
	m_pHttp[ index ].m_Seq    = 1;
	m_pHttp[ index ].index    = index;

	m_sHttpRequestMax ++;

	return index;
}

gxBool gxNetworkManager::CloseURL(Uint32 uIndex)
{
	m_pHttp[uIndex].bClose = gxTrue;
	return gxTrue;
}


void gxNetworkManager::Action()
{
	if( m_pHttp.size() > 0 )
	{
		gxUIManager::GetInstance()->NowNetworking();

		std::vector<int> eraseList;

		for (auto itr = m_pHttp.begin(); itr != m_pHttp.end(); itr++)
		{
			if (itr->second.bClose)
			{
				eraseList.push_back(itr->first);
			}
		}
		for (Sint32 ii = 0; ii < eraseList.size(); ii++)
		{
			m_pHttp.erase(eraseList[ii]);
		}
	}



}


gxNetworkManager::StHTTP * gxNetworkManager::GetNextReq()
{
	//for(Sint32 ii=0; ii<gxNetworkManager::enHttpOpenMax; ii++ )

	if (m_pHttp.size() == 0) return NULL;


	for (auto itr = m_pHttp.begin(); itr != m_pHttp.end(); itr++ )
	{
		//gxNetworkManager::StHTTP *httpInfo;
		//httpInfo = gxNetworkManager::GetInstance()->GetHTTPInfo( ii );
		if(itr->second.bRequest )
		{
			return &itr->second;
		}
	}


	return NULL;
}

