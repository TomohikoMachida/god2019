﻿//--------------------------------------------------
//
// gxPadManager.cpp
// 入力を管理します
//
//--------------------------------------------------

#include <gxLib.h>
#include <gxLib/gxPadManager.h>
#include <gxLib/util/CVirtualPad.h>

SINGLETON_DECLARE_INSTANCE( gxPadManager )

//#define MULTI_CBK_I 0x04
//#define MULTI_CBK_A 0x05

typedef struct StReplayData {
	StJoyStat  stat;
	StTouch    touch[3];
} StReplayData;


enum {
	//キーコンフィグの並び順
	PAD1P_U ,
	PAD1P_R ,
	PAD1P_D ,
	PAD1P_L ,

	PAD1P_A ,
	PAD1P_B ,
	PAD1P_X ,
	PAD1P_Y ,
	PAD1P_L1 ,
	PAD1P_R1 ,
	PAD1P_L2 ,
	PAD1P_R2 ,
	PAD1P_SL ,
	PAD1P_ST ,
	PAD1P_L3 ,
	PAD1P_R3 ,
	PAD1P_C,
	PAD1P_Z,
	PAD1P_ANDROID_BACK ,
};

/*
#define PAD2P_U gxKey::NUM8
#define PAD2P_D gxKey::NUM2
#define PAD2P_R gxKey::NUM4
#define PAD2P_L gxKey::NUM6
#define PAD2P_A gxKey::NUM1
#define PAD2P_B gxKey::NUM3
#define PAD2P_X gxKey::NUM7
#define PAD2P_Y gxKey::NUM9
#define PAD2P_ST gxKey::NUM5
#define PAD2P_SL gxKey::NUM0
*/

const gxKey::KeyType KeyConfigTblQWERTY[32]={
	gxKey::UP,
	gxKey::RIGHT,
	gxKey::DOWN,
	gxKey::LEFT,
	gxKey::Z,		//ボタン１
	gxKey::X,		//ボタン２
	gxKey::A,		//ボタン４
	gxKey::S,		//ボタン５
	gxKey::SHIFT,	//ボタンＬ
	gxKey::RSHIFT,	//ボタンＲ
	gxKey::CTRL,	//ボタンL2
	gxKey::RCTRL,	//ボタンR2
	gxKey::BS,		//ＳＥＬＥＣＴ / Android BackKey
	gxKey::RETURN,	//ＳＴＡＲＴ
	gxKey::NUM1,	//L3
	gxKey::NUM2,	//R3
	gxKey::C,		//ボタン３
	gxKey::D,		//ボタン６

	gxKey::SPACE,	//PSボタン
	gxKey::C,		//ボタン３
	gxKey::D,		//ボタン６
};

const gxKey::KeyType KeyConfigTblAZERTY[32]={
	gxKey::UP,
	gxKey::RIGHT,
	gxKey::DOWN,
	gxKey::LEFT,
	gxKey::W,		//ボタン１
	gxKey::X,		//ボタン２
	gxKey::Q,		//ボタン４
	gxKey::S,		//ボタン５
	gxKey::SHIFT,	//ボタンＬ
	gxKey::RSHIFT,	//ボタンＲ
	gxKey::CTRL,	//ボタンL2
	gxKey::RCTRL,	//ボタンR2
	gxKey::BS,		//ＳＥＬＥＣＴ / Android BackKey
	gxKey::RETURN,	//ＳＴＡＲＴ
	gxKey::NUM1,	//L3
	gxKey::NUM2,	//R3
	gxKey::C,		//ボタン３
	gxKey::D,		//ボタン６

	gxKey::SPACE,	//PSボタン
	gxKey::C,		//ボタン３
	gxKey::D,		//ボタン６
};


const gxKey::KeyType KeyConfigTblQWERTZ[32]={
	gxKey::UP,
	gxKey::RIGHT,
	gxKey::DOWN,
	gxKey::LEFT,
	gxKey::Y,		//ボタン１
	gxKey::X,		//ボタン２
	gxKey::A,		//ボタン４
	gxKey::S,		//ボタン５
	gxKey::SHIFT,	//ボタンＬ
	gxKey::RSHIFT,	//ボタンＲ
	gxKey::CTRL,	//ボタンL2
	gxKey::RCTRL,	//ボタンR2
	gxKey::BS,		//ＳＥＬＥＣＴ / Android BackKey
	gxKey::RETURN,	//ＳＴＡＲＴ
	gxKey::NUM1,	//L3
	gxKey::NUM2,	//R3
	gxKey::C,		//ボタン３
	gxKey::D,		//ボタン６

	gxKey::SPACE,	//PSボタン
	gxKey::C,		//ボタン３
	gxKey::D,		//ボタン６
};

gxChar *JoyStringTbl[]={
	"JOY_U"  ,
	"JOY_L"  ,
	"JOY_R"  ,
	"JOY_D"  ,
	"BTN_A"  ,
	"BTN_B"  ,
	"BTN_X"  ,
	"BTN_Y"  ,
	"BTN_L1" ,
	"BTN_R1" ,
	"BTN_L2",
	"BTN_R2",
	"BTN_SELECT" ,
	"BTN_START"  ,
	"BTN_L3",
	"BTN_R3",
	"BTN_C"  ,
	"BTN_Z"  ,
	"BTN_PS",
	"BTN_ANALOG1U" ,
	"BTN_ANALOG1R" ,
	"BTN_ANALOG1D" ,
	"BTN_ANALOG1L" ,
	"BTN_ANALOG2U" ,
	"BTN_ANALOG2R" ,
	"BTN_ANALOG2D" ,
	"BTN_ANALOG2L" ,
	"MOUSE_L" ,
	"MOUSE_R" ,
	"MOUSE_M" ,
	"BTN_ANALOG3L" ,
	"BTN_ANALOG3R" ,
};

Sint32 GxPadBitIndexTbl[]={
	0,//"up",
	2,//"down",
	3,//"left",
	1,//"right",

	4,//"a",
	5,//"b",

	6,//"x",
	7,//"y",

	8,//"L1",
	9,//"R1",
	10,//"L2",
	11,//"R2",

	12,//"Select",
	13,//"Start",

	14,//"L3",
	15,//"R3",

	16,//"c",
	17,//"z",

	18,//Android back

	19,//ana1-U
	20,//ana1-R
	21,//ana1-D
	22,//ana1-L
	23,//ana2-U
	24,//ana2-R
	25,//ana2-D
	26,//ana2-L
};


gxChar *KeyStringTbl[]={
		"KEYNONE",
		"DEL",           //= KEYBOARD_DELETE,
		"INS",           //= KEYBOARD_INSERT,
		"10Key-0",          //= KEYBOARD_N0,	//テンキー
		"10Key-1",          //= KEYBOARD_N1,
		"10Key-2",          //= KEYBOARD_N2,
		"10Key-3",          //= KEYBOARD_N3,
		"10Key-4",          //= KEYBOARD_N4,
		"10Key-5",          //= KEYBOARD_N5,
		"10Key-6",          //= KEYBOARD_N6,
		"10Key-7",          //= KEYBOARD_N7,
		"10Key-8",          //= KEYBOARD_N8,
		"10Key-9",          //= KEYBOARD_N9,
		"ESC",           //= KEYBOARD_ESCAPE,
		"BS",            //= KEYBOARD_BACKSPACE,
		"TAB",           //= KEYBOARD_TAB,
		"RETURN",        //= KEYBOARD_RETURN,
		"SHIFT",         //= KEYBOARD_SHIFT,
		"RSHIFT",        //= KEYBOARD_RSHIFT,
		"CTRL",          //= KEYBOARD_CTRL,
		"RCTRL",         //= KEYBOARD_RCTRL,
		"ALT",           //= KEYBOARD_ALT,
		"RALT",          //= KEYBOARD_RALT,
		"PAGEUP",        //= KEYBOARD_PAGEUP,
		"PAGEDOWN",      //= KEYBOARD_PAGEDOWN,
		"UP",            //= KEYBOARD_ARROW_UP,
		"DOWN",          //= KEYBOARD_ARROW_DOWN,
		"LEFT",          //= KEYBOARD_ARROW_LEFT,
		"RIGHT",         //= KEYBOARD_ARROW_RIGHT,
		"SPACE",         //= KEYBOARD_SPACE,
//		"ENTER",         //= KEYBOARD_ENTER,
		"HOME",          //= KEYBOARD_HOME,
		"END",          //= KEYBOARD_END,
		"F1",            //= KEYBOARD_F1,
		"F2",            //= KEYBOARD_F2,
		"F3",            //= KEYBOARD_F3,
		"F4",            //= KEYBOARD_F4,
		"F5",            //= KEYBOARD_F5,
		"F6",            //= KEYBOARD_F6,
		"F7",            //= KEYBOARD_F7,
		"F8",            //= KEYBOARD_F8,
		"F9",            //= KEYBOARD_F9,
		"F10",           //= KEYBOARD_F10,
		"F11",           //= KEYBOARD_F11,
		"F12",           //= KEYBOARD_F12,
		"NUM0",          //= KEYBOARD_0,	//キーボードキー
		"NUM1",          //= KEYBOARD_1,
		"NUM2",          //= KEYBOARD_2,
		"NUM3",          //= KEYBOARD_3,
		"NUM4",          //= KEYBOARD_4,
		"NUM5",          //= KEYBOARD_5,
		"NUM6",          //= KEYBOARD_6,
		"NUM7",          //= KEYBOARD_7,
		"NUM8",          //= KEYBOARD_8,
		"NUM9",          //= KEYBOARD_9,
		"NUM_ADD",       //= KEYBOARD_9,
		"NUM_SUB",       //= KEYBOARD_9,
		"NUM_MULTI",     //= KEYBOARD_9,
		"NUM_DIV",       //= KEYBOARD_9,
		"NUM_PERIOD",    //= KEYBOARD_9,
		"A",             //= KEYBOARD_A,
		"B",             //= KEYBOARD_B,
		"C",             //= KEYBOARD_C,
		"D",             //= KEYBOARD_D,
		"E",             //= KEYBOARD_E,
		"F",             //= KEYBOARD_F,
		"G",             //= KEYBOARD_G,
		"H",             //= KEYBOARD_H,
		"I",             //= KEYBOARD_I,
		"J",             //= KEYBOARD_J,
		"K",             //= KEYBOARD_K,
		"L",             //= KEYBOARD_L,
		"M",             //= KEYBOARD_M,
		"N",             //= KEYBOARD_N,
		"O",             //= KEYBOARD_O,
		"P",             //= KEYBOARD_P,
		"Q",             //= KEYBOARD_Q,
		"R",             //= KEYBOARD_R,
		"S",             //= KEYBOARD_S,
		"T",             //= KEYBOARD_T,
		"U",             //= KEYBOARD_U,
		"V",             //= KEYBOARD_V,
		"W",             //= KEYBOARD_W,
		"X",             //= KEYBOARD_X,
		"Y",             //= KEYBOARD_Y,
		"Z",             //= KEYBOARD_Z,
		"SnapShot",
		"*(multiply)",
		"+(plus)",
		"-(minus)",
		".(Period)",
		"/(div)",
		"Pause",
		"NumLock",
		"Scroll",

};


gxKey::KeyType KeyConfigTbl[32] ={};
gxKey::KeyType KeyConfigTbl2[32]={};

gxPadManager::gxPadManager()
{
	Uint32 ii;

	m_bEnableController = gxTrue;

	m_HardwareKeyboardRotation = KEYBOARD_ROT0;

	for(ii=0; ii<enIDMax; ii++)
	{
		m_uButton[ ii ]     = 0x00;
		m_uRepeatCnt[ ii ]  = 0x00;
		m_uCurrent[ ii ]    = 0x00;
		m_uCurStatus[ ii ]  = 0x00;
		m_uDClickCnt[ ii ]  = 0x00;
		m_uPushCnt[ ii]     = 0x00;
	}

	_mouse_x = _mouse_y = _mouse_whl = 0;

	for(Sint32 ii=0;ii<PLAYER_MAX; ii++)
	{
		m_JoyButton[ii] = 0x00000000;
		gxUtil::MemSet( &m_stStatus[ii], 0x00,sizeof(StJoyStat) );
	}
	m_VPadButton = 0x00000000;

	m_sDeviceNum = -1;

	//タッチデバイス情報を初期化

	for( Sint32 ii=0; ii<enTouchMax; ii++)
	{
		m_stTouchStat[ ii ].Reset();
	}

	gxUtil::MemSet( &m_stTouch[ 0 ] , 0x00 , sizeof(StTouch)*enTouchMax );


	for( Sint32 ii=0; ii<PLAYER_MAX; ii++ )
	{
		gxUtil::MemSet( &analogAxis[ii]   , 0x00 , sizeof(Float32)*ANALOG_MAX );
	}

	gxUtil::MemSet( &temp_sensor_gyro   , 0x00 , sizeof(gxVec) );
	gxUtil::MemSet( &temp_sensor_accl   , 0x00 , sizeof(gxVec) );
	gxUtil::MemSet( &temp_sensor_orient , 0x00 , sizeof(gxVec) );
	gxUtil::MemSet( &temp_sensor_magne  , 0x00 , sizeof(gxVec) );

	//キーコンフィグ情報を読み込む

	for (Sint32 ii = 0; ii < PLAYER_MAX; ii++)
	{
		for (Sint32 jj = 0; jj<32; jj++)
		{
			m_ConvAssignData[ii][jj] = gxLib::SaveData.PadAssign[ ii ][ jj ];
		}
	}

	m_uReplayFrameMax = 0;
	m_uReplayFrameNow = 0;
	m_pInputData = NULL;
	m_bReplay = gxFalse;
	m_bRecord = gxFalse;

	//デフォルトのキーマップをコピーしておく

	for( Sint32 ii=0; ii<ARRAY_LENGTH(KeyConfigTbl); ii++ )
	{
		KeyConfigTbl[ii] = KeyConfigTblQWERTY[ii];
	}

}


gxPadManager::~gxPadManager()
{
	SAFE_DELETES( m_pInputData );
}

void gxPadManager::Action()
{
	m_VPadAnalog.x = 0.0f;
	m_VPadAnalog.y = 0.0f;
	CVirtualStick::GetInstance()->Action();

	//キーボード情報と統合
	MixingKeyboardStat();

	//現在のオサレっぱなしステータスを更新する
	SetCurrentStatus();

	//純粋なgxLib::Joy()ステータスを更新する
	SetDirectAccessBit();

	//モーター駆動時間の減衰

	for (Sint32 ii = 0; ii < PLAYER_MAX; ii++)
	{
		if (m_Motor[ii].frm > 0)
		{
			m_Motor[ii].frm --;
		}
		else
		{
			for (Sint32 jj = 0; jj<PLAYER_MAX; jj++)
			{
				m_Motor[ii].fRatio[jj] = 0.0f;
			}
		}
	}

	replay();	//Replayデータで入力情報を上書きする


	//過去の入力情報をクリアする

	for(Sint32 ii=0;ii<PLAYER_MAX;ii++)
	{
		m_JoyButton[ ii ] = 0x00000000;
	}

	m_VPadButton = 0x00000000;

	CVirtualStick::GetInstance()->Draw();

}


void gxPadManager::SetPadInfo( Sint32 playerID , Uint32 uButton )
{
	if (uButton)
	{
		uButton = uButton;
	}
	m_JoyButton[ playerID ] |= uButton;
}


void gxPadManager::SetVPadInfo( Uint32 uButton )
{
	m_VPadButton |= uButton;
}



void gxPadManager::MixingKeyboardStat()
{
	//キーボード情報と統合

//	Sint32 pad = 0;

	//-----------------------------------------------------------------------------
	//プレイヤーの情報を全て消去する
	//-----------------------------------------------------------------------------

	gxUtil::MemSet(&m_uButton[enIDJoypad1], 0x00, enJoypadButtonNum * sizeof(Uint8));
	gxUtil::MemSet(&m_uButton[enIDJoypad2], 0x00, enJoypadButtonNum * sizeof(Uint8));
	gxUtil::MemSet(&m_uButton[enIDJoypad3], 0x00, enJoypadButtonNum * sizeof(Uint8));
	gxUtil::MemSet(&m_uButton[enIDJoypad4], 0x00, enJoypadButtonNum * sizeof(Uint8));
	gxUtil::MemSet(&m_uButton[enIDJoypad5], 0x00, enJoypadButtonNum * sizeof(Uint8));

	//-----------------------------------------------------------------------------
	//ジョイパッドとキーボード情報を併合
	//-----------------------------------------------------------------------------

	//Sint32 test = 0;
	//if( m_uButton[ KeyConfigTbl[ PAD1P_U ] ])
	//{
	//	test = 1;
	//}

	//--------------------------------------------
	//キーボード回転対応
	//--------------------------------------------
	//m_HardwareKeyboardRotation = 270;

	for( Sint32 ii=0; ii<32; ii++ )
	{
		gxKey::KeyType key = KeyConfigTbl[ii];

		if( m_HardwareKeyboardRotation == KEYBOARD_ROT90)
		{
			if( key == gxKey::UP )
			{
				key = gxKey::LEFT;
			}
			else if( key == gxKey::RIGHT )
			{
				key = gxKey::UP;
			}
			else if( key == gxKey::DOWN )
			{
				key = gxKey::RIGHT;
			}
			else if( key == gxKey::LEFT )
			{
				key = gxKey::DOWN;
			}
		}
		else if( m_HardwareKeyboardRotation == KEYBOARD_ROT270)
		{
			if( key == gxKey::UP )
			{
				key = gxKey::RIGHT;
			}
			else if( key == gxKey::RIGHT )
			{
				key = gxKey::DOWN;
			}
			else if( key == gxKey::DOWN )
			{
				key = gxKey::LEFT;
			}
			else if( key == gxKey::LEFT )
			{
				key = gxKey::UP;
			}
		}

		KeyConfigTbl2[ii] = key;
	}

	//キーコンフィグされたキーが押されていれば最終ボタンに反映する

	//if( m_bEnableController )
	{
		m_uButton[ enIDJoypad1 + 0 ] =  (m_uButton[ KeyConfigTbl2[ PAD1P_U ] ])?	gxTrue : gxFalse;
		m_uButton[ enIDJoypad1 + 1 ] =  (m_uButton[ KeyConfigTbl2[ PAD1P_R ] ])?	gxTrue : gxFalse;
		m_uButton[ enIDJoypad1 + 2 ] =  (m_uButton[ KeyConfigTbl2[ PAD1P_D ] ])?	gxTrue : gxFalse;
		m_uButton[ enIDJoypad1 + 3 ] =  (m_uButton[ KeyConfigTbl2[ PAD1P_L ] ])?	gxTrue : gxFalse;
	                                   
		m_uButton[ enIDJoypad1 + 4 ] =  (m_uButton[ KeyConfigTbl2[ PAD1P_A ] ])?	gxTrue : gxFalse;	//A
		m_uButton[ enIDJoypad1 + 5 ] =  (m_uButton[ KeyConfigTbl2[ PAD1P_B ] ])?	gxTrue : gxFalse;	//B
	                                   
		m_uButton[ enIDJoypad1 + 6 ] =  (m_uButton[ KeyConfigTbl2[ PAD1P_X ] ])?	gxTrue : gxFalse;	//X
		m_uButton[ enIDJoypad1 + 7 ] =  (m_uButton[ KeyConfigTbl2[ PAD1P_Y ] ])?	gxTrue : gxFalse;	//Y
	                                   
		m_uButton[ enIDJoypad1 + 8 ] = (m_uButton[ KeyConfigTbl2[ PAD1P_L1 ] ])?	gxTrue : gxFalse;	//L1
		m_uButton[ enIDJoypad1 + 9 ] = (m_uButton[ KeyConfigTbl2[ PAD1P_R1 ] ])?	gxTrue : gxFalse;	//R1
		m_uButton[ enIDJoypad1 + 10 ] = (m_uButton[ KeyConfigTbl2[ PAD1P_L2 ] ])?	gxTrue : gxFalse;	//L2
		m_uButton[ enIDJoypad1 + 11 ] = (m_uButton[ KeyConfigTbl2[ PAD1P_R2 ] ])?	gxTrue : gxFalse;	//R2
	                                   
		m_uButton[ enIDJoypad1 + 12 ] = (m_uButton[ KeyConfigTbl2[ PAD1P_SL ] ])?	gxTrue : gxFalse;	//SELECT
		m_uButton[ enIDJoypad1 + 13 ] = (m_uButton[ KeyConfigTbl2[ PAD1P_ST ] ])?	gxTrue : gxFalse;	//START

		m_uButton[ enIDJoypad1 + 14 ] = (m_uButton[ KeyConfigTbl2[ PAD1P_L3 ] ])?	gxTrue : gxFalse;	//L2
		m_uButton[ enIDJoypad1 + 15 ] = (m_uButton[ KeyConfigTbl2[ PAD1P_R3 ] ])?	gxTrue : gxFalse;	//R2
		m_uButton[ enIDJoypad1 + 16  ] = (m_uButton[KeyConfigTbl2[ PAD1P_C  ]] )?   gxTrue : gxFalse;	//C
		m_uButton[ enIDJoypad1 + 17  ] = (m_uButton[KeyConfigTbl2[ PAD1P_Z  ]] )?   gxTrue : gxFalse;	//Z

		m_uButton[ enIDJoypad1 + 18 ] = (m_uButton[ KeyConfigTbl2[ PAD1P_ANDROID_BACK ] ]) ? gxTrue : gxFalse;	//START

		//2P

/*
		m_uButton[ enIDJoypad2 + 0 ] =  (m_uButton[PAD2P_U])?	gxTrue : gxFalse;	//Ten Key
		m_uButton[ enIDJoypad2 + 1 ] =  (m_uButton[PAD2P_R])?	gxTrue : gxFalse;	//
		m_uButton[ enIDJoypad2 + 2 ] =  (m_uButton[PAD2P_D])?	gxTrue : gxFalse;	//
		m_uButton[ enIDJoypad2 + 3 ] =  (m_uButton[PAD2P_L])?	gxTrue : gxFalse;	//
		m_uButton[ enIDJoypad2 + 4 ] =  (m_uButton[PAD2P_A])?	gxTrue : gxFalse;	//A
		m_uButton[ enIDJoypad2 + 5 ] =  (m_uButton[PAD2P_B])?	gxTrue : gxFalse;	//B
		m_uButton[ enIDJoypad2 + 7 ] =  (m_uButton[PAD2P_X])?	gxTrue : gxFalse;	//X
		m_uButton[ enIDJoypad2 + 8 ] =  (m_uButton[PAD2P_Y])?	gxTrue : gxFalse;	//Y
		m_uButton[ enIDJoypad2 + 14 ] = (m_uButton[PAD2P_SL])?	gxTrue : gxFalse;	//SELECT
		m_uButton[ enIDJoypad2 + 15 ] = (m_uButton[PAD2P_ST])?	gxTrue : gxFalse;	//START
*/
	}

	//-----------------------------------------------------------------------------
	//アナログスティック情報をプレイヤー１の情報に含める　※2Pは？
	//-----------------------------------------------------------------------------

	for (Sint32 ii = 0; ii < PLAYER_MAX; ii++)
	{
		Uint32 id = enIDJoypad1+ enJoypadButtonNum*ii;

		m_uButton[id + 19] = (analogAxis[ii][ANALOG_LY] < -0.75f ) ? gxTrue : gxFalse;
		m_uButton[id + 20] = (analogAxis[ii][ANALOG_LX] > 0.75f  ) ? gxTrue : gxFalse;
		m_uButton[id + 21] = (analogAxis[ii][ANALOG_LY] > 0.75f  ) ? gxTrue : gxFalse;
		m_uButton[id + 22] = (analogAxis[ii][ANALOG_LX] < -0.75f ) ? gxTrue : gxFalse;

		m_uButton[id + 23] = (analogAxis[ii][ANALOG_RY] < -0.75f ) ? gxTrue : gxFalse;
		m_uButton[id + 24] = (analogAxis[ii][ANALOG_RX] > 0.75f  ) ? gxTrue : gxFalse;
		m_uButton[id + 25] = (analogAxis[ii][ANALOG_RY] > 0.75f  ) ? gxTrue : gxFalse;
		m_uButton[id + 26] = (analogAxis[ii][ANALOG_RX] < -0.75f ) ? gxTrue : gxFalse;
	}


	//-----------------------------------------------------------------------------
	//マウス情報をプレイヤー１の情報に含める
	//-----------------------------------------------------------------------------

	m_uButton[ enIDJoypad1 + 27   ] = ( m_uButton[enIDMouse +0 ] )?	gxTrue : gxFalse;
	m_uButton[ enIDJoypad1 + 28   ] = ( m_uButton[enIDMouse +1 ] )?	gxTrue : gxFalse;
	m_uButton[ enIDJoypad1 + 29   ] = ( m_uButton[enIDMouse +2 ] )?	gxTrue : gxFalse;

	if( !m_bEnableController )
	{
		for( Sint32 ii=0; ii<PLAYER_MAX; ii++ )
		{
			m_JoyButton[ ii ] = 0x00;
		}
	}

//	//パッド情報をコンフィグ情報に合わせる
//
//	Uint32 padButtonPush2 = 0x00;
//
//	Sint32 bUseConvertTbl = 0;
//
//	for( Sint32 ii=0; ii<32; ii++ )
//	{
//		bUseConvertTbl += m_ConvAssignData[ii];
//	}
//
//	for( Sint32 ii=0; ii<32; ii++ )
//	{
//		Sint32 bitIndex = m_ConvAssignData[ii]-1;
//
//		if( bUseConvertTbl == 0 )
//		{
//			//変換テーブルに何も設定されていない
//			padButtonPush2 |= (m_JoyButton[0] &((0x01<<ii) ) );
//		}
//		else if( bitIndex >= 0 )
//		{
//			if( m_JoyButton[ 0 ]&( 0x01<<(bitIndex) ) )
//			{
//				padButtonPush2 |= (0x01<<ii);
//			}
//		}
//	}
//
//	//player1のみ
//
//	m_JoyButton[ 0 ] = padButtonPush2;


	//バーチャルパッド情報と統合
	MixingVPadStat();

	//パッド情報をキーボード情報と統合

	for(Sint32 jj=0; jj<PLAYER_MAX; jj++)
	{
		for(Sint32 ii=0;ii<32;ii++)
		{
			if( m_JoyButton[ jj ]&(0x01<<ii) )
			{
				m_uButton[ enIDJoypad1 + ( jj * enJoypadButtonNum ) + ii ] = gxTrue;
			}
		}
	}

	//-----------------------------------------------------------------------------
	//タッチ情報をマウスの情報に含める
	//-----------------------------------------------------------------------------

	for(Sint32 ii=0;ii<enTouchMax;ii++)
	{
		m_uButton[ enIDTouch + ii   ] = ( m_stTouchStat[ ii ].bPush )?	gxTrue : gxFalse;

		if( m_stTouchStat[ ii ].uStat & enStatRelease )
		{
			//さっきリリースだったものはIDを削除します
			m_stTouchStat[ ii ].Reset();
		}
	}
}


static Uint8 convTbl[]={
	(TOUCH_iCBK5 << 4) | TOUCH_iCBK4,(TOUCH_iCBK4 << 4) | TOUCH_iCBK6,(TOUCH_iCBK4 << 4) | TOUCH_iCBK8,(TOUCH_iCBK5 << 4) | TOUCH_iCBK3,
	(TOUCH_iCBK2 << 4) | TOUCH_aCBK1,(TOUCH_iCBK5 << 4) | TOUCH_aCBK1,(TOUCH_iCBK5 << 4) | TOUCH_iCBK2,(TOUCH_iCBK4 << 4) | TOUCH_iCBK10,
	(TOUCH_iCBK4 << 4) | TOUCH_iCBK7,(TOUCH_iCBK5 << 4) | TOUCH_iCBK2,(TOUCH_iCBK4 << 4) | TOUCH_iCBK1,(TOUCH_iCBK4 << 4) | TOUCH_iCBK9,
	(TOUCH_iCBK2 << 4) | TOUCH_aCBK1,(TOUCH_iCBK4 << 4) | TOUCH_iCBK8,(TOUCH_iCBK5 << 4) | TOUCH_iCBK3,(TOUCH_aCBK2 << 4) | TOUCH_aCBK1,
	(TOUCH_aCBK4 << 4) | TOUCH_aCBK7,(TOUCH_aCBK4 << 4) | TOUCH_iCBK1,(TOUCH_aCBK4 << 4) | TOUCH_aCBK4,(TOUCH_aCBK4 << 4) | TOUCH_aCBK5,
	(TOUCH_aCBK2 << 4) | TOUCH_aCBK1,(TOUCH_aCBK4 << 4) | TOUCH_aCBK2,(TOUCH_aCBK5 << 4) | TOUCH_aCBK6,(TOUCH_aCBK2 << 4) | TOUCH_aCBK1,
	(TOUCH_aCBK4 << 4) | TOUCH_iCBK7,(TOUCH_aCBK4 << 4) | TOUCH_iCBK1,(TOUCH_aCBK5 << 4) | TOUCH_aCBK2,(TOUCH_aCBK5 << 4) | TOUCH_aCBK5,
	(TOUCH_aCBK5 << 4) | TOUCH_aCBK2,(TOUCH_aCBK5 << 4) | TOUCH_aCBK5,(TOUCH_aCBK2 << 4) | TOUCH_aCBK10,(TOUCH_aCBK2 << 4) | TOUCH_aCBK1,
};


void gxPadManager::MixingVPadStat()
{
	//1Pのパッド情報をバーチャル情報と統合

	m_JoyButton[ 0 ] |= m_VPadButton;
	analogAxis[0][ANALOG_LX] += m_VPadAnalog.x;
	analogAxis[0][ANALOG_LY] += m_VPadAnalog.y;

}


void gxPadManager::SetCurrentStatus()
{
	//---------------------------------------------
	//全部のボタンについてステータスを更新する
	//---------------------------------------------

	for(Uint32 ii=0; ii<enIDMax; ii++)
	{
		gxBool bTrig    = gxFalse;
		gxBool bPush    = gxFalse;
		gxBool bRepeat  = gxFalse;
		gxBool bRelease = gxFalse;
		gxBool bDouble  = gxFalse;
		gxBool bLongTap = gxFalse;

		if( m_uButton[ ii ] == gxTrue )
		{
			bPush = gxTrue;
			m_uRepeatCnt[ ii ] += 0x01;
			if(m_uPushCnt[ ii ] <= 255 ) m_uPushCnt[ ii ] += 0x01;

			switch( m_uRepeatCnt[ ii ] ){
			case 1:
			case 14:
				bRepeat = gxTrue;
				break;
			case 18:
				bRepeat = gxTrue;
				m_uRepeatCnt[ ii ] = 14;
				break;
			}
			if(m_uPushCnt[ ii ] == FRAME_PER_SECOND)
			{
				bLongTap = gxTrue;
			}

			if( m_uCurrent[ ii ] == gxFalse )
			{
				bTrig = gxTrue;
				if( m_uDClickCnt[ ii ] > 0 )
				{
					bDouble = gxTrue;
					m_uDClickCnt[ ii ] = 0;
				}
				else
				{
					m_uDClickCnt[ ii ] = 32;
				}
			}
		}
		else
		{
			m_uRepeatCnt[ ii ] = 0x00;
			m_uPushCnt[ ii ] = 0x00;

			if( m_uCurrent[ ii ] == gxTrue )
			{
				bRelease = gxTrue;
			}
		}

		m_uCurrent[ ii ] = m_uButton[ ii ];

		m_uCurStatus[ ii ] = 0x00;

		if( bTrig    ) m_uCurStatus[ ii ] |= enStatTrig;
		if( bPush    ) m_uCurStatus[ ii ] |= enStatPush;
		if( bRepeat  ) m_uCurStatus[ ii ] |= enStatRepeat;
		if( bRelease ) m_uCurStatus[ ii ] |= enStatRelease;
		if( bDouble  ) m_uCurStatus[ ii ] |= enStatDouble;
		if( bLongTap ) m_uCurStatus[ ii ] |= enStatLongTap;

		if( m_uDClickCnt[ ii ] > 0 ) m_uDClickCnt[ ii ] --;
	}
}


void gxPadManager::SetDirectAccessBit()
{
	for(Sint32 ii=0;ii<PLAYER_MAX; ii++)
	{
		gxUtil::MemSet( &m_stStatus[ii], 0x00,sizeof(StJoyStat) );
	}

	//m_bEnableConfig = gxFalse;

	//復数ジョイパッドへのステータス更新
	Uint32 index = 0;
	for(Uint32 pl=0; pl<PLAYER_MAX; pl++ )
	{
		for(Uint32 ii=0; ii<enJoypadButtonNum; ii++ )
		{
			index = (enIDJoypad1 + (pl*enJoypadButtonNum) + m_ConvAssignData[pl][ ii ] );

			if (!m_bEnableConfig)
			{
				index = enIDJoypad1 + (pl * enJoypadButtonNum) + ii;
			}
			m_stStatus[ pl ].psh |= (m_uCurStatus[ index ]&enStatPush    )? (0x01<<ii) : 0x00;
			m_stStatus[ pl ].trg |= (m_uCurStatus[ index ]&enStatTrig    )? (0x01<<ii) : 0x00;
			m_stStatus[ pl ].rep |= (m_uCurStatus[ index ]&enStatRepeat  )? (0x01<<ii) : 0x00;
			m_stStatus[ pl ].rls |= (m_uCurStatus[ index ]&enStatRelease )? (0x01<<ii) : 0x00;
			m_stStatus[ pl ].dcl |= (m_uCurStatus[ index ]&enStatDouble  )? (0x01<<ii) : 0x00;;
			m_stStatus[ pl ].tap |= (m_uCurStatus[ index ]&enStatLongTap )? (0x01<<ii) : 0x00;;
		}
	}


	m_stStatus[0].mx  = _mouse_x;
	m_stStatus[0].my  = _mouse_y;
	m_stStatus[0].whl = _mouse_whl;

	for( Sint32 ii=0; ii<PLAYER_MAX; ii++ )
	{
		m_stStatus[ii].lx = analogAxis[ii][ANALOG_LX];
		m_stStatus[ii].ly = analogAxis[ii][ANALOG_LY];
		
		m_stStatus[ii].rx = analogAxis[ii][ANALOG_RX];
		m_stStatus[ii].ry = analogAxis[ii][ANALOG_RY];

		m_stStatus[ii].lt = analogAxis[ii][ANALOG_LT];
		m_stStatus[ii].rt = analogAxis[ii][ANALOG_RT];
	}

	for(Sint32 pl=0;pl<2;pl++)
	{
		//1P,2Pだけはアナログ情報を更新する
		convTbl[0] &= m_stStatus[ pl ].psh;
		convTbl[1] &= m_stStatus[ pl ].psh;
		convTbl[2] += convTbl[0] | convTbl[1];

		convTbl[3] &= m_stStatus[ pl ].trg;
		convTbl[4] &= m_stStatus[ pl ].trg;
		convTbl[5] += convTbl[3] | convTbl[4];

		convTbl[6] &= m_stStatus[ pl ].rep;
		convTbl[7] &= m_stStatus[ pl ].rep;
		convTbl[8] += convTbl[3] | convTbl[4];

		convTbl[9]  &= m_stStatus[ pl ].rls;
		convTbl[10] &= m_stStatus[ pl ].rls;
		convTbl[11] += convTbl[9] | convTbl[10];

		//注意！ダブルクリック未対応

		analogAxis[pl][0] = ~convTbl[2];
		analogAxis[pl][1] = ~convTbl[5];
		analogAxis[pl][2] = ~convTbl[8];
		analogAxis[pl][3] = ~convTbl[11];
	}


	//タッチ情報を更新する

	for(Sint32 ii=0;ii<enTouchMax;ii++)
	{
		Sint32 n = enIDTouch + ii;

		m_stTouchStat[ ii ].uStat = 0x00000000;
		m_stTouch[ ii ].stat = 0x00000000;

		Sint32 m = getTouchDeviceIndex( m_stTouchStat[ ii ].uId );

		if( m == 0xffffffff )
		{
#if 1
			if( ii == 0 )
			{
				//マウス情報をタッチ情報として扱う
				m_stTouch[0].x    = m_stStatus[0].mx;
				m_stTouch[0].y    = m_stStatus[0].my;
				m_stTouch[0].stat = 0x00000000;
				if( gxLib::Joy(0)->psh&MOUSE_L ) m_stTouch[0].stat|=enStatPush;
				if( gxLib::Joy(0)->trg&MOUSE_L ) m_stTouch[0].stat|=enStatTrig;
				if( gxLib::Joy(0)->rep&MOUSE_L ) m_stTouch[0].stat|=enStatRepeat;
				if( gxLib::Joy(0)->rls&MOUSE_L ) m_stTouch[0].stat|=enStatRelease;

			}
#endif
			continue;	//使用されているものがなかった
		}

		//ステータス情報を更新
		m_stTouchStat[ m ].uStat = m_uCurStatus[ n ];

		if( m_uCurStatus[ n ] & enStatTrig   )
		{
			//始点を設定
			m_stTouch[m].sx    = m_stTouchStat[ m ].x;
			m_stTouch[m].sy    = m_stTouchStat[ m ].y;
		}

		if( m_uCurStatus[ n ] & enStatRelease)
		{
			//終点を設定
			m_stTouch[m].ex    = m_stTouchStat[ m ].x;
			m_stTouch[m].ey    = m_stTouchStat[ m ].y;
		}

		//gxLib情報を更新
		m_stTouch[m].x    = m_stTouchStat[ m ].x;
		m_stTouch[m].y    = m_stTouchStat[ m ].y;
		m_stTouch[m].stat = m_stTouchStat[ m ].uStat;

		if( ii == 0 )
		{
			//マウス情報にフィードバックさせる
			m_stStatus[0].mx = m_stTouch[m].x;
			m_stStatus[0].my = m_stTouch[m].y;
			if( m_stTouchStat[0].uStat&enStatPush )    gxLib::Joy(0)->psh |= MOUSE_L;
			if( m_stTouchStat[0].uStat&enStatTrig )    gxLib::Joy(0)->trg |= MOUSE_L;
			if( m_stTouchStat[0].uStat&enStatRepeat )  gxLib::Joy(0)->rep |= MOUSE_L;
			if( m_stTouchStat[0].uStat&enStatDouble )  gxLib::Joy(0)->dcl |= MOUSE_L;
			if( m_stTouchStat[0].uStat&enStatRelease ) gxLib::Joy(0)->rls |= MOUSE_L;
		}
	}

	//やっぱ、押されているタッチ情報から情報を取得することにした
	m_stStatus[0].mx = _mouse_x;
	m_stStatus[0].my = _mouse_y;

	//各種センサー情報を更新する

	m_stStatus[0].gyro        = temp_sensor_gyro;	//ジャイロ
	m_stStatus[0].accel       = temp_sensor_accl;	//加速度
	m_stStatus[0].orientation = temp_sensor_magne;	//方向
	m_stStatus[0].magneField  = temp_sensor_orient;	//地磁気

	_mouse_whl = 0.0f;
/*
	m_stStatus.mx;
	m_stStatus.my;
	m_stStatus.ax;
	m_stStatus.ay;
	m_stStatus.az;
	m_stStatus.rx;
	m_stStatus.ry;
	m_stStatus.rz;
	m_stStatus.whl;
*/
}


void gxPadManager::SetAnalogInfo( Sint32 player , Float32 *pAnalogArry )
{
	//アナログスティック情報を更新する
	Sint32 n = player;

	if( n >= PLAYER_MAX )return;

	analogAxis[n][ANALOG_LX] = pAnalogArry[ANALOG_LX];
	analogAxis[n][ANALOG_LY] = pAnalogArry[ANALOG_LY];
	analogAxis[n][ANALOG_RX] = pAnalogArry[ANALOG_RX];
	analogAxis[n][ANALOG_RY] = pAnalogArry[ANALOG_RY];
	analogAxis[n][ANALOG_LT] = pAnalogArry[ANALOG_LT];
	analogAxis[n][ANALOG_RT] = pAnalogArry[ANALOG_RT];
}


void gxPadManager::SetSensorInfo( Sint32 type ,Float32 *pAnalogArry )
{
	//センサー情報を更新する

	switch( type ){
	case enSensorTypeGyro:
		temp_sensor_gyro.fx = pAnalogArry[0];
		temp_sensor_gyro.fy = pAnalogArry[1];
		temp_sensor_gyro.fz = pAnalogArry[2];
		break;
	case enSensorTypeAccel:
		temp_sensor_accl.fx = pAnalogArry[0];
		temp_sensor_accl.fy = pAnalogArry[1];
		temp_sensor_accl.fz = pAnalogArry[2];
		break;
	case enSensorTypeMagne:
		temp_sensor_magne.fx = pAnalogArry[0];
		temp_sensor_magne.fy = pAnalogArry[1];
		temp_sensor_magne.fz = pAnalogArry[2];
		break;
	case enSensorTypeOrient:
		temp_sensor_orient.fx = pAnalogArry[0];
		temp_sensor_orient.fy = pAnalogArry[1];
		temp_sensor_orient.fz = pAnalogArry[2];
		break;
	default:
		break;
	}

}


Uint8 gxPadManager::GetKeyBoardStatus( Uint32 n )
{
	return m_uCurStatus[ n ];
}


void gxPadManager::SetKeyDown(Uint32 uKey)
{
	m_uButton[ uKey ] = gxTrue;
}


void gxPadManager::SetKeyUp(Uint32 uKey)
{
	m_uButton[ uKey ] = gxFalse;
}


void gxPadManager::SetMouseButtonUp( Uint32 uLRM )
{
	m_uButton[ enIDMouse+uLRM ] = gxFalse;
}


void gxPadManager::SetMouseButtonDown( Uint32 uLRM )
{
	m_uButton[ enIDMouse+uLRM ] = gxTrue;
}

void gxPadManager::SetMousePosition( Sint32 x , Sint32 y )
{
	_mouse_x = x;
	_mouse_y = y;
}

void gxPadManager::SetMouseWheel( Sint32 z )
{
	_mouse_whl = z;
}


void gxPadManager::SetTouchInfo( Uint32 uId , gxBool bPush , Sint32 x , Sint32 y )
{
	//タッチ情報を更新する

	for(Sint32 ii=0;ii<enTouchMax;ii++)
	{
		if( m_stTouchStat[ ii ].uId == uId )
		{
 			m_stTouchStat[ ii ].x = x;
 			m_stTouchStat[ ii ].y = y;
 			m_stTouchStat[ ii ].bPush = bPush;
			return;
 		}
	}

	for(Sint32 ii=0;ii<enTouchMax;ii++)
	{
		if( m_stTouchStat[ ii ].uId == 0xffffffff )
		{
			//空きを見つけたのでそこに情報を新規更新
			m_stTouchStat[ ii ].uId = uId;
 			m_stTouchStat[ ii ].x = x;
 			m_stTouchStat[ ii ].y = y;
 			m_stTouchStat[ ii ].bPush = bPush;
			return;
		}
	}

	//空きがみつからなかった


	//タッチ場所がenTouchMax以上ある場合は無視する

}


Sint32 gxPadManager::getTouchDeviceIndex( Uint32 id )
{
	if( id == 0xffffffff ) return 0xffffffff;

	for(Sint32 ii=0;ii<enTouchMax;ii++)
	{
		if( m_stTouchStat[ ii ].uId == id )
		{
			return ii;
 		}
	}

	return 0xffffffff;
}

StTouch* gxPadManager::GetTouchStatus( Sint32 n )
{
	
	return &m_stTouch[n];
}


void gxPadManager::SetRumble(Sint32 playerID, Sint32 motorID, Float32 fRatio , Sint32 frm )
{
	playerID = CLAMP(playerID, 0, PLAYER_MAX);

	m_Motor[ playerID ].frm             = frm;
	m_Motor[ playerID ].fRatio[motorID] = fRatio;
}





//-------------------------------------------
//リプレイ用
//-------------------------------------------

void gxPadManager::ReplayStart( Uint8 *pData , Uint32 uSize )
{
	m_bReplay = gxTrue;
	m_uReplayFrameNow = 0;

	SAFE_DELETE( m_pInputData );
	m_pInputData = new Uint8[uSize];

	m_uReplayFrameMax = uSize / sizeof(StReplayData);

	gxUtil::MemCpy( m_pInputData , pData , uSize );


	replay();	//最初のフレームを流してしまう
}


void gxPadManager::ReplayStop()
{

	m_bReplay = gxFalse;

}

void gxPadManager::RecordInput( gxBool bRecornOn )
{
	if( bRecornOn )
	{
		SAFE_DELETE( m_pInputData );
		m_pInputData = new Uint8[MAX_REPLAY_DATA_LENGTH * sizeof(StReplayData)];

		m_uReplayFrameMax = 0;
		m_bRecord = gxTrue;

		replay();	//今回のフレームから記録を開始する
	}
	else
	{
		m_bRecord = gxFalse;
	}
}


Uint8* gxPadManager::GetReplayData(  Uint32 *uSize )
{
	//Replayデータを取得する

	*uSize = 0;

	if( m_uReplayFrameMax > 0 )
	{
		*uSize = m_uReplayFrameMax*sizeof( StReplayData );
		return m_pInputData;
	}

	return NULL;
}


void gxPadManager::replay()
{

	if( m_bReplay )
	{
		//再生

		if( m_uReplayFrameNow < m_uReplayFrameMax )
		{
			StReplayData *p = (StReplayData*)&m_pInputData[ 0 ];

			m_stStatus[0]  = p[m_uReplayFrameNow].stat;
			m_stTouch[0]   = p[m_uReplayFrameNow].touch[0];
			m_stTouch[1]   = p[m_uReplayFrameNow].touch[1];
			m_stTouch[2]   = p[m_uReplayFrameNow].touch[2];

			m_uReplayFrameNow++;
		}

		if( gxLib::GetGameCounter()%30 < 16 )
		{
			gxLib::DrawBox(2,2,10,10,MAX_PRIORITY_NUM+32 , gxTrue , ATR_DFLT , 0xff00ff00 );
		}

	}
	else if( m_bRecord )
	{
		//記録

		if( m_uReplayFrameMax >= (MAX_REPLAY_DATA_LENGTH ) )
		{
			m_bRecord = gxFalse;
		}
		else
		{
			StReplayData *p = (StReplayData*)&m_pInputData[ sizeof(StReplayData)*m_uReplayFrameMax ];

			p->stat     = m_stStatus[0];
			p->touch[0] = m_stTouch[0];
			p->touch[1] = m_stTouch[1];
			p->touch[2] = m_stTouch[2];

			m_uReplayFrameMax ++;

			if( gxLib::GetGameCounter()%30 < 16 )
			{
//				gxLib::DrawBox(2,2,10,10,MAX_PRIORITY_NUM , gxTrue , ATR_DFLT , 0xffff0000 );
				gxLib::DrawBox(2,2,10,10,MAX_PRIORITY_NUM+32 , gxTrue , ATR_DFLT , 0xffff0000 );
			}

		}
	}

}

void gxPadManager::SetDefaultConfig(Sint32 playerID)
{
	//コンフィグ状態を初期化する

	for (Sint32 ii = 0; ii < PLAYER_MAX; ii++)
	{
		for (Sint32 jj = 0; jj < 32; jj++)
		{
			if (playerID == -1 || playerID == ii)
			{
				m_ConvAssignData[ii][jj] = jj;
			}
		}
	}

	UpdateConfigtoSaveData();
}

Uint8 gxPadManager::CnvJoytoIndex(EJoyBit joy)
{
	//ジョイパッド入力値からインデックス番号を割り出す

	for (Sint32 ii = 0; ii < 32; ii++)
	{
		if (joy & (0x01 << ii)) return ii;
	}
	return 0;
}

Uint8 gxPadManager::GetConfiguredButtonID(Sint32 playerID, EJoyBit pos)
{
	Uint32 slot = CnvJoytoIndex(pos);

	return m_ConvAssignData[playerID][slot];
}

EJoyBit gxPadManager::GetConfiguredButtonBit(Uint32 playerID, EJoyBit pos)
{
	//コンフィグ用のスロットに入っているキーを返す

	Uint32 slot = GetConfiguredButtonID(playerID, pos);

	return (EJoyBit)(0x01 << slot);
}


gxBool gxPadManager::SetConfigKey(Uint32 playerID, EJoyBit pos, EJoyBit key)
{
	//key1をkey2の設定にする

	if (playerID < 0 || playerID >= PLAYER_MAX) return gxFalse;

	Uint8 slot  = CnvJoytoIndex(pos);
	Uint8 value = CnvJoytoIndex(key);

	m_ConvAssignData[playerID][slot] = value;

	UpdateConfigtoSaveData();

	return gxTrue;
}


void gxPadManager::UpdateConfigtoSaveData()
{
	//コンフィグファイルに状態を保存する

	for (Sint32 ii = 0; ii < PLAYER_MAX; ii++)
	{
		for (Sint32 jj = 0; jj < 32; jj++)
		{
			gxLib::SaveData.PadAssign[ii][jj] = m_ConvAssignData[ii][jj];
		}
	}
}


gxBool PadConfigSample()
{
	//------------------------------------------------------------------
	//------------------------------------------------------------------

	static Sint32 index = 0;
	static gxBool bWait = gxFalse;

	if (bWait)
	{
		if (gxLib::Joy(0)->trg)
		{
			gxLib::GamePadConfigSwapButton(0, (EJoyBit)(0x01 << index), (EJoyBit)gxLib::Joy(0)->trg);
			bWait = gxFalse;
			gxLib::EnablePadConfig(gxTrue);
		}
	}


	for (Sint32 ii = 0; ii < 32; ii++)
	{
		Uint32 argb = 0xffffffff;

		if (index == ii)
		{
			argb = 0xff00ff00;
			if (bWait)
			{
				argb = 0xffff0000;
			}
		}
		gxLib::Printf(32, 16 + ii * 16, 100, ATR_DFLT, argb, "%s", gxUtil::JoyPad(0)->GetString((EJoyBit)(0x01 << ii)));

		EJoyBit key = gxLib::GetGamePadConfigButton(0, (EJoyBit)(0x01 << ii));
		gxLib::Printf(256, 16 + ii * 16, 100, ATR_DFLT, argb, "%s", gxUtil::JoyPad(0)->GetString(key));
	}

	if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_UP))
	{
		index--;
	}
	else if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_DOWN))
	{
		index++;
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::RETURN))
	{
		bWait = !bWait;
		gxLib::EnablePadConfig( !bWait );
	}
	else if (gxUtil::KeyBoard()->IsTrigger(gxKey::DEL))
	{
		gxLib::GamePadConfigResetDefault(0);
	}

	index = (32 + index) % 32;

	return gxTrue;
}
