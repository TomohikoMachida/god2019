﻿//--------------------------------------------------
//
// gxMain.h
// メインループ制御
//
//--------------------------------------------------

#include <gxLib.h>
#include "gx.h"
#include "gxOrderManager.h"
#include "gxTexManager.h"
#include "gxRender.h"
#include "gxPadManager.h"
#include "gxSoundManager.h"
#include "gxMovieManager.h"
#include "gxFileManager.h"
#include "gxNetworkManager.h"
#include "gxDebug.h"
#include "gxBlueTooth.h"
#include "util/gxUIManager.h"
#include <gxLib/util/Font/CFontManager.h>
#include <gxLib/util/CCsv.h>

//#define BGV_ON

enum {
	enOrderRequestSeqInit,
	enOrderRequestSeqAccept,
	enOrderRequestSeqEnd,
};

void* GameMainThread(void *pArgs);
void* GameNetWorkThread(void*);
void* GameFileThread(void*);

SINGLETON_DECLARE_INSTANCE( CGameGirl );

CGameGirl::CGameGirl()
{
	m_bMainLoop  = gxTrue;
	m_bAppFinish = gxFalse;
	m_bResume    = gxFalse;

	m_bHardPause     = gxFalse;
	m_bSoftPause     = gxFalse;
	m_sStepFrm = 0;

	m_sTimer = 0;
	m_uGameCounter = 0;
	m_sFrameSkip = 0;

	m_uMemoryTotal   = 1;
	m_uMemoryMaximum = 1;
	m_uMemoryUse     = 1;

	m_bResetButton   = gxFalse;
	m_bPadDeviceConfigMode = gxFalse;

	m_bInitializeCompleted = gxFalse;

	m_bWaitVSync = gxTrue;

	m_uGlobalUID = 0x00000000;
	m_uGlobalIP  = 0x00000000;
	m_uLocalIP   = 0x00000000;
	m_bOnLine    = gxFalse;

	m_bSubThread = gxFalse;
	m_b3DView    = gxFalse;

	//m_sRequestAccept = enOrderRequestSeqInit;
	gxOrderManager::GetInstance()->RequestInit();

	m_bToggleDeviceMode = gxFalse;

	m_pFontManager = new CFontManager();

	m_bThroughGameMain = gxFalse;

	m_ScreenMode = enScreenModeAspect;

	m_GameScreenWidth  = WINDOW_W;
	m_GameScreenHeight = WINDOW_H;

	m_WindowScreenWidth  = WINDOW_W;
	m_WindowScreenHeight = WINDOW_H;

	gxFileManager::CreateInstance();
}


CGameGirl::~CGameGirl()
{
	gxUIManager::DeleteInstance();
	gxFileManager::DeleteInstance();
	gxNetworkManager::DeleteInstance();
	CBlueToothManager::DeleteInstance();

	SAFE_DELETE( m_pFontManager );
}

void CGameGirl::Init()
{
	CDeviceManager::GetInstance()->Clock(&m_StartTime);

	if( !gxLib::LoadConfig() )
	{
		//ロード失敗したのでデフォルトデータを作る

		gxLib::SaveData.makeDefault();

	}
	
	//if( !m_bAppFinish )
	{
		GX_DEBUGLOG("[GameGirl] Application Init.");

		drawInit();
		soundInit();
		movieInit();
		inputInit();
	}

	m_bInitializeCompleted = gxTrue;

	//m_sRequestAccept = enOrderRequestSeqInit;
	gxOrderManager::GetInstance()->RequestInit();
}


void CGameGirl::Action()
{
	if( !IsExist() )
	{
		GX_DEBUGLOG("[GameGirl] Accept Exit Signal.");
		m_bAppFinish = gxTrue;
		return;
	}


	//MULTIスレッド時は別スレッドでゲームが回っている
	static gxBool m_bFirst = gxTrue;

	if (m_bFirst)
	{
		gxDebug::GetInstance()->SetAlreadyUseRAM();

#ifdef _USE_MULTITHREAD_
		gxLib::CreateThread(GameMainThread, NULL);

    #ifdef PLATFORM_ANDROID

    #else
        gxLib::CreateThread(GameNetWorkThread, NULL);
        gxLib::CreateThread(GameFileThread, NULL);
    #endif
		GX_DEBUGLOG("[GameGirl] Activate MultiThread.");
#endif
		m_bFirst = gxFalse;
	}


#ifdef _USE_MULTITHREAD_

#else
	GameMain();
#endif


#ifdef _USE_MULTITHREAD_
	#ifdef PLATFORM_ANDROID
		CGameGirl::GetInstance()->network();
        gxFileManager::GetInstance()->Action();
	#endif
#else
	gxFileManager::GetInstance()->Action();
	CGameGirl::GetInstance()->network();
#endif

	if(gxTexManager::GetInstance()->IsUploadTextureExist())
	{
		gxTexManager::GetInstance()->UploadTexture();	//スレッド使うとどうもバッティングする？
	}

	soundMain();

	if( gxOrderManager::GetInstance()->IsDrawable() )
	{
		//リクエストの受付を締め切っていれば描画開始
		{
			gxOrderManager::GetInstance()->DrawInit();

			gxDebug::GetInstance()->Action();

			movieMain();

			drawMain();

			gxOrderManager::GetInstance()->Reset();

			gxDebug::GetInstance()->AddDrawCount();

			gxOrderManager::GetInstance()->DrawEnd();
		}
	}
	else
	{
		return;
	}


#ifdef _USE_MULTITHREAD_


#else

	VSync();
#endif

	flip();

	if( m_bResume )
	{
		resume();
		m_bResume = gxFalse;
	}

}

void CGameGirl::GameMain()
{
	//ゲーム用のサブスレッドの中身

	if( m_bResetButton )
	{
		//リセットが押された
		GX_DEBUGLOG("[GameGirl] Accept Reset Signal.");
		m_bResetButton = gxFalse;
		::GameReset();
	}

	if( IsPause() )
	{
		//ポーズ中
		if( m_sStepFrm == 0 )
		{

		}
		else
		{
			m_sStepFrm ++;
			goto exit;
		}
	}

	m_sStepFrm ++;

	//if( m_sRequestAccept == enOrderRequestSeqInit )
	//if( gxOrderManager::GetInstance()->ChangeRequestBuffer() )
	{
		//m_sRequestAccept = enOrderRequestSeqAccept;
		gxOrderManager::GetInstance()->SetRequestEnable( gxTrue );
	}

	//システム更新

	CDeviceManager::GetInstance()->GameInit();

	//入力更新

	inputMain();

	//Drag&Drop処理

	if( gxFileManager::GetInstance()->IsDragEnable() )
	{
		//マルチスレッド時にコンフリクトするのでブロックする

		for( Sint32 ii=0; ii<gxFileManager::GetInstance()->GetDropFileNum(); ii++ )
		{
			gxBool DragAndDrop(char* szFileName);
			gxChar* pFileName = gxFileManager::GetInstance()->GetDropFileName( ii );

			DragAndDrop( pFileName );
		}

		gxFileManager::GetInstance()->ClearDropFiles();
	}

	//ゲームメイン

	if( !gameMain() )
	{
		m_bMainLoop = gxFalse;
	}

	gxUIManager::GetInstance()->Action();
	gxUIManager::GetInstance()->Draw();

	if( m_bExit )
	{
		m_bMainLoop = gxFalse;
	}

	gxDebug::GetInstance()->AddAction();

	gxDebug::GetInstance()->UpdateRenderInfo(
		gxOrderManager::GetInstance()->GetOrderNum(),
		gxOrderManager::GetInstance()->GetSubOrderNum()
	);

	gxOrderManager::GetInstance()->ChangeRequestBuffer();

exit:

	//ネットワーク処理

//#ifdef _USE_MULTITHREAD_
//
    	int test = 0;	//goto labelによりなんか処理がないとエラーになるよ
//		CGameGirl::GetInstance()->network();
//#endif

}


gxBool m_GameMainThreadExist = gxFalse;
void* GameMainThread( void *pArgs )
{
	//このサブスレッドでゲームがずっと回る
	CGameGirl::GetInstance()->SubThreadExist( gxTrue );

	while( CGameGirl::GetInstance()->IsExist() )
	{
		m_GameMainThreadExist = gxTrue;
		CGameGirl::GetInstance()->GameMain();

		CGameGirl::GetInstance()->VSync();
	}

	CGameGirl::GetInstance()->SubThreadExist( gxFalse );

	GX_DEBUGLOG("[GameGirl] Exit Multi Thread");

	m_GameMainThreadExist = gxFalse;

	return NULL;
}


void* GameNetWorkThread(void*)
{
	while (CGameGirl::GetInstance()->IsExist())
	{
		CGameGirl::GetInstance()->network();
		CGameGirl::GetInstance()->network();
		CGameGirl::GetInstance()->network();
		CGameGirl::GetInstance()->network();

		gxLib::Sleep(4);
	}

	return NULL;
}


void* GameFileThread(void*)
{
	while (CGameGirl::GetInstance()->IsExist())
	{
		gxFileManager::GetInstance()->Action();

		gxLib::Sleep(4);
	}

	return NULL;
}


gxBool CGameGirl::network()
{
	//ネットワーク処理

	static Sint32 seq = 0;
	Float32 fRatio = 0.0f;

	//ネットワーク接続要求
	static Sint32 index;

//m_bOnLine

#if 1
	//IPアドレス取得
	switch( seq ){
	case 0:
		m_bOnLine = gxFalse;
		CGameGirl::GetInstance()->SetIPAddressV4(0, 0);
		seq = 100;
		break;

	case 100:
		index = gxUtil::OpenWebFile( GLOBAL_IP_ADDRESS_CHECK_URL );
		seq = 200;
		break;

	case 200:
		if (gxUtil::IsDownloadWebFile(index, &fRatio))
		{
			if (gxUtil::IsDownloadSucceeded(index))
			{
				Uint8* pData;
				size_t uSize;

				pData = gxUtil::GetDownloadWebFile(index, &uSize);

				if (pData)
				{
					m_bOnLine = gxTrue;
					CCsv* pCsv = new CCsv();
					pCsv->ReadFile(pData, uSize);
					gxChar* pString = pCsv->GetCell(5, 0);
					gxChar buf[FILENAMEBUF_LENGTH];
					sprintf(buf, "0x%s", pString);
					Uint32 ip = strtoul(buf, NULL, 16);
					CGameGirl::GetInstance()->SetIPAddressV4(ip, 0);
					SAFE_DELETE( pCsv );
					seq = 999;

					SAFE_DELETES(pData);
				}
				gxUtil::CloseWebFile(index);
				seq = 999;
			}
			else
			{
				m_bOnLine = gxFalse;
				seq = 999;
			}
		}
		break;

	case 999:
	default:
		break;
	}
#endif
	gxNetworkManager::GetInstance()->Action();
	CDeviceManager::GetInstance()->NetWork();

	return gxFalse;

}

void CGameGirl::End()
{
	//CDeviceManager::GetInstance()->SaveConfig();	//ここで固まる

	gxLib::SaveConfig();

	//if( !m_bAppFinish )
	{
		GX_DEBUGLOG("[GameGirl] Apprication Finish.");

		gameEnd();

		while ( gxTrue )
		{
			gxFileManager::GetInstance()->Action();

			if (gxFileManager::GetInstance()->IsLoadTaskExist())
			{
				VSync();
				continue;
			}
			break;
		}

		drawEnd();
		soundEnd();
		movieEnd();
		inputEnd();

		CBlueToothManager::GetInstance()->DestroyBlueToothThread();

		gxDebug::DeleteInstance();
	}

	//m_bAppFinish = gxTrue;

}


void CGameGirl::resume()
{
	GX_DEBUGLOG("[GameGirl] Accept Resume Signal.");

	CDeviceManager::GetInstance()->Resume();

}


gxBool CGameGirl::drawInit()
{
	GX_DEBUGLOG("[GameGirl] drawInit.");

	gxTexManager::GetInstance();
	gxOrderManager::GetInstance();
	gxRender::GetInstance();

	return gxTrue;
}

gxBool CGameGirl::drawMain()
{
	Float32 fTime = gxLib::GetTime();

	gxRender::GetInstance()->Action();

	CDeviceManager::GetInstance()->Render();

	fTime = gxLib::GetTime() - fTime;

	gxDebug::GetInstance()->UpdateRenderTime( fTime );

	return gxTrue;
}

gxBool CGameGirl::drawEnd()
{
	GX_DEBUGLOG("[GameGirl] drawEnd.");

	gxTexManager::DeleteInstance();
	gxOrderManager::DeleteInstance();
	gxRender::DeleteInstance();

	return gxTrue;
}


gxBool CGameGirl::soundInit()
{
	GX_DEBUGLOG("[GameGirl] soundInit.");

	gxSoundManager::GetInstance();
	return gxTrue;
}


gxBool CGameGirl::soundMain()
{
/*
	if(m_bHardPause)
	{
		gxSoundManager::GetInstance()->SetForceVolumeLevel( enMasterVolumeLow );
	}
	else
	{
		gxSoundManager::GetInstance()->SetForceVolumeLevel( enMasterVolumeMiddle );
	}
*/
	gxSoundManager::GetInstance()->Action();
	gxSoundManager::GetInstance()->Play();

	return gxTrue;
}

gxBool CGameGirl::soundEnd()
{
	GX_DEBUGLOG("[GameGirl] soundEnd.");

	gxSoundManager::DeleteInstance();

	return gxTrue;
}


gxBool CGameGirl::movieInit()
{
	GX_DEBUGLOG("[GameGirl] movieInit.");
	CMovieManager::GetInstance();
	return gxTrue;
}


gxBool CGameGirl::movieMain()
{
	CMovieManager::GetInstance()->Action();
	CMovieManager::GetInstance()->Draw();
	return gxTrue;
}

gxBool CGameGirl::movieEnd()
{
	GX_DEBUGLOG("[GameGirl] movieEnd.");
	CMovieManager::DeleteInstance();
	return gxTrue;
}


gxBool CGameGirl::inputInit()
{
	GX_DEBUGLOG("[GameGirl] inputInit.");
	gxPadManager::GetInstance();

	return gxTrue;
}

gxBool CGameGirl::inputMain()
{
	gxPadManager::GetInstance()->Action();

	if( gxLib::Joy(0)->psh&BTN_SELECT && gxLib::Joy(0)->trg&BTN_START )
	{
		CGameGirl::GetInstance()->SetReset();
	}

	return gxTrue;
}

gxBool CGameGirl::inputEnd()
{
	GX_DEBUGLOG("[GameGirl] inputEnd.");
	gxPadManager::DeleteInstance();

	return gxTrue;
}


gxBool CGameGirl::gameMain()
{
	//--------------------------------------------
	//ゲームのアップデート処理
	//--------------------------------------------

	gxBool bExist = gxTrue;

	static gxBool s_bFirst = gxTrue;

	if( s_bFirst )
	{
		m_pFontManager->SetDefaultData();
		m_pFontManager->SetFontScale(1.0f , 1.0f);
		m_pFontManager->SetProportional( gxTrue );

		//CFontManager::GetInstance()->SetOutLine( gxTrue , 0xffff0000 );
		//CFontManager::GetInstance()->SetDropShadow( gxTrue , 0xff0000ff );
		s_bFirst = gxFalse;
	}


	if( m_bThroughGameMain )
	{
		//一時的に処理を通したくないときを制御する
		m_bThroughGameMain = gxFalse;
	}
	else
	{
		//BG描画
#ifdef BGV_ON
		for (Sint32 yy = -200; yy < WINDOW_H; yy += 100)
		{
			for (Sint32 xx = -200; xx < WINDOW_W; xx += 200)
			{
				Sint32 ax = ((yy / 100) % 2 == 0) ? 100 + xx : xx;

				ax += gxLib::GetGameCounter() % 100;
				Sint32 ay = gxLib::GetGameCounter() % 100;

				gxLib::DrawBox(ax, yy + ay, ax + 100, yy + 100+ay, 0, gxTrue, ATR_DFLT, 0xff808080);
			}
		}
#endif

		static Sint32 m_SoftPauseCnt = 0;
		if( m_bSoftPause )
		{
			if( m_SoftPauseCnt == 0 )
			{
				bExist = CDeviceManager::GetInstance()->GameUpdate();
				gxLib::CaptureScreen(255);
			}
			else
			{
				Float32 fBlurPower = m_SoftPauseCnt/100.0f;
				fBlurPower = CLAMP( fBlurPower , 0.f , 0.1f );
				gxLib::PutSprite(0,0,0, enCapturePage , 0,0,WINDOW_W,WINDOW_H , 0,0 );
				gxLib::CreatePostEffectBlur( 0 , fBlurPower );
				bExist = CDeviceManager::GetInstance()->GamePause();

			}
			m_SoftPauseCnt ++;
		}
		else
		{
			bExist = CDeviceManager::GetInstance()->GameUpdate();
			m_SoftPauseCnt = 0;
		}

		m_uGameCounter ++;
	}

	gxDebug::GetInstance()->Draw();

	return bExist;
}


gxBool CGameGirl::gameEnd()
{
	GX_DEBUGLOG("[GameGirl] gameEnd.");
	::GameEnd();

	return gxTrue;
}


gxBool CGameGirl::VSync()
{
	if( !CGameGirl::GetInstance()->IsWaitVSync() ) return gxFalse;

	CDeviceManager::GetInstance()->vSync();

	return gxTrue;
}


gxBool CGameGirl::flip()
{
	CDeviceManager::GetInstance()->Flip();
	return gxTrue;
}


void CGameGirl::GetMemoryRemain( Uint32* uNow , Uint32* uTotal , Uint32* uMax )
{
	CDeviceManager::GetInstance()->UpdateMemoryStatus( uNow , uTotal , uMax );
}


void CGameGirl::AdjustScreenResolution()
{
	//画面サイズに応じたゲーム解像度を設定する
	Float32 sw, sh;

	//Windowの解像度

	sw = m_WindowScreenWidth;
	sh = m_WindowScreenHeight;

	//m_ScreenMode = enScreenModeAspect;//enScreenModeFull;//enScreenModeOriginal;//enScreenModeFull;

	if (m_ScreenMode == enScreenModeAspect)
	{
		//アスペクト比を維持した変形

		//まずは横に合わせる
		float fRatio = sw / WINDOW_W;

		m_GameScreenWidth = WINDOW_W*fRatio;
		m_GameScreenHeight = WINDOW_H*fRatio;

		if (m_GameScreenHeight > sh)
		{
			//縦幅が画面からはみ出した場合
			fRatio = sh / WINDOW_H;

			//縦に合わせる
			m_GameScreenWidth = WINDOW_W * fRatio;
			m_GameScreenHeight = WINDOW_H * fRatio;
		}
	}
	else if (m_ScreenMode == enScreenModeFull)
	{
		//フル画面

		m_GameScreenWidth = sw;
		m_GameScreenHeight = sh;
	}
	else// if( m_ScreenMode == enScreenModeFull )
	{
		//オリジナルサイズ

		m_GameScreenWidth = WINDOW_W;
		m_GameScreenHeight = WINDOW_H;
	}

}

gxChar* CGameGirl::GetScreenShotFileName()
{
	if (m_ScreenShotFileName[0])
	{
		return m_ScreenShotFileName;
	}

	return NULL;
}


void    CGameGirl::SetScreenShot( gxBool bShot )
{
	if (bShot)
	{
		Sint32 id = gxLib::SaveData.snapNum;
		sprintf(m_ScreenShotFileName, "%03d_%s.tga", id , APPLICATION_NAME );// pScreenShotFileName);
		gxLib::SaveData.snapNum++;
	}
	else
	{
		m_ScreenShotFileName[0] = 0x00;
	}

}

Float32 CGameGirl::GetTime( gxLib::Clock *pClock )
{
	*pClock = m_Time;

	gxLib::Clock old, now;

	old = m_StartTime;
	CDeviceManager::GetInstance()->Clock( &now );
	if (pClock)
	{
		*pClock = now;
	}

/*
	Uint32 oldMSec = old.Hour *60*60*1000 + old.Min*60*1000 + old.Sec*1000 + old.MSec;
	Uint32 nowMSec = now.Hour *60*60*1000 + now.Min*60*1000 + now.Sec*1000 + now.MSec;
	Uint32 sabunMsec = nowMSec - oldMSec;
	Float32 ret = (sabunMsec/1000.0f);
*/
	Uint32 oldUSec = old.Hour * 60 * 60 * 1000*1000 + old.Min * 60 * 1000 * 1000 + old.Sec * 1000 * 1000 + old.MSec * 1000 + old.USec;
	Uint32 nowUSec = now.Hour * 60 * 60 * 1000*1000 + now.Min * 60 * 1000 * 1000 + now.Sec * 1000 * 1000 + now.MSec * 1000 + now.USec;
	Uint32 sabunUsec = nowUSec - oldUSec;
	Float32 ret = (sabunUsec / 1000.0f);// *1000 * .0f);
	ret = ret / 1000.0f;
	return ret;
}
